//
//  SignatureViewController.m
//  MFmPay
//
//  Created by Prism Pay on 6/8/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SignatureViewController.h"
#import "SettingsViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "VoidRefundMainViewController.h"
#import "SupportViewController.h"
#import "Luhn.h"
#import "UIView+Toast.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "PaymentSelectionViewController.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "SignatureApprovedViewController.h"
#import "CreditCardSaleViewController.h"
#import "AppDelegate.h"
@interface SignatureViewController ()

@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self styleMe];

   
    self.amount.text=[NSString stringWithFormat:@"$%.2f",[self.amounttxt floatValue]];
    
    [self forceRotate];
    
}
-(BOOL)prefersStatusBarHidden{
    return YES;
}
-(void)forceRotate{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    //[self.navigationController setStatusBarHidden:YES animated:NO];
    
    
    //if (self.interfaceOrientation == UIInterfaceOrientationPortrait)
    //{
    self.view.transform = CGAffineTransformIdentity;
    self.view.transform = CGAffineTransformMakeRotation((M_PI * (90) / 180.0));
    self.view.bounds = CGRectMake(0.0, 0.0, 480, 320);
    
    //}
    [UIView commitAnimations];
}

-(void)viewDidDisappear:(BOOL)animated
{

}
//-(void)tap:(UISwipeGestureRecognizer *)tapRec{
//    if(tapRec.state==UIGestureRecognizerStateBegan)
//    self.signLbl.hidden=true;
//    
//    
//}
-(void)styleMe{
    [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
    
    self.processBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.processBtn.layer.cornerRadius = self.processBtn.bounds.size.width/2;
    self.processBtn.layer.masksToBounds = YES;
    
    self.amount.textColor=[ThemeColor changeGraphColor];
    
    [self.signView setLineWidth:2.0];
    self.signView.foregroundLineColor = [UIColor colorWithRed:0.204 green:0.596 blue:0.859 alpha:1.000];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settings:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}


- (IBAction)clear:(id)sender {
    self.signLbl.hidden=false;
    [self.signView clear];
}
- (IBAction)process:(id)sender {
    [self performSegueWithIdentifier:@"signApproved" sender:self];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    SignatureApprovedViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.ProcAppCode=self.ProcAppCode;;
    vcToPushTo.MCSTransID=self.MCSTransID;
    vcToPushTo.amounttxt=self.amounttxt;
    vcToPushTo.RefNum=self.RefNum;
    vcToPushTo.ProceTransID=self.ProceTransID;
    vcToPushTo.saleType = self.saleType;
}
@end
