//
//  SettingsViewController.h
//  MFmPay
//
//  Created by Prism Pay on 5/13/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *c1;
@property (weak, nonatomic) IBOutlet UIButton *c2;
@property (weak, nonatomic) IBOutlet UIButton *c3;
@property (weak, nonatomic) IBOutlet UIButton *c4;
@property (weak, nonatomic) IBOutlet UIButton *c5;
@property (weak, nonatomic) IBOutlet UIButton *c6;
@property (weak, nonatomic) IBOutlet UIButton *c7;
@property (weak, nonatomic) IBOutlet UIButton *c8;
- (IBAction)lightBlue:(id)sender;
- (IBAction)red:(id)sender;
- (IBAction)purple:(id)sender;
- (IBAction)mfmGreen:(id)sender;
- (IBAction)gray:(id)sender;
- (IBAction)darkBlue:(id)sender;

- (IBAction)lameBlue:(id)sender;

- (IBAction)parrotGreen:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *settingsBtn;
- (IBAction)activity:(id)sender;

- (IBAction)support:(id)sender;
- (IBAction)credit:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *tick1;
@property (weak, nonatomic) IBOutlet UIImageView *tick2;

@property (weak, nonatomic) IBOutlet UIImageView *tick3;
@property (weak, nonatomic) IBOutlet UIImageView *tick4;
@property (weak, nonatomic) IBOutlet UIImageView *tick8;
@property (weak, nonatomic) IBOutlet UIImageView *tick7;

@property (weak, nonatomic) IBOutlet UIImageView *tick6;
@property (weak, nonatomic) IBOutlet UIImageView *tick5;
@property (weak, nonatomic) IBOutlet UIButton *wizardBtn;
- (IBAction)runWizard:(id)sender;
- (IBAction)switchAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;
- (IBAction)payAction:(id)sender;
@end
