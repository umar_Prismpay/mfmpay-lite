//
//  PaymentSelectionViewController.h
//  MFMPay_iPhone
//
//  Created by Muhammad Umar on 3/6/18.
//  Copyright © 2018 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentSelectionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btn_card;
@property (weak, nonatomic) IBOutlet UIButton *btn_ach;
- (IBAction)credit_card:(id)sender;
- (IBAction)ach:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnActivity;
@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnSupport;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
- (IBAction)credit_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnACHCredit;
- (IBAction)achCredit:(id)sender;

@end
