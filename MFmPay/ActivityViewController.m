//
//  ActivityViewController.m
//  MFmPay
//
//  Created by Prism Pay on 4/21/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//CDInhSBWiH72jie3mTrCA==
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE_6_plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736 ) < DBL_EPSILON )
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#import "ActivityViewController.h"
//#import "NSMutableArray+TTMutableArray.h"
#import "ThemeColor.h"
#import "HistoryTableViewCell.h"
#import "PaymentSelectionViewController.h"
#import "UIView+Toast.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "SupportViewController.h"
#import "Reachability.h"
#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "SearchViewController.h"
#import "CreditCardSaleViewController.h"
#import "CreditCardSaleViewController.h"
@interface ActivityViewController ()
{
    NSString *url;
    NSString *pageCount;
    int count;
    NSString* emailCount;
    float transactionAmount;
    NSMutableArray *customerName;
    NSMutableArray *status;
    NSMutableArray *amount;
    NSMutableArray *date;
    NSMutableArray *transType;
    NSMutableArray *cardType;
    NSMutableArray *authCode;
    NSMutableArray *cardNum;
    NSMutableArray *historyID;
    NSMutableArray *numOfTrans;
    NSMutableArray *amountofTrans;
    NSMutableArray *appID;
    NSMutableArray *merchantID;
    NSMutableArray *acctTypeID;
    NSMutableArray *barTransDate;
    NSIndexPath *pathforseague;
    NSString *mcsacctID;
}
@end

@implementation ActivityViewController
- (void)loadView
{
    [super loadView];
    [self loadGraph];
    
//    UIButton *changeButton			= [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    changeButton.frame				= CGRectMake(0.0,
//                                                 _chart.frame.origin.y + _chart.frame.size.height + 20.0,
//                                                 100.0,
//                                                 44.0);
//    changeButton.center				= CGPointMake(self.view.frame.size.width / 2.0, changeButton.center.y);
//    [changeButton setTitle:@"Change" forState:UIControlStateNormal];
   // [changeButton addTarget:self action:@selector(changeClicked) forControlEvents:UIControlEventTouchDown];
    
//    [self.view addSubview:changeButton];
}
-(void)loadGraph{
    _values							= @[@0,@0,@0,@0,@0,@0,@0,@0,@0,@0,@0,@0,@0,@0];
    _barColors						= @[[UIColor blueColor], [UIColor redColor], [UIColor blackColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor colorWithRed:0.243 green:0.478 blue:0.518 alpha:1]];
    _currentBarColor				= 5;
    
    CGRect chartFrame;
    if(IS_IPHONE_6)
    {
        chartFrame				= CGRectMake(0,
                                             0.0,
                                             self.graphView.frame.size.width-30,
                                             self.graphView.frame.size.height-70);		;
    }
    else if(IS_IPHONE_6_plus)
    {
        chartFrame				= CGRectMake(10,
                                             0.0,
                                             self.graphView.frame.size.width+30,
                                             self.graphView.frame.size.height+30);		;
    }
    else if(IS_IPHONE_5)
    {
        chartFrame				= CGRectMake(10,
                                             30.0,
                                             self.graphView.frame.size.width-75,
                                             self.graphView.frame.size.height-90);		;
    }
    else
    {
        chartFrame				= CGRectMake(10,
                                             0.0,
                                             self.graphView.frame.size.width+600,
                                             self.graphView.frame.size.height+290);		;
    }
    _chart							= [[SimpleBarChart alloc] initWithFrame:chartFrame];
    //    _chart.center					= CGPointMake(self.graphView.frame.size.width / 2.0, self.graphView.frame.size.height / 2.0);
    _chart.delegate					= self;
    _chart.dataSource				= self;
    _chart.barShadowOffset			= CGSizeMake(2.0, 1.0);
    _chart.animationDuration		= 1.0;
    _chart.barShadowColor			= [UIColor clearColor];
    _chart.barShadowAlpha			= 0.5;
    _chart.barShadowRadius			= 1.0;
    _chart.incrementValue           = 0;
    _chart.hasYLabels=false;
    if(IS_IPHONE_5)
        _chart.barWidth					= 15.0;
    else if(IS_IPHONE_6_plus)
        _chart.barWidth					= 35;
    else if(IS_IPHONE_6)
        _chart.barWidth					= 25;
    else
        _chart.barWidth					= 20.00;
    _chart.xLabelType				= SimpleBarChartXLabelTypeHorizontal;
    
    _chart.barTextType				= SimpleBarChartBarTextTypeTop;
    _chart.barTextColor				= [UIColor whiteColor];
    _chart.gridColor				= [UIColor clearColor];
    
    [self.graphView addSubview:_chart];

}
- (void)viewDidLoad {
    [super viewDidLoad];
 
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
    
    //reporting.prismpay.com
    //atlasapi.paymentstage.com
    url  =[NSString stringWithFormat:@"%@Report/Transaction",sharedManager.url];
//    url  =@"https://demo.prismpay.com/API/Report/Transaction";
    
    
    
    pageCount=@"50";
    count=50;
    [self styleme];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hidehud) userInfo:nil repeats:NO];
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.blurView addGestureRecognizer: tapRec];
    UITapGestureRecognizer *tabletap = [[UITapGestureRecognizer alloc]
                                        initWithTarget:self action:@selector(scrollUp:)];
    [self.top_bar addGestureRecognizer: tabletap];
    
    UITapGestureRecognizer *tabletap1 = [[UITapGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(scrollUp:)];
    [self.top_image addGestureRecognizer: tabletap1];
    
    [self getService];
    [self getBarService];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [[UIApplication sharedApplication]setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
    //[self forceLandscape];
    //CGRect newframe=CGRectMake(0, 0, 667, 375);
    //self.view.frame=newframe;
   // [self.navigationController willRotateToInterfaceOrientation:10];
}

-(void)scrollUp:(UITapGestureRecognizer *)tapRec{
    [self.historyTableView setContentOffset:CGPointZero animated:YES];
}
-(void)tap:(UITapGestureRecognizer *)tapRec{
    self.blurView.hidden=true;
    self.csvView.hidden=true;
    [self.csvEmailField resignFirstResponder];
  
}
-(void)styleme{
    self.csvEmailField.delegate=self;
    self.view.backgroundColor=[UIColor whiteColor];

    self.numberofSalesLbl.textColor=[ThemeColor changeGraphColor];
    self.totalPriceSale.textColor=[ThemeColor changeGraphColor];
    self.sendRecieptBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.csvBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.csvBtn.layer.cornerRadius = self.csvBtn.bounds.size.width/2;
    self.csvBtn.layer.masksToBounds = YES;
    self.sendRecieptBtn.layer.cornerRadius = self.sendRecieptBtn.bounds.size.width/2;
    
    self.sendRecieptBtn.layer.masksToBounds = YES;
    self.headerView.backgroundColor=[ThemeColor changeGraphColor];
    self.activityBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.activityBtn.layer.cornerRadius = 10;
    self.activityBtn.layer.masksToBounds = YES;
}
-(void)getService{
    
    
    //Date work
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
    //eNds here
    if(count<51)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
            NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",mcsacctID,@"MCSAccountID", nil];
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                customerName=[[NSMutableArray alloc]init];
                status=[[NSMutableArray alloc]init];
                amount=[[NSMutableArray alloc]init];
                date=[[NSMutableArray alloc]init];
                transType=[[NSMutableArray alloc]init];
                cardType=[[NSMutableArray alloc]init];
                authCode=[[NSMutableArray alloc]init];
                cardNum=[[NSMutableArray alloc]init];
                historyID=[[NSMutableArray alloc]init];
                acctTypeID=[[NSMutableArray alloc]init];
                merchantID=[[NSMutableArray alloc]init];
                appID=[[NSMutableArray alloc]init];
                NSArray *arr = [[NSArray alloc]init];
                if( [responseObject valueForKey:@"Object"] == (id)[NSNull null])
                {
                    
                }
                else
                    arr=[responseObject valueForKey:@"Object"];
                
                if(arr.count > 0)
                {
                for(NSArray *dic in arr)
                {
                    if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                        [customerName addObject:@"N/A"];
                    else
                    [customerName addObject:[dic valueForKey:@"CardHolderName"]];
                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
                    [amount addObject:[dic valueForKey:@"Amount"]];
                    [date addObject:[dic valueForKey:@"TransactionDate"]];
                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
                    [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
                    [appID addObject:[dic valueForKey:@"ApplicationID"]];
                    [merchantID addObject:[dic valueForKey:@"MerchantID"]];
                    [acctTypeID addObject:[dic valueForKey:@"AccountTypeID"]];
                
                    
                }
                emailCount=[responseObject valueForKey:@"Custom"];
                if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
                while (count<[[responseObject valueForKey:@"Custom"]intValue])
                    
                    
                {
                    
                    count+=50;
                    pageCount=[NSString stringWithFormat:@"%d",count];
                    
                    [self getService1];
                    
                    
                }
                [self.historyTableView reloadData];
            }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
            
        });
    });
    
}
-(void)getService1{
    
    
    //Date work
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
    
    NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",mcsacctID,@"MCSAccountID", nil];
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        customerName=[[NSMutableArray alloc]init];
        status=[[NSMutableArray alloc]init];
        amount=[[NSMutableArray alloc]init];
        date=[[NSMutableArray alloc]init];
        transType=[[NSMutableArray alloc]init];
        cardType=[[NSMutableArray alloc]init];
        authCode=[[NSMutableArray alloc]init];
        cardNum=[[NSMutableArray alloc]init];
        historyID=[[NSMutableArray alloc]init];
        acctTypeID=[[NSMutableArray alloc]init];
        merchantID=[[NSMutableArray alloc]init];
        appID=[[NSMutableArray alloc]init];
        NSArray *arr=[responseObject valueForKey:@"Object"];
        
        
        for(NSArray *dic in arr)
        {
            if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                [customerName addObject:@"N/A"];
            else
                [customerName addObject:[dic valueForKey:@"CardHolderName"]];
            [status addObject:[dic valueForKey:@"TransactionStatusID"]];
            [amount addObject:[dic valueForKey:@"Amount"]];
            [date addObject:[dic valueForKey:@"TransactionDate"]];
            [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
            [cardType addObject:[dic valueForKey:@"CardTypeName"]];
            [authCode addObject:[dic valueForKey:@"AuthCode"]];
            [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
            [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
            [appID addObject:[dic valueForKey:@"ApplicationID"]];
            [merchantID addObject:[dic valueForKey:@"MerchantID"]];
            [acctTypeID addObject:[dic valueForKey:@"AccountTypeID"]];
        }
        if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
        while (count<[[responseObject valueForKey:@"Custom"]intValue])
            
            
        {
            
            count+=50;
            pageCount=[NSString stringWithFormat:@"%d",count];
            
            [self getService1];
            
            
        }
      
        [self.historyTableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"error: %@", error);
    }];
    
    
}

-(void)getBarService
{
    
    //Date work
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    self.startDate.text=startDate;
    self.endDate.text=@"Today";
    //eNds here
    
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
    
    NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",mcsacctID,@"MCSAccountID", nil];
            
            NSString *str=[NSString stringWithFormat:@"%@Dashboard/TransactionSummaryDaily?EndDate=%@&StartDate=%@&userID=%@",sharedManager.url,endDate,startDate,mcsacctID];
//            NSString *str=[NSString stringWithFormat:@"https://demo.prismpay.com/API/Dashboard/TransactionSummaryDaily?EndDate=%@&StartDate=%@&userID=%@",endDate,startDate,mcsacctID];
    [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *arr = [[NSArray alloc]init];
        if( [responseObject valueForKey:@"Object"] == (id)[NSNull null])
        {
            
        }
        else
        arr=[responseObject valueForKey:@"Object"];
        numOfTrans=[[NSMutableArray alloc]init];
        amountofTrans=[[NSMutableArray alloc]init];
        barTransDate=[[NSMutableArray alloc]init];
        if([arr count]> 0)
        {
        for(NSArray *dic in arr)
        {
   
            [numOfTrans addObject:[dic valueForKey:@"NoOfTransaction"]];
            [amountofTrans addObject:[dic valueForKey:@"Amount"]];
            [barTransDate addObject:[dic valueForKey:@"TransactionDate"]];
        }
        if([numOfTrans count]>0)
            self.no_data_view.hidden=true;
        int trans=0;
        float totAmount=0.00;
        float increment=0.00;
        for(NSUInteger i=0;i<[numOfTrans count];i++)
        {
            NSString *noTrans=[NSString stringWithFormat:@"%@",[numOfTrans objectAtIndex:i]];
            if([noTrans isEqualToString:@"0"])
            {
                NSLog(@"i am here");
            }
            else
            {
                trans+=[noTrans intValue];
            }
            NSString *totalAmount=[NSString stringWithFormat:@"%@",[amountofTrans objectAtIndex:i]];
            if([totalAmount floatValue]>increment)
                increment=[totalAmount floatValue];
            totAmount+=[totalAmount floatValue];
            
            
        }
       int roundedUp = ceil(increment);
        
        
        NSString *card = [NSString stringWithFormat:@"%d",roundedUp];
        
        
        for (int i = 1; i < card.length; i++) {
            
            
                NSRange range = NSMakeRange(i, 1);
                card = [card stringByReplacingCharactersInRange:range withString:@"0"];
            
            
        } //out: **** **** **** 1234
        
        roundedUp=[card intValue];
        
        
        NSLog(@"%d",roundedUp);
        _values=amountofTrans;
        
       // _values=@[@20,@10,@30,@50,@40,@20,@40,@06,@80,@10,@40,@50,@30,@70];
//        if(_chart.incrementValue <10)
//        {
//            _chart.incrementValue = 1;
//        }
//        else
//        {
//        _chart.incrementValue= roundedUp/3;
//        }
        _chart.incrementValue = 300;
        _chart.hasYLabels=true;
    
        //_chart.xLabelColor=[UIColor clearColor];
        [_chart reloadData];
        NSLog(@"%d",trans);
        self.numberofSalesLbl.text=[NSString stringWithFormat:@"%d",trans];
        self.totalPriceSale.text=[NSString stringWithFormat:@"$%.02f",totAmount];
    }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"error: %@", error);
    }];
        });
    });

}
- (void)viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];
    
    
  //  [_chart reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSUInteger)numberOfBarsInBarChart:(SimpleBarChart *)barChart
{
    return _values.count;
}

- (CGFloat)barChart:(SimpleBarChart *)barChart valueForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] floatValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart textForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (NSString *)barChart:(SimpleBarChart *)barChart xLabelForBarAtIndex:(NSUInteger)index
{
    return [[_values objectAtIndex:index] stringValue];
}

- (UIColor *)barChart:(SimpleBarChart *)barChart colorForBarAtIndex:(NSUInteger)index
{
   // return [_barColors objectAtIndex:_currentBarColor];
    return  [ThemeColor changeGraphColor];
}





#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ customerName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pos";
    
    
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
//    if([[[ActivityData statusArr]objectAtIndex:indexPath.row]isEqualToString:@"Declined"])
//        cell.status_lbl.textColor=[UIColor redColor];
    NSString *str;
   
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
        if([str isEqualToString:@"0"] )
        {
         
            //[status replaceObjectAtIndex:indexPath.row withObject:@"Approved"];
            str=@"Approved";
        }
    else
    {
       // [status replaceObjectAtIndex:indexPath.row withObject:@"Declined"];
        cell.status_lbl.textColor=[UIColor redColor];
        str=@"Declined";
    }
    
    cell.status_lbl.text=str;
    cell.customer_name_lbl.text=[ customerName objectAtIndex:indexPath.row];
    cell.customer_name_lbl.textColor=[ThemeColor changeGraphColor];
    NSString *string = [date objectAtIndex:indexPath.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
  //  preTel = @"2018-02-14";
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
    //[date replaceObjectAtIndex:indexPath.row withObject:finalDate];
    cell.date_lbl.text=finalDate;
    
   
    cell.price_lbl.text=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:indexPath.row]floatValue ]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str;
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    pathforseague=indexPath;
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
    if([str isEqualToString:@"0"] )
        [self performSegueWithIdentifier:@"approved" sender:self];
        else
        [self performSegueWithIdentifier:@"declined" sender:self];
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
//    NSIndexPath *path = [self.historyTableView indexPathForSelectedRow];
    ApprovedViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.index = pathforseague.row;
    vcToPushTo.customerName=[customerName objectAtIndex:pathforseague.row];
    vcToPushTo.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    
    NSString *string = [date objectAtIndex:pathforseague.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
    
    vcToPushTo.date=finalDate;
    vcToPushTo.transType=[transType objectAtIndex:pathforseague.row];
    vcToPushTo.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    vcToPushTo.acctTypeID=[NSString stringWithFormat:@"%@",[acctTypeID objectAtIndex:pathforseague.row]];
    vcToPushTo.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    vcToPushTo.appID=[NSString stringWithFormat:@"%@",[appID objectAtIndex:pathforseague.row]];
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.cardTypetxt=@"N/A";
    else
        vcToPushTo.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.authCodetxt=@"N/A";
    else
    vcToPushTo.authCodetxt=[authCode objectAtIndex:pathforseague.row];
   // vcToPushTo.status=[status objectAtIndex:path.row];
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    
    
    
    DeclindedViewController *dec = segue.destinationViewController;
    dec.index = pathforseague.row;
    dec.customerName=[customerName objectAtIndex:pathforseague.row];
    dec.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    
    dec.acctTypeID=[NSString stringWithFormat:@"%@",[acctTypeID objectAtIndex:pathforseague.row]];
    dec.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    dec.appID=[NSString stringWithFormat:@"%@",[appID objectAtIndex:pathforseague.row]];
    
    dec.date=finalDate;
    dec.transType=[transType objectAtIndex:pathforseague.row];
    dec.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.cardTypetxt=@"N/A";
    else
        dec.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    

    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.authCodetxt=@"N/A";
    else
        dec.authCodetxt=[authCode objectAtIndex:pathforseague.row];
  //  dec.status=[status objectAtIndex:path.row];
    dec.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
}







#pragma button actions
- (IBAction)showHistory:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(hidehud) userInfo:nil repeats:NO];
    self.historyImage.image=[UIImage imageNamed:@"Detail_List_Icon"];
    self.historyLabel.hidden=false;
    self.graphImage.image=[UIImage imageNamed:@"Graph_Icon_Deselect"];
    self.graphLabel.hidden=true;
    self.sendRecieptBtn.hidden=false;
    self.tableViewLeading.constant=0;
    self.tableViewTrailing.constant=0;
    [self.historyTableView layoutIfNeeded];
 
}
-(void)hidehud{
       [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (IBAction)showGraph:(id)sender {
    
    self.historyImage.image=[UIImage imageNamed:@"Detail_List_Icon_Deselect"];
    self.historyLabel.hidden=true;
    self.graphImage.image=[UIImage imageNamed:@"Graph_Icon"];
    self.graphLabel.hidden=false;
    self.sendRecieptBtn.hidden=true;
    self.tableViewLeading.constant=1050;
    self.tableViewTrailing.constant=-1050;
    [self.historyTableView layoutIfNeeded];
    
    
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settingsAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)supportAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)activityAction:(id)sender {
    
}

- (IBAction)sendReceipt:(id)sender {
    self.csvEmailField.text=@"";
    self.blurView.hidden=false;
    self.csvView.hidden=false;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion https://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL)validate
{
    if(self.csvEmailField.text.length==0)
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Email Address", @"Please Enter Email Address")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self NSStringIsValidEmail:self.csvEmailField.text])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Valid Email Address", @"Please Enter Valid Email Address")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    return true;
}
- (IBAction)sendCsv:(id)sender {
    if([self validate])
    {
    [self sendemailCSv];
    }

}

- (IBAction)search:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *myVC = (SearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"search"];
    [self.navigationController pushViewController:myVC animated:NO];
}

-(void)sendemailCSv
{
    //Date work
    if([self NSStringIsValidEmail:self.csvEmailField.text])
    {
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
    //eNds here
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            NSString * myUrl = [NSString stringWithFormat:@"%@Utility/ExportToCSV",sharedManager.url];
            NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",emailCount,@"PageCount",@"1",@"PageIndex",self.csvEmailField.text,@"ToEmail",mcsacctID,@"MCSAccountID", nil];

            [manager POST:myUrl parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//                [manager POST:@"https://demo.prismpay.com/API/Utility/ExportToCSV" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                    mail.mailComposeDelegate = self;
                    if ([MFMailComposeViewController canSendMail])
                    {
                        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                        mail.mailComposeDelegate = self;
                        NSDateFormatter *format = [[NSDateFormatter alloc] init];
                        [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
                        NSString *message = [NSString stringWithFormat:@"A %@ has been processed.\r\nDate/Time: %@.\r\nAmount: $ %@.\r\nForm of Payment: %@\r\nThank You!.\r\nPlease print out this authorization and keep it with your bank records",@"Sale", [[format stringFromDate:[NSDate date]] capitalizedString], [NSString stringWithFormat:@"%@",self.totalPriceSale.text],@"Credit Card: Sale"];
                        
                        
                        NSString * emailID = self.csvEmailField.text;
                        [mail setSubject:@"Sales History"];
                        [mail setMessageBody:message isHTML:NO];
                        [mail setToRecipients:@[emailID]];
                        
                        [self presentViewController:mail animated:YES completion:NULL];
                        
                        
                        //                [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                        //                            duration:3.0
                        //                            position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                        //                               title:NSLocalizedString(@"Success!", @"Success!")
                        //                               image:nil
                        //                               style:nil
                        //                          completion:nil];
                        [self.csvEmailField resignFirstResponder];
                        self.csvEmailField.text=@"";
                       
                        
                    }
                    else
                    {
                        NSLog(@"This device cannot send email");
                    }
                
                
                
                
                
                
                
//                
//                [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent") 
//                            duration:3.0
//                            position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                               title:NSLocalizedString(@"Success!", @"Success!") 
//                               image:nil
//                               style:nil
//                          completion:nil];
                [self.csvEmailField resignFirstResponder];
                self.blurView.hidden=true;
                self.csvView.hidden=true;
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
        });
    });
    }
    else
    {
        [self.view makeToast:@"Enter valid email address"
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:nil
                       image:nil
                       style:nil
                  completion:nil];

    }
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Success!", @"Success!")
                           image:nil
                           style:nil
                      completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.csvEmailField resignFirstResponder];
   
    return YES;
}

@end
