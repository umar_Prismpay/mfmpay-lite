//
//  ACHSaleViewController.m
//  MFMPay_iPhone
//
//  Created by Muhammad Umar on 3/6/18.
//  Copyright © 2018 Merchant First. All rights reserved.
//

#import "ACHSaleViewController.h"
#import "CreditCardSaleViewController.h"
#import "SettingsViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "VoidRefundMainViewController.h"
#import "SupportViewController.h"
#import "Luhn.h"
#import "UIView+Toast.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "XMLDictionary.h"
#import "SignatureApprovedViewController.h"
#import "RealSignatureApprovedViewController.h"
#import "PinDeclinedViewController.h"
#import "ProgressHUD.h"
#import "BKCardNumberField.h"
#import "EADSessionController.h"
#import "DELProtocol.h"
#import "BKMoneyUtils.h"
#import "ConnectorListModel.h"
#import "PaymentSelectionViewController.h"
@interface ACHSaleViewController ()

@end

@implementation ACHSaleViewController
{
    NSString * accountHolderType;
    NSString * accountType;
    NSString *resultCode;
    NSString *amountTxt;
    NSString *MCSTransactionID;
    NSString *ProcessorApprovalCode;
    NSString *ProcessorTransactionID;
    NSString *ReferenceNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleMe];
    accountHolderType = @"0";
    accountType = @"0";
    self.txtAmount.delegate=self;
    [self.txtAmount addTarget:self action:@selector(editMe:) forControlEvents:UIControlEventEditingChanged];
  
    
    UIToolbar* checkNumberBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    checkNumberBar.barStyle = UIBarStyleBlackTranslucent;
    checkNumberBar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [checkNumberBar sizeToFit];
    
    
    UIToolbar* amountBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    amountBar.barStyle = UIBarStyleBlackTranslucent;
    amountBar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(AmountBar)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [amountBar sizeToFit];
    
    
    
    UIToolbar* accountTitleBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    accountTitleBar.barStyle = UIBarStyleBlackTranslucent;
    accountTitleBar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(AccountTitleBar)],
                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [accountTitleBar sizeToFit];
    
    
    UIToolbar* accountNumberBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    accountNumberBar.barStyle = UIBarStyleBlackTranslucent;
    accountNumberBar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(accountNumberBar)],
                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [accountNumberBar sizeToFit];
    
    
    
    UIToolbar* routingNumberBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    routingNumberBar.barStyle = UIBarStyleBlackTranslucent;
    routingNumberBar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(routingNumberBar)],
                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [routingNumberBar sizeToFit];
    
    
    
    
    
    _txtAmount.inputAccessoryView = amountBar;
    _txtCheckNumber.inputAccessoryView = checkNumberBar;
    _txtAccountNumber.inputAccessoryView = accountNumberBar;
    _txtRoutingNumber.inputAccessoryView = routingNumberBar;
    _txtAccountTitle.inputAccessoryView = accountTitleBar;
    
                            
                            
    // Do any additional setup after loading the view.
}
                            -(void)cancelNumberPad{
                                [_txtAmount resignFirstResponder];
                                [_txtCheckNumber resignFirstResponder];
                                [_txtAccountNumber resignFirstResponder];
                                [_txtRoutingNumber resignFirstResponder];
                            }

-(void)AmountBar{
    [_txtAmount resignFirstResponder];
    [_txtAccountTitle becomeFirstResponder];
}

-(void)AccountTitleBar{
    [_txtAccountTitle resignFirstResponder];
    [_txtAccountNumber becomeFirstResponder];
    
}

-(void)accountNumberBar{
    [_txtAccountNumber resignFirstResponder];
    [_txtRoutingNumber becomeFirstResponder];
    
}

-(void)routingNumberBar{
    [_txtRoutingNumber resignFirstResponder];
    [_txtCheckNumber becomeFirstResponder];
    
}
-(void)styleMe{
//    self.btnCredit.backgroundColor=[ThemeColor changeGraphColor];
    self.btnProcess.backgroundColor=[ThemeColor changeGraphColor];
    self.btnClearSale.backgroundColor = [ThemeColor changeGraphColor];
    self.btnClearSale.layer.cornerRadius = self.btnClearSale.frame.size.width/2;
    self.switchAccountType.tintColor = [ThemeColor changeGraphColor];
    self.switchAccountHolderType.tintColor = [ThemeColor changeGraphColor];
//    self.btnSupport.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnActivity.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnSettings.backgroundColor=[ThemeColor changeGraphColor];
    [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
}
-(void)editMe:(id)sender{
    NSString* strPrice = [sender text];
    static BOOL toggle = NO;
    if (toggle) {
        toggle = NO;
    }
  
    strPrice = [strPrice stringByReplacingOccurrencesOfString:@"." withString:@""];
    toggle = YES;
    self.txtAmount.text = [@"" stringByAppendingFormat:@"%0.2f", [strPrice floatValue]/100.0];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)accountType:(id)sender {
    if (self.switchAccountType.selectedSegmentIndex == 0)
        accountType = @"0";
    else
        accountType = @"1";
}

- (IBAction)accountHolderType:(id)sender {
    if (self.switchAccountHolderType.selectedSegmentIndex == 0)
        accountHolderType = @"0";
    else
        accountHolderType = @"1";
}
-(BOOL)Validate{
    if([self.txtAmount.text isEqualToString:@""]||[self.txtAmount.text isEqualToString:@"0.00"])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Amount greater than 0.00", @"Please Enter Amount greater than 0.00")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                      
         ];
        return false;
    }
    

    if([self.txtAccountTitle.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Account Title", @"Please Enter Account Title") duration:3.0 position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]];
//        [self.view makeToast:NSLocalizedString(@"Please Enter Account Title", @"Please Enter Account Title")
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:NSLocalizedString(@"Alert", @"Alert")
        
        return false;
    }
    if([self.txtAccountNumber.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Account Number", @"Please Enter Account Number")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
         ];
        return false;
    }
    if([self.txtRoutingNumber.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Routing Number", @"Please Enter Routing Number")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
         
         ];
        return false;
    }
    return true;
}
- (IBAction)processTransaction:(id)sender {
    if([self.soapAction isEqualToString:@"ACHSale"])
    {
    if(self.Validate)
    {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        
        
    

        
        
   
        NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
   
        NSString *mcid=[prefs valueForKey:@"MCSID"];
        NSString * routing = self.txtRoutingNumber.text;
    NSString * accountNumber = self.txtAccountNumber.text;
    NSString * accountTitle = self.txtAccountTitle.text;
    NSString * amount = self.txtAmount.text;
    NSString * checkNumber = self.txtCheckNumber.text;
    

//        NSString *urlStringgetairport = [NSString stringWithFormat:@"https://test.mycardstorage.com/api/api.asmx"];
        NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
        NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
        NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
        NSString *soapmessage = [NSString stringWithFormat:@ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                 "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                                 "<soap12:Header>\n"
                                 "<AuthHeader xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<UserName>MFUser</UserName>\n"
                                 "<Password>slYC8T#fhnK0tLp5</Password>\n"
                                 "</AuthHeader>\n"
                                 "</soap12:Header>\n"
                                 "<soap12:Body>\n"
                                 "<ACHSale_Soap xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<achSale>\n"
                                 "<ServiceSecurity>\n"
                                 "<ServiceUserName>MF</ServiceUserName>\n"
                                 "<ServicePassword>kZJ33HgBhH$NFFdvE</ServicePassword>\n"
                                 "<MCSAccountID>%@</MCSAccountID>\n"
                                 "</ServiceSecurity>\n"
                                 "<TokenData>\n"
                                 "<Token></Token>\n"
                                 "<AccountNumber>%@</AccountNumber>\n"
                                 "<AccountType>%@</AccountType>\n"
                                 "<RoutingNumber>%@</RoutingNumber>\n"
                                 "<AccountHolderName>%@</AccountHolderName>\n"
                                 "<AccountHolderType>%@</AccountHolderType>\n"
                                 "</TokenData>\n"
                                 "<TransactionData>\n"
                                 "<Amount>%@</Amount>\n"
                                 "<SECCode>web</SECCode>\n"
                                 "<TicketNumber></TicketNumber>\n"
                                 "<CheckNumber>%@</CheckNumber>\n"
                                 "<GatewayID>1</GatewayID>\n"
                                 "</TransactionData>\n"
                                 "</achSale>\n"
                                 "</ACHSale_Soap>\n"
                                 "</soap12:Body>\n"
                                 "</soap12:Envelope>\n",mcid,accountNumber,accountType,routing,accountTitle,accountHolderType,[NSString stringWithFormat:@"%@",amount],checkNumber];
        
        
        
        
        NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
        
        
        [finalRequest addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [finalRequest setHTTPMethod:@"POST"];
        [finalRequest setHTTPBody:soapdata];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
        NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"Response string: %@",fetchedXML);
            
            NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
            NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
            NSDictionary *credit=[soap valueForKey:@"ACHSale_SoapResponse"];
            NSDictionary *CreditSale_SoapResult=[credit valueForKey:@"ACHSale_SoapResult"];
            NSDictionary *result=[CreditSale_SoapResult valueForKey:@"Result"];
            resultCode=[result valueForKey:@"ResultCode"];
            amountTxt=[CreditSale_SoapResult valueForKey:@"Amount"];
            MCSTransactionID=[CreditSale_SoapResult valueForKey:@"TransactionID"];
            ProcessorApprovalCode=[CreditSale_SoapResult valueForKey:@"ProcessorApprovalCode"];
            ProcessorTransactionID=[CreditSale_SoapResult valueForKey:@"ProcessorTransactionID"];
            ReferenceNumber=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
            if([resultCode isEqualToString:@"0"])
            {
                // [self performSegueWithIdentifier:@"signApproved" sender:self];
                [self performSegueWithIdentifier:@"realSign" sender:self];
            }
            else
            {
                [self performSegueWithIdentifier:@"pinDeclined" sender:self];
            }
            NSLog(@"dictionary: %@", xmlDoc);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        [task resume];
    }
    
    
    //Ends here
    }
    else
    {
        if(self.Validate)
        {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            
            
            
            
            
            
            
            NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
            
            NSString *mcid=[prefs valueForKey:@"MCSID"];
            NSString * routing = self.txtRoutingNumber.text;
            NSString * accountNumber = self.txtAccountNumber.text;
            NSString * accountTitle = self.txtAccountTitle.text;
            NSString * amount = self.txtAmount.text;
            NSString * checkNumber = self.txtCheckNumber.text;
            
            
         
//            NSString *urlStringgetairport = [NSString stringWithFormat:@"https://test.mycardstorage.com/api/api.asmx"];
            NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
            NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
            NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
            NSString *soapmessage = [NSString stringWithFormat:@ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                     "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                                     "<soap12:Header>\n"
                                     "<AuthHeader xmlns=\"https://MyCardStorage.com/\">\n"
                                     "<UserName>MFUser</UserName>\n"
                                     "<Password>slYC8T#fhnK0tLp5</Password>\n"
                                     "</AuthHeader>\n"
                                     "</soap12:Header>\n"
                                     "<soap12:Body>\n"
                                     "<ACHCredit_Soap xmlns=\"https://MyCardStorage.com/\">\n"
                                     "<achCredit>\n"
                                     "<ServiceSecurity>\n"
                                     "<ServiceUserName>MF</ServiceUserName>\n"
                                     "<ServicePassword>kZJ33HgBhH$NFFdvE</ServicePassword>\n"
                                     "<MCSAccountID>%@</MCSAccountID>\n"
                                     "</ServiceSecurity>\n"
                                     "<TokenData>\n"
                                     "<Token></Token>\n"
                                     "<AccountNumber>%@</AccountNumber>\n"
                                     "<AccountType>%@</AccountType>\n"
                                     "<RoutingNumber>%@</RoutingNumber>\n"
                                     "<AccountHolderName>%@</AccountHolderName>\n"
                                     "<AccountHolderType>%@</AccountHolderType>\n"
                                     "</TokenData>\n"
                                     "<TransactionData>\n"
                                     "<Amount>%@</Amount>\n"
                                     "<SECCode>web</SECCode>\n"
                                     "<TicketNumber></TicketNumber>\n"
                                     "<CheckNumber>%@</CheckNumber>\n"
                                     "<GatewayID>1</GatewayID>\n"
                                     "</TransactionData>\n"
                                     "</achCredit>\n"
                                     "</ACHCredit_Soap>\n"
                                     "</soap12:Body>\n"
                                     "</soap12:Envelope>\n",mcid,accountNumber,accountType,routing,accountTitle,accountHolderType,[NSString stringWithFormat:@"%@",amount],checkNumber];
            
            
            
            
            NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
            
            
            [finalRequest addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            
            [finalRequest setHTTPMethod:@"POST"];
            [finalRequest setHTTPBody:soapdata];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
            NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"Response string: %@",fetchedXML);
                
                NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
                NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
                NSDictionary *credit=[soap valueForKey:@"ACHCredit_SoapResponse"];
                NSDictionary *CreditSale_SoapResult=[credit valueForKey:@"ACHCredit_SoapResult"];
                NSDictionary *result=[CreditSale_SoapResult valueForKey:@"Result"];
                resultCode=[result valueForKey:@"ResultCode"];
                amountTxt=[CreditSale_SoapResult valueForKey:@"Amount"];
                MCSTransactionID=[CreditSale_SoapResult valueForKey:@"TransactionID"];
                ProcessorApprovalCode=[CreditSale_SoapResult valueForKey:@"ProcessorApprovalCode"];
                ProcessorTransactionID=[CreditSale_SoapResult valueForKey:@"ProcessorTransactionID"];
                ReferenceNumber=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
                if([resultCode isEqualToString:@"0"])
                {
                    // [self performSegueWithIdentifier:@"signApproved" sender:self];
                    [self performSegueWithIdentifier:@"realSign" sender:self];
                }
                else
                {
                    [self performSegueWithIdentifier:@"pinDeclined" sender:self];
                }
                NSLog(@"dictionary: %@", xmlDoc);
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }];
            [task resume];
        }
        
        
        //Ends here
    }
    
    
}

- (IBAction)clearSale:(id)sender {
    self.txtRoutingNumber.text = @"";
    self.txtAccountNumber.text = @"";
    self.txtAccountNumber.text = @"";
    self.txtAccountTitle.text  = @"";
    self.txtCheckNumber.text  = @"";
    self.txtAmount.text = @"0.00";
}

- (IBAction)payAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)activities:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    
    NSString *mcid=[prefs valueForKey:@"MCSID"];
    NSLog(@"%@",segue.destinationViewController);
    // SignatureApprovedViewController *vcToPushTo = segue.destinationViewController;
    RealSignatureApprovedViewController *vcToPushTo=segue.destinationViewController;
    vcToPushTo.ProcAppCode=@"N/A";
    vcToPushTo.MCSTransID=[NSString stringWithFormat:@"%@",mcid];
    vcToPushTo.amounttxt= amountTxt;
    vcToPushTo.RefNum=@"N/A";
    if([self.soapAction isEqualToString:@"ACHSale"])
    vcToPushTo.saleType = @"ACH: Sale";
       else
       {
vcToPushTo.saleType = @"ACH: Credit";
       }
    if(ProcessorTransactionID.length == 0)
    {
        vcToPushTo.ProceTransID = @"N/A";
    }
    else
    vcToPushTo.ProceTransID=ProcessorTransactionID;
    
    
    
    
    
    
    PinDeclinedViewController *dec=segue.destinationViewController;
    dec.ProcAppCode=@"N/A";
    dec.MCSTransID=[NSString stringWithFormat:@"%@",mcid];
    dec.amounttxt= amountTxt;
    dec.RefNum=@"N/A";
    if(ProcessorTransactionID.length == 0)
    {
        dec.ProceTransID = @"N/A";
    }
    else
        dec.ProceTransID=ProcessorTransactionID;
    
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder ];
    return true;
}

- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
@end
