//
//  VoidRefundMainViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/9/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "VoidRefundMainViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "ThemeColor.h"
#import "HistoryTableViewCell.h"
#import "PaymentSelectionViewController.h"
#import "UIView+Toast.h"
#import "VoidViewController.h"
#import "RefundViewController.h"
#import "SupportViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "SearchViewController.h"
#import "VoidSearchViewController.h"
#import "RefundSearchViewController.h"
@interface VoidRefundMainViewController ()
{
    BOOL isRefund;
    BOOL isVoid;
    NSString *url;
    NSString *pageCount;
    int count;
    NSMutableArray *customerName;
    NSMutableArray *status;
    NSMutableArray *amount;
    NSMutableArray *date;
    NSMutableArray *transType;
    NSMutableArray *cardType;
    NSMutableArray *authCode;
    NSMutableArray *cardNum;
    NSMutableArray *historyID;
    NSMutableArray *numOfTrans;
    NSMutableArray *amountofTrans;
    NSMutableArray *stationID;
    NSMutableArray *merchantID;
    NSMutableArray *applicationID;
    NSIndexPath *pathforseague;
    NSString * mcsacctID;
    
}
@end

@implementation VoidRefundMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
    isRefund=true;
    isVoid=false;
    // Do any additional setup after loading the view.
    
    url  = [NSString stringWithFormat:@"%@Report/Transaction",sharedManager.url];
//    url = @"https://demo.prismpay.com/API/Report/Transaction";
    
    pageCount=@"50";
    count=50;
    [self styleme];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(hidehud) userInfo:nil repeats:NO];
    [self getService];

}
-(void)styleme{
    self.view.backgroundColor=[UIColor whiteColor];
    self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.headerView.backgroundColor=[ThemeColor changeGraphColor];
    self.creditBtn.layer.cornerRadius = 10;
    self.creditBtn.layer.masksToBounds = YES;

}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)getService{
    
    
    //Date work
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
    //eNds here
     if(count<51)
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
            NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",@"0",@"TransactionStatusID",@"1",@"TransactionTypeID",mcsacctID,@"MCSAccountID", nil];
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                customerName=[[NSMutableArray alloc]init];
                status=[[NSMutableArray alloc]init];
                amount=[[NSMutableArray alloc]init];
                date=[[NSMutableArray alloc]init];
                transType=[[NSMutableArray alloc]init];
                cardType=[[NSMutableArray alloc]init];
                authCode=[[NSMutableArray alloc]init];
                cardNum=[[NSMutableArray alloc]init];
                historyID=[[NSMutableArray alloc]init];
                applicationID=[[NSMutableArray alloc]init];
                stationID=[[NSMutableArray alloc]init];
                merchantID=[[NSMutableArray alloc]init];
                NSArray *arr=[responseObject valueForKey:@"Object"];
                
                if (arr != (id)[NSNull null] && [arr count] != 0)
                for(NSArray *dic in arr)
                {
                    if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                        [customerName addObject:@"N/A"];
                    else
                        [customerName addObject:[dic valueForKey:@"CardHolderName"]];
                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
                    [amount addObject:[dic valueForKey:@"Amount"]];
                    [date addObject:[dic valueForKey:@"TransactionDate"]];
                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
                    [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
                    [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
                    [stationID addObject:[dic valueForKey:@"StationID"]];
                    [merchantID addObject:[dic valueForKey:@"MerchantID"]];

                    
                }
                if([customerName count]>0)
                    self.no_data_view.hidden=true;
                if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
                while (count<[[responseObject valueForKey:@"Custom"]intValue])
                    
                    
                {
                    
                    count+=50;
                    pageCount=[NSString stringWithFormat:@"%d",count];
                   
                    [self getService1];
                    
                    
                }
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.records reloadData];
               
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
            
        });
    });
    
}
-(void)getService1{
    
    
    //Date work
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
          //  NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex", nil];
    
     NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",@"0",@"TransactionStatusID",mcsacctID,@"MCSAccountID", nil];
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                customerName=[[NSMutableArray alloc]init];
                status=[[NSMutableArray alloc]init];
                amount=[[NSMutableArray alloc]init];
                date=[[NSMutableArray alloc]init];
                transType=[[NSMutableArray alloc]init];
                cardType=[[NSMutableArray alloc]init];
                authCode=[[NSMutableArray alloc]init];
                cardNum=[[NSMutableArray alloc]init];
                historyID=[[NSMutableArray alloc]init];
                applicationID=[[NSMutableArray alloc]init];
                stationID=[[NSMutableArray alloc]init];
                merchantID=[[NSMutableArray alloc]init];
                NSArray *arr=[responseObject valueForKey:@"Object"];
                
                
                for(NSArray *dic in arr)
                {
                    if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                        [customerName addObject:@"N/A"];
                    else
                        [customerName addObject:[dic valueForKey:@"CardHolderName"]];                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
                    [amount addObject:[dic valueForKey:@"Amount"]];
                    [date addObject:[dic valueForKey:@"TransactionDate"]];
                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
                    [historyID addObject:[dic valueForKey:@"TransactionID"]];
                    [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
                    [stationID addObject:[dic valueForKey:@"StationID"]];
                    [merchantID addObject:[dic valueForKey:@"MerchantID"]];
                }
                if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
                while (count<[[responseObject valueForKey:@"Custom"]intValue])
                    
                    
                {
                    
                    count+=50;
                    pageCount=[NSString stringWithFormat:@"%d",count];
                    
                    [self getService1];
                    
                    
                }
                [self.records reloadData];
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ customerName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pos";
    
    
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //    if([[[ActivityData statusArr]objectAtIndex:indexPath.row]isEqualToString:@"Declined"])
    //        cell.status_lbl.textColor=[UIColor redColor];
    NSString *str;
    
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
    if([str isEqualToString:@"0"] )
    {
        
        //[status replaceObjectAtIndex:indexPath.row withObject:@"Approved"];
        str=@"Approved";
    }
    else
    {
        // [status replaceObjectAtIndex:indexPath.row withObject:@"Declined"];
      //  cell.status_lbl.textColor=[UIColor redColor];
        str=@"Declined";
        str=@"Approved";
    }
    
    cell.status_lbl.text=str;
    cell.customer_name_lbl.textColor=[ThemeColor changeGraphColor];
    cell.customer_name_lbl.text=[ customerName objectAtIndex:indexPath.row];
    
    NSString *string = [date objectAtIndex:indexPath.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
  //  [date replaceObjectAtIndex:indexPath.row withObject:finalDate];
    cell.date_lbl.text=finalDate;
    
    
    cell.price_lbl.text=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:indexPath.row]floatValue ]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    pathforseague=indexPath;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(isRefund)
        [self performSegueWithIdentifier:@"refund" sender:self];
    else
        [self performSegueWithIdentifier:@"void" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
//    NSIndexPath *path = [self.records indexPathForSelectedRow];
    RefundViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.index = pathforseague.row;
    vcToPushTo.customerName=[customerName objectAtIndex:pathforseague.row];
    vcToPushTo.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    
    NSString *string = [date objectAtIndex:pathforseague.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
    
    vcToPushTo.date=finalDate;
    vcToPushTo.transType=[transType objectAtIndex:pathforseague.row];
    vcToPushTo.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.cardTypetxt=@"N/A";
    else
        vcToPushTo.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.authCodetxt=@"N/A";
    else
        vcToPushTo.authCodetxt=[authCode objectAtIndex:pathforseague.row];
    //vcToPushTo.status=[status objectAtIndex:path.row];
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    vcToPushTo.applicationID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    vcToPushTo.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    vcToPushTo.stationID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
    
    
    VoidViewController *dec = segue.destinationViewController;
    dec.index = pathforseague.row;
    dec.customerName=[customerName objectAtIndex:pathforseague.row];
    dec.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    dec.date=finalDate;
    dec.transType=[transType objectAtIndex:pathforseague.row];
    dec.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.cardTypetxt=@"N/A";
    else
        dec.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.authCodetxt=@"N/A";
    else
        dec.authCodetxt=[authCode objectAtIndex:pathforseague.row];
 //   dec.status=[status objectAtIndex:path.row];
    dec.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    dec.applicationID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    dec.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    dec.stationID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
}

#pragma buttonACtions
//-(void)hidehud{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//}
- (IBAction)showRefund:(id)sender {
    isRefund=true;
    isVoid=false;
    self.RefundnVoidLbl.text=@"Refund Transaction";
    self.refundLbl.hidden=false;
    self.imgRefund.image=[UIImage imageNamed:@"Refund_Icon"];
    self.imgVoid.image=[UIImage imageNamed:@"Void_Icon_deactivate"];
    self.voidLbl.hidden=true;
}

- (IBAction)showVoid:(id)sender {
    
    isRefund=false;
    isVoid=true;
    self.RefundnVoidLbl.text=@"Void Transaction";
    self.refundLbl.hidden=true;
    self.imgRefund.image=[UIImage imageNamed:@"Refund_Icon_deactivate"];
    self.imgVoid.image=[UIImage imageNamed:@"Void_Icon"];
    self.voidLbl.hidden=false;
}
- (IBAction)activityAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)supportAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)search:(id)sender {
    if(isVoid)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        VoidSearchViewController *myVC = (VoidSearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidsearch"];
        [self.navigationController pushViewController:myVC animated:NO];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        RefundSearchViewController *myVC = (RefundSearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"refundsearch"];
        [self.navigationController pushViewController:myVC animated:NO];
    }
   
}
@end
