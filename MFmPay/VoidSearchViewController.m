//
//  VoidSearchViewController.m
//  MFmPay
//
//  Created by Prism Pay on 6/17/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//
#define MAX_LENGTH 19
#import "VoidSearchViewController.h"
#import "HSDatePickerViewController.h"
#import "SettingsViewController.h"
#import "SupportViewController.h"
#import "VoidRefundMainViewController.h"
#import "UIView+Toast.h"
#import "DropDownTableViewCell.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ActivityViewController.h"
#import "PaymentSelectionViewController.h"
#import "SearchResultViewController.h"
#import "CreditCardSaleViewController.h"
#import "VoidViewController.h"
#import "VoidRefundSearchViewController.h"
@interface VoidSearchViewController ()
{
    
    BOOL startDate;
    NSArray *TransTypeID;
}
@end

@implementation VoidSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleMe];
    [self setNumKeyPad];
    TransTypeID=[NSArray arrayWithObjects:@"Credit Card: Sale", nil];
    UITapGestureRecognizer *startTap = [[UITapGestureRecognizer alloc]
                                        initWithTarget:self action:@selector(startDate)];
    [self.start_date addGestureRecognizer: startTap];
    
    
    UITapGestureRecognizer *endTap = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(endDate)];
    [self.end_date addGestureRecognizer: endTap];
    
//    UITapGestureRecognizer *Ttype = [[UITapGestureRecognizer alloc]
//                                     initWithTarget:self action:@selector(selectType)];
//    [self.transaction_type addGestureRecognizer: Ttype];
    // Do any additional setup after loading the view.
}
#pragma Tap Gestures Functions
-(void)startDate{
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    startDate=true;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate=self;
    [self presentViewController:hsdpvc animated:YES completion:nil];
}
-(void)endDate{
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    startDate=false;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate=self;
    [self presentViewController:hsdpvc animated:YES completion:nil];
}
-(void)selectType{
    [UIView animateWithDuration:.5f animations:^{
        
        self.dropDownHeight.constant=200;
        [self.dropDown layoutIfNeeded];
    }];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.transaction_number_field.text.length >= MAX_LENGTH && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {return YES;}
}
#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd";
    if(startDate)
    {
        self.start_date_label.textColor=[UIColor blackColor];
        self.start_date_label.text = [dateFormater stringFromDate:date];
    }
    else
    {
        self.end_date_label.text=[dateFormater stringFromDate:date];
        self.end_date_label.textColor=[UIColor blackColor];
    }
}



-(void)setNumKeyPad{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.transaction_number_field.inputAccessoryView = numberToolbar;
    
}
-(void)cancelNumberPad{
    [self.transaction_number_field resignFirstResponder];
    self.transaction_number_field.text = @"";
}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.transaction_number_field resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)styleMe{
    self.customer_name_field.delegate=self;
    self.transaction_number_field.delegate=self;
    self.clear_btn.backgroundColor=[ThemeColor changeGraphColor];
    self.clear_btn.layer.cornerRadius =  self.clear_btn.bounds.size.width/2;
    self.clear_btn.layer.masksToBounds = YES;
    
    self.search_Btn.backgroundColor=[ThemeColor changeGraphColor];
    self.search_Btn.layer.cornerRadius =  self.search_Btn.bounds.size.width/2;
    self.search_Btn.layer.masksToBounds = YES;
    
    
    self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.creditBtn.layer.cornerRadius = 10;
    self.creditBtn.layer.masksToBounds = YES;
    
    
    self.header_view.backgroundColor=[ThemeColor changeGraphColor];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(BOOL)validate{
    
    if([self.customer_name_field.text isEqualToString:@""] &&[self.transaction_number_field.text isEqualToString:@""]&&[self.start_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")]&&[self.end_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")]&&[self.transaction_type_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [self.view makeToast:NSLocalizedString(@"All fields are empty", @"All fields are empty")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if([self.start_date_label.text isEqualToString:@""]&&![self.end_date_label.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Enter Start Date", @"Enter Start Date")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self.start_date_label.text isEqualToString:@""]&&[self.end_date_label.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Enter End Date", @"Enter End Date")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self.start_date_label.text isEqualToString:@""]&&![self.end_date_label.text isEqualToString:@""])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // this is imporant - we set our input date format to match our input string
        // if format doesn't match you'll get nil from your string, so be careful
        
        [dateFormatter setDateFormat:@"MMM/dd/yyyy"];
        NSDate *date1 = [[NSDate alloc] init];
        NSDate *date2 = [[NSDate alloc] init];
        // voila!
        date1 = [dateFormatter dateFromString:self.start_date_label.text];
        date2=[dateFormatter dateFromString:self.end_date_label.text];
        
        
        if ([date1 compare:date2] == NSOrderedDescending) {
            NSLog(@"date1 is later than date2");
            [self.view makeToast:NSLocalizedString(@"Start date should be earlier than end date", @"Start date should be earlier than end date")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            return false;
        }
        return true;
    }
    return true;
}
- (IBAction)search:(id)sender {
    if([self validate])
        [self performSegueWithIdentifier:@"Voidresults" sender:self];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    VoidRefundSearchViewController *svc=segue.destinationViewController;
    svc.startDate=self.start_date_label.text;
    svc.endDate=self.end_date_label.text;
    svc.transTypeID=self.transaction_number_field.text;
    svc.isRefund=false;
    svc.isVoid=true;
//    if([self.transaction_type_label.text isEqualToString:@"Credit Sale"])
//        svc.transTypeID=@"1";
    [self.transaction_type_label.text isEqualToString:@"Void"];
//        svc.transTypeID=@"2";
//    else if([self.transaction_type_label.text isEqualToString:@"Refund"])
//        svc.transTypeID=@"3";
//    else
//        svc.transTypeID=@"";
    //    svc.transTypeID=self.transaction_type_label.text;
    if([self.customer_name_field.text isEqualToString:@""])
        svc.customerNamestr=@"";
    else
    svc.customerNamestr=self.customer_name_field.text;
    
    
}
- (IBAction)clear_form:(id)sender {
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    self.end_date_label.textColor=[UIColor lightGrayColor];
    self.start_date_label.textColor=[UIColor lightGrayColor];
    self.transaction_type_label.textColor=[UIColor lightGrayColor];
    self.end_date_label.text=NSLocalizedString(@"Not Set",@"Not Set");
    self.start_date_label.text=NSLocalizedString(@"Not Set",@"Not Set");
    self.customer_name_field.text=@"";
//    self.transaction_type_label.text=NSLocalizedString(@"Not Set",@"Not Set");
    self.transaction_number_field.text=@"";
}

- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)activity:(id)sender {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
        [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settingsAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)supportAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [TransTypeID count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"dropDown";
    
    
    DropDownTableViewCell *cell = (DropDownTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.contentView.backgroundColor=[ThemeColor changeGraphColor];
    cell.txtLabel.text=[TransTypeID objectAtIndex:indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [UIView animateWithDuration:.5f animations:^{
        
        self.dropDownHeight.constant=0;
        [self.dropDown layoutIfNeeded];
    }];
    self.transaction_type_label.text=[TransTypeID objectAtIndex:indexPath.row];
    self.transaction_type_label.textColor=[UIColor blackColor];
}

#pragma keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.customer_name_field resignFirstResponder];
    
    return YES;
}
- (IBAction)back:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    VoidViewController *myVC = (VoidViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
//    [self.navigationController pushViewController:myVC animated:NO];
    
    [self.navigationController popViewControllerAnimated:NO];
}


@end
