//
//  SearchResultViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/24/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SupportViewController.h"
#import "SettingsViewController.h"
#import "VoidRefundMainViewController.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "PaymentSelectionViewController.h"
#import "AFNetworking.h"
#import "HistoryTableViewCell.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "SearchViewController.h"
#import "CreditCardSaleViewController.h"
@interface SearchResultViewController ()
{
    NSString *url;
    NSString *pageCount;
    int count;
    
   
    NSMutableArray *customerName;
    NSMutableArray *status;
    NSMutableArray *amount;
    NSMutableArray *date;
    NSMutableArray *transType;
    NSMutableArray *cardType;
    NSMutableArray *authCode;
    NSMutableArray *cardNum;
    NSMutableArray *historyID;
    NSMutableArray *numOfTrans;
    NSMutableArray *amountofTrans;
    NSMutableArray *stationID;
    NSMutableArray *merchantID;
    NSMutableArray *applicationID;
    NSIndexPath *pathforseague;
    NSString *mcsacctID;

}
@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    url  =[NSString stringWithFormat:@"%@Report/Transaction",sharedManager.url];
//    url  =@"https://demo.prismpay.com/API/Report/Transaction";
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
    pageCount=@"50";
    count=50;
    [self styleMe];
[self getService];
}

-(void)styleMe{
    self.header_view.backgroundColor=[ThemeColor changeGraphColor];
    self.activity_Btn.backgroundColor=[ThemeColor changeGraphColor];
    self.activity_Btn.layer.cornerRadius = 10;
    self.activity_Btn.layer.masksToBounds = YES;
}
-(void)getService{
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"%@",self.transTypeID);

    if(count<51)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
            NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.startDate,@"StartDate",mcsacctID, @"MCSAccountID",self.endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",self.customerNamestr,@"CardHolderName",self.transID,@"GatewayTransactionID",self.transTypeID,@"TransactionTypeID",self.transStatusID,@"TransactionStatusID"  , nil];
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithDictionary:params];
            NSLog(@"%@",self.transStatusID);
            if([self.customerNamestr isEqualToString:@""])
                [dic removeObjectForKey:@"CardHolderName"];
            if([self.transTypeID isEqualToString:@""])
               [dic removeObjectForKey:@"TransactionTypeID"];
            if([self.transStatusID isEqualToString:@""])
                [dic removeObjectForKey:@"TransactionStatusID"];
            if([self.customerNamestr isEqualToString:@""])
                [dic removeObjectForKey:@"Name"];
            if([self.transID isEqualToString:@""])
                [dic removeObjectForKey:@"TransactionID"];
            if([self.startDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
            {
                [dic removeObjectForKey:@"StartDate"];
                [dic setObject:startDate forKey:@"StartDate"];
            }
            if([self.endDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
            {
                [dic removeObjectForKey:@"EndDate"];
                [dic setObject:endDate forKey:@"EndDate"];
            }
            params=dic;
            
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                customerName=[[NSMutableArray alloc]init];
                status=[[NSMutableArray alloc]init];
                amount=[[NSMutableArray alloc]init];
                date=[[NSMutableArray alloc]init];
                transType=[[NSMutableArray alloc]init];
                cardType=[[NSMutableArray alloc]init];
                authCode=[[NSMutableArray alloc]init];
                cardNum=[[NSMutableArray alloc]init];
                historyID=[[NSMutableArray alloc]init];
                applicationID=[[NSMutableArray alloc]init];
                stationID=[[NSMutableArray alloc]init];
                merchantID=[[NSMutableArray alloc]init];
                NSArray *arr=[responseObject valueForKey:@"Object"];
                
                 if (arr != (id)[NSNull null] && [arr count] != 0)
                for(NSArray *dic in arr)
                {
//                    [customerName addObject:[dic valueForKey:@"CardHolderName"]];
//                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
//                    [amount addObject:[dic valueForKey:@"Amount"]];
//                    [date addObject:[dic valueForKey:@"TransactionDate"]];
//                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
//                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
//                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
//                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
//                    [historyID addObject:[dic valueForKey:@"TransactionID"]];
                    
                    
                    
                    if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                        [customerName addObject:@"N/A"];
                    else
                        [customerName addObject:[dic valueForKey:@"CardHolderName"]];
                    
                    
                    if([dic valueForKey:@"TransactionStatusID"]== (id)[NSNull null])
                        [status addObject:@"N/A"];
                    else
                        [status addObject:[dic valueForKey:@"TransactionStatusID"]];
                    
                    
                    if([dic valueForKey:@"Amount"]== (id)[NSNull null])
                        [amount addObject:@"N/A"];
                    else
                        [amount addObject:[dic valueForKey:@"Amount"]];
                    
                    
                    if([dic valueForKey:@"TransactionDate"]== (id)[NSNull null])
                        [date addObject:@"N/A"];
                    else
                        [date addObject:[dic valueForKey:@"TransactionDate"]];
                    
                    
                    
                    if([dic valueForKey:@"TransactionTypeName"]== (id)[NSNull null])
                        [transType addObject:@"N/A"];
                    else
                        [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
                    
                    
                    
                    if([dic valueForKey:@"CardTypeName"]== (id)[NSNull null])
                        [cardType addObject:@"N/A"];
                    else
                        [cardType addObject:[dic valueForKey:@"CardTypeName"]];
                    
                    
                    
                    if([dic valueForKey:@"AuthCode"]== (id)[NSNull null])
                        [authCode addObject:@"N/A"];
                    else
                        [authCode addObject:[dic valueForKey:@"AuthCode"]];
                    
                    
                    
                    if([dic valueForKey:@"Last4Digit"]== (id)[NSNull null])
                        [cardNum addObject:@"N/A"];
                    else
                        [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
                    
                    
                    
                    if([dic valueForKey:@"GatewayTransactionID"]== (id)[NSNull null])
                        [historyID addObject:@"N/A"];
                    else
                        [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
                    
                    
                    
                    if([dic valueForKey:@"ApplicationID"]== (id)[NSNull null])
                        [applicationID addObject:@"N/A"];
                    else
                        [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
                    
                    
                    
                    if([dic valueForKey:@"StationID"]== (id)[NSNull null])
                        [stationID addObject:@"N/A"];
                    else
                        [stationID addObject:[dic valueForKey:@"StationID"]];
                    
                    
                    
                    
                    if([dic valueForKey:@"MerchantID"]== (id)[NSNull null])
                        [merchantID addObject:@"N/A"];
                    else
                        [merchantID addObject:[dic valueForKey:@"MerchantID"]];
                    

                    
                    
                    
                    
                    
                }
                if([customerName count]>0)
                    self.blank_view.hidden=true;
                if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
                while (count<[[responseObject valueForKey:@"Custom"]intValue])
                    
                    
                {
                    
                    count+=50;
                    pageCount=[NSString stringWithFormat:@"%d",count];
                    
                    [self getService1];
                    
                    
                }
                [self.result reloadData];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
            
        });
    });
    
}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)getService1{
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];

    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
    
    NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.startDate,@"StartDate",self.endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",self.customerNamestr,@"Name",self.transTypeID,@"GatewayTransactionID",self.transID,@"TransactionID",self.transStatusID,@"TransactionStatusID",mcsacctID,@"MCSAccountID", nil];
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithDictionary:params];
    if([self.transTypeID isEqualToString:@""])
        [dic removeObjectForKey:@"TransactionStatusID"];
    if([self.transTypeID isEqualToString:@""])
        [dic removeObjectForKey:@"TransactionTypeID"];
    if([self.customerNamestr isEqualToString:@""])
        [dic removeObjectForKey:@"Name"];
    if([self.transID isEqualToString:@""])
        [dic removeObjectForKey:@"TransactionID"];
    if([self.startDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [dic removeObjectForKey:@"StartDate"];
        [dic setObject:startDate forKey:@"StartDate"];
    }
    if([self.endDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [dic removeObjectForKey:@"EndDate"];
        [dic setObject:endDate forKey:@"EndDate"];
    }
    params=dic;
    
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        customerName=[[NSMutableArray alloc]init];
        status=[[NSMutableArray alloc]init];
        amount=[[NSMutableArray alloc]init];
        date=[[NSMutableArray alloc]init];
        transType=[[NSMutableArray alloc]init];
        cardType=[[NSMutableArray alloc]init];
        authCode=[[NSMutableArray alloc]init];
        cardNum=[[NSMutableArray alloc]init];
        historyID=[[NSMutableArray alloc]init];
        applicationID=[[NSMutableArray alloc]init];
        stationID=[[NSMutableArray alloc]init];
        merchantID=[[NSMutableArray alloc]init];
        NSArray *arr=[responseObject valueForKey:@"Object"];
        
        
        for(NSArray *dic in arr)
        {
            //                    [customerName addObject:[dic valueForKey:@"CardHolderName"]];
            //                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
            //                    [amount addObject:[dic valueForKey:@"Amount"]];
            //                    [date addObject:[dic valueForKey:@"TransactionDate"]];
            //                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
            //                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
            //                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
            //                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
            //                    [historyID addObject:[dic valueForKey:@"TransactionID"]];
            
            
            
            if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                [customerName addObject:@"N/A"];
            else
                [customerName addObject:[dic valueForKey:@"CardHolderName"]];
            
            
            if([dic valueForKey:@"TransactionStatusID"]== (id)[NSNull null])
                [status addObject:@"N/A"];
            else
                [status addObject:[dic valueForKey:@"TransactionStatusID"]];
            
            
            if([dic valueForKey:@"Amount"]== (id)[NSNull null])
                [amount addObject:@"N/A"];
            else
                [amount addObject:[dic valueForKey:@"Amount"]];
            
            
            if([dic valueForKey:@"TransactionDate"]== (id)[NSNull null])
                [date addObject:@"N/A"];
            else
                [date addObject:[dic valueForKey:@"TransactionDate"]];
            
            
            
            if([dic valueForKey:@"TransactionTypeName"]== (id)[NSNull null])
                [transType addObject:@"N/A"];
            else
                [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
            
            
            
            if([dic valueForKey:@"CardTypeName"]== (id)[NSNull null])
                [cardType addObject:@"N/A"];
            else
                [cardType addObject:[dic valueForKey:@"CardTypeName"]];
            
            
            
            if([dic valueForKey:@"AuthCode"]== (id)[NSNull null])
                [authCode addObject:@"N/A"];
            else
                [authCode addObject:[dic valueForKey:@"AuthCode"]];
            
            
            
            if([dic valueForKey:@"Last4Digit"]== (id)[NSNull null])
                [cardNum addObject:@"N/A"];
            else
                [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
            
            
            
            if([dic valueForKey:@"TransactionID"]== (id)[NSNull null])
                [historyID addObject:@"N/A"];
            else
                [historyID addObject:[dic valueForKey:@"TransactionID"]];
            
            
            
            if([dic valueForKey:@"ApplicationID"]== (id)[NSNull null])
                [applicationID addObject:@"N/A"];
            else
                [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
            
            
            
            if([dic valueForKey:@"StationID"]== (id)[NSNull null])
                [stationID addObject:@"N/A"];
            else
                [stationID addObject:[dic valueForKey:@"StationID"]];
            
            
            
            
            if([dic valueForKey:@"MerchantID"]== (id)[NSNull null])
                [merchantID addObject:@"N/A"];
            else
                [merchantID addObject:[dic valueForKey:@"MerchantID"]];
            
            
            
            
            
            
            
        }
        if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
        while (count<[[responseObject valueForKey:@"Custom"]intValue])
            
            
        {
            
            count+=50;
            pageCount=[NSString stringWithFormat:@"%d",count];
            
            [self getService1];
            
            
        }
        [self.result reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"error: %@", error);
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settingsAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)supportAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}


#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ customerName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pos";
    
    
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //    if([[[ActivityData statusArr]objectAtIndex:indexPath.row]isEqualToString:@"Declined"])
    //        cell.status_lbl.textColor=[UIColor redColor];
    NSString *str;
    
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
    if([str isEqualToString:@"0"] )
    {
        
        //[status replaceObjectAtIndex:indexPath.row withObject:@"Approved"];
        str=@"Approved";
    }
    else
    {
        // [status replaceObjectAtIndex:indexPath.row withObject:@"Declined"];
        cell.status_lbl.textColor=[UIColor redColor];
        str=@"Declined";
    }
    
    cell.status_lbl.text=str;
      cell.customer_name_lbl.textColor=[ThemeColor changeGraphColor];
    cell.customer_name_lbl.text=[ customerName objectAtIndex:indexPath.row];
    
    NSString *string = [date objectAtIndex:indexPath.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    [date replaceObjectAtIndex:indexPath.row withObject:preTel];
    cell.date_lbl.text=preTel;
    
    
    cell.price_lbl.text=[NSString stringWithFormat:@"%.02f",[[amount objectAtIndex:indexPath.row]floatValue ]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str;
    pathforseague=indexPath;
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
    if([str isEqualToString:@"1"]||[str isEqualToString:@"N/A"] )
        [self performSegueWithIdentifier:@"declined" sender:self];
    else
        [self performSegueWithIdentifier:@"approved" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
//    NSIndexPath *path = [self.result indexPathForSelectedRow];
    ApprovedViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.index = pathforseague.row;
    vcToPushTo.customerName=[customerName objectAtIndex:pathforseague.row];
    vcToPushTo.amountTxt=[NSString stringWithFormat:@"%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    vcToPushTo.date=[date objectAtIndex:pathforseague.row];
    vcToPushTo.transType=[transType objectAtIndex:pathforseague.row];
    vcToPushTo.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    vcToPushTo.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.authCodetxt=@"N/A";
    else
        vcToPushTo.authCodetxt=[authCode objectAtIndex:pathforseague.row];
    // vcToPushTo.status=[status objectAtIndex:path.row];
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    vcToPushTo.appID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    vcToPushTo.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    vcToPushTo.acctTypeID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
    
    
    DeclindedViewController *dec = segue.destinationViewController;
    dec.index = pathforseague.row;
    dec.customerName=[customerName objectAtIndex:pathforseague.row];
    dec.amountTxt=[NSString stringWithFormat:@"%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    dec.date=[date objectAtIndex:pathforseague.row];
    dec.transType=[transType objectAtIndex:pathforseague.row];
    dec.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    dec.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.authCodetxt=@"N/A";
    else
        dec.authCodetxt=[authCode objectAtIndex:pathforseague.row];
    //  dec.status=[status objectAtIndex:path.row];
    dec.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    dec.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    dec.appID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    dec.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    dec.acctTypeID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
}



- (IBAction)back:(id)sender {
   
    [self.navigationController popViewControllerAnimated:NO];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    SearchViewController *myVC = (SearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"search"];
//    [self.navigationController pushViewController:myVC animated:NO];
}
@end
