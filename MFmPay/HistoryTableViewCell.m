//
//  HistoryTableViewCell.m
//  MFmPay
//
//  Created by Prism Pay on 4/26/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
