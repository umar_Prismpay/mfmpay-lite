//
//  RefundViewController.h
//  MFmPay
//
//  Created by Prism Pay on 5/9/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface RefundViewController : BaseViewController<UITextFieldDelegate>
@property(nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UILabel *datelbl;
@property (weak, nonatomic) IBOutlet UILabel *saleType;
@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *cardType;
@property (weak, nonatomic) IBOutlet UILabel *cardNum;
@property (weak, nonatomic) IBOutlet UILabel *historyID;
@property (weak, nonatomic) IBOutlet UILabel *orderID;
@property (weak, nonatomic) IBOutlet UILabel *acctID;
@property (weak, nonatomic) IBOutlet UILabel *subID;
@property (weak, nonatomic) IBOutlet UILabel *authCode;

- (IBAction)search:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *blurView;


@property (weak, nonatomic) IBOutlet UIButton *creditBtn;

@property (weak, nonatomic) IBOutlet UIButton *refundBtn;
- (IBAction)showView:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *refund_Price_view;

@property (weak, nonatomic) IBOutlet UITextField *amountTxtField;
- (IBAction)settings:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *tickBtn;

- (IBAction)go_Refund:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *amount;
- (IBAction)activities:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *amount_header;
- (IBAction)back:(id)sender;

- (IBAction)support:(id)sender;

//Get values
@property (strong, nonatomic)NSString *customerName;
@property (strong, nonatomic)NSString *status;
@property (strong, nonatomic)NSString *amountTxt;
@property (strong, nonatomic)NSString *date;
@property (strong, nonatomic)NSString *transType;
@property (strong, nonatomic)NSString *cardTypetxt;
@property (strong, nonatomic)NSString *authCodetxt;
@property (strong, nonatomic)NSString *cardNumtxt;
@property (strong, nonatomic)NSString *historyIDtxt;
@property (strong, nonatomic)NSString *stationID;
@property (strong, nonatomic)NSString *merchantID;
@property (strong, nonatomic)NSString *applicationID;
- (IBAction)payAction:(id)sender;


@end
