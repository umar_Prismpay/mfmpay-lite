//
//  DeclindedViewController.m
//  MFmPay
//
//  Created by Prism Pay on 4/28/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "DeclindedViewController.h"

#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "ActivityViewController.h"
#import "PaymentSelectionViewController.h"
@interface DeclindedViewController ()

@end

@implementation DeclindedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    [self setfield];
    [self styleME];
    
    NSString *controller=[NSString stringWithFormat:@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]];
    NSArray *myArray = [controller componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
    controller=[NSString stringWithFormat:@"%@",[myArray objectAtIndex:0]];
    if([controller isEqualToString:@"<CreditCardSaleViewController"])
    {
        self.backBtn.hidden=true;
        [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
        [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    if([controller isEqualToString:@"<RefundViewController"])
    {
        self.backBtn.hidden=true;
        [self.creditBtn setImage:[UIImage imageNamed:@"Credit_Icon_white"] forState:UIControlStateNormal];
        self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
        self.creditBtn.layer.cornerRadius = 10;
        self.creditBtn.layer.masksToBounds = YES;
        [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    if([controller isEqualToString:@"<VoidViewController"])
    {
        self.backBtn.hidden=true;
        [self.creditBtn setImage:[UIImage imageNamed:@"Credit_Icon_white"] forState:UIControlStateNormal];
        self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
        self.creditBtn.layer.cornerRadius = 10;
        self.creditBtn.layer.masksToBounds = YES;
        [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    // Do any additional setup after loading the view.
}
-(void)setfield{
    
    if([_date isEqualToString:@"<null>"]||[_date isEqualToString:@""])
        _date=@"N/A";
    
    if([_transType isEqualToString:@"<null>"]||[_transType isEqualToString:@""])
        _transType=@"N/A";
    
    if([_customerName isEqualToString:@"<null>"]||[_customerName isEqualToString:@""])
        _customerName=@"N/A";
    
    if([_amountTxt isEqualToString:@"<null>"]||[_amountTxt isEqualToString:@""])
        _amountTxt=@"N/A";
    
    if([_cardTypetxt isEqualToString:@"<null>"]||[_cardTypetxt isEqualToString:@""])
        _cardTypetxt=@"N/A";
    
    if([_cardNumtxt isEqualToString:@"<null>"]||[_cardNumtxt isEqualToString:@""])
        _cardNumtxt=@"N/A";
    
    if([_historyIDtxt isEqualToString:@"<null>"]||[_historyIDtxt isEqualToString:@""])
        _historyIDtxt=@"N/A";
    
    if([_acctTypeID isEqualToString:@"<null>"]||[_acctTypeID isEqualToString:@""])
        _acctTypeID=@"N/A";
    
    if([_merchantID isEqualToString:@"<null>"]||[_merchantID isEqualToString:@""])
        _merchantID=@"N/A";
    
    if([_authCodetxt isEqualToString:@"<null>"]||[_authCodetxt isEqualToString:@""])
        _authCodetxt=@"N/A";
    
    
    self.datelbl.text=_date;
    self.saleType.text=_transType;
    self.custName.text=_customerName;
    self.amount.text=_amountTxt;
    self.cardType.text=_cardTypetxt;
    self.cardNum.text=_cardNumtxt;
    self.historyID.text=_historyIDtxt;
    self.orderID.text=_appID;
    self.acctID.text=_acctTypeID;
    self.subID.text=_merchantID;
    self.authCode.text=_authCodetxt;
}
-(void)styleME
{
    self.activityBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.activityBtn.layer.cornerRadius = 10;
    self.activityBtn.layer.masksToBounds = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)shouldAutorotate {
    return NO;
}
- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)back:(id)sender {
     [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)activity:(id)sender {
    NSString *controller=[NSString stringWithFormat:@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]];
    NSArray *myArray = [controller componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
    controller=[NSString stringWithFormat:@"%@",[myArray objectAtIndex:0]];
    if([controller isEqualToString:@"<CreditCardSaleViewController"]||[controller isEqualToString:@"<RefundViewController"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
        [self.navigationController pushViewController:myVC animated:NO];
    }
}
@end
