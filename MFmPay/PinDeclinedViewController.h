//
//  PinDeclinedViewController.h
//  MFmPay
//
//  Created by Prism Pay on 6/14/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface PinDeclinedViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *label_view;

- (IBAction)payAction:(id)sender;

- (IBAction)settings:(id)sender;
- (IBAction)support:(id)sender;
- (IBAction)credit:(id)sender;
- (IBAction)activity:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *approvalCode;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *refNumber;

@property (weak, nonatomic) IBOutlet UILabel *mcsTransID;
@property (weak, nonatomic) IBOutlet UILabel *procTransID;
@property (weak, nonatomic) NSString * saleType;
- (IBAction)back:(id)sender;

//Get values
@property (strong, nonatomic)NSString *MCSTransID;
@property (strong, nonatomic)NSString *ProceTransID;
@property (strong, nonatomic)NSString *amounttxt;
@property (strong, nonatomic)NSString *RefNum;
@property (strong, nonatomic)NSString *ProcAppCode;


@end
