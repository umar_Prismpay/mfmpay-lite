//
//  PAXSSLManager.m
//  PAXController
//
//  Created by Aamir Jelani on 28/04/2015.
//  Copyright (c) 2015 AppiLancers. All rights reserved.
//

#import "MP200.h"
#import "ProgressHUD.h"
@interface MP200 ()
{
    NSString * nssReadyToShow_Data;
    uint32_t _totalBytesRead;
    
}

@end

@implementation MP200


-(void)MP200BTCommunicationSetup {
    NSLog(@"0. MP200BTCommunicationSetup");
    
    self.bltController = [EADSessionController sharedController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
    [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    
    // watch for received data from the accessory
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_sessionDataReceived:) name:EADSessionDataReceivedNotification object:nil];
    
}

- (BOOL) launchMP200Device {
    {
        NSLog(@"3. launchMP200Device");
        
        NSUInteger count_acc=[[[EAAccessoryManager sharedAccessoryManager] connectedAccessories] count];
        if(count_acc > 0){
            
            [ProgressHUD show:@"Connecting with MP200"];
            
            
            for (EAAccessory *item in [[EAAccessoryManager sharedAccessoryManager] connectedAccessories]) {
                if([item.name isEqualToString:@"MP200"])
                {
                    
                    self.accessory = item;
                    if(![item.protocolStrings count])
                        continue;
                    
                    NSLog(@"Device Session %lu",(unsigned long)[self.bltController.accessory connectionID]);
                    
                    [self.bltController setupControllerForAccessory:self.accessory
                                                 withProtocolString:item.protocolStrings[0]];
                    [self.bltController openSession];
                    
                    NSLog(@"item protocol %@",item.protocolStrings[0]);
                    return TRUE;
                    //HUD.detailsLabelText=[NSString stringWithFormat:@"Connecting with MP200"];
                    
                }
            }
            
        }else{
            [ProgressHUD showError:@"Cannot find a MP200 Device.\nPlease connect a MP200 Device or reboot \nSettings->Blutooth->MPOS..."];
        }
        return FALSE;
    }
}
- (void)sendHex:(NSString *)amount :(NSString *)transType
{
    NSLog(@"2. sendHex");
    
    //if([self launchMP200Device]){
        
        [ProgressHUD show:@"Processing with MP200"];
        
        NSString *accid=@"py7l4";
        NSString *subid=@"POS01";
        NSString *mpin=@"1234";
    
    
        NSString *serviceNo=@"2";
        NSString *serviceType=@"nor";   //nor: normal sp:  sales promotion
        NSString *qps=@"0";   //0 or 1
        
        //billing info
        NSString *billAddress1=@"";
        NSString *billZip=@"";
        
        //sales promotion
        NSString *dm=@"0";   //deferel month
        NSString *rm=@"0";   //recure month
        NSString *pp=@"0";   // payment plan
        NSString *bpf=@"0";     //blilling plan
        
        //custom fields
        NSString *custom1=@"1";
        NSString *custom2=@"2";
        NSString *custom3=@"3";
        NSString *custom4=@"4";
        NSString *custom5=@"5";
        NSString *custom6=@"6";

    
    NSString *mp200XmlRequest=[NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?><mc><amt>%@</amt><accid>%@</accid><subid>%@</subid><mpin>%@</mpin><sno>%@</sno><st>%@</st><qps>%@</qps><badd1>%@</badd1><bzip>%@</bzip><c1>%@</c1><c2>%@</c2><c3>%@</c3><c4>%@</c4><c5>%@</c5><c6>%@</c6></mc>",amount,accid,subid,mpin,serviceNo,serviceType,qps,billAddress1,billZip,custom1,custom2,custom3,custom4,custom5,custom6];
    
    
           //NSString *amt=@"2.58";
        
        char *CMDAMT;
        if([transType isEqualToString:@"insertCard"])
        {
            CMDAMT = [mp200XmlRequest CMD_Insert_Card];
        }
        if([transType isEqualToString:@"swipeCard"])
        {
            CMDAMT = [mp200XmlRequest CMD_Swipe_Card];
        }
        
        NSString *NS_CMDAMT = [NSString stringWithFormat:@"%s", CMDAMT];
        char *buf = [NS_CMDAMT Send_DLE];
        
        
        uint32_t stringLen = strlen(buf);
        
        for (uint32_t i = 0; i < stringLen; i++) {
            NSLog(@"in view %02X ", buf[i]);
        }
        
        if (buf)
        {
            uint32_t len = strlen(buf) + 1;
            [[EADSessionController sharedController] writeData:[NSData dataWithBytes:buf length:len]];
        }
    //}
}

- (void)_sessionDataReceived:(NSNotification *)notification
{
    NSLog(@"4. _sessionDataReceived");
    
    [ProgressHUD dismiss];
    EADSessionController *sessionController = (EADSessionController *)[notification object];
    uint32_t bytesAvailable = 0;
    NSString* nssReal_Data = @"";
    //NSString* nssResult_1 = @"Result: APPROVAL";
    NSString* nssResult_1 = @"";
    NSString* nssResult_2 = @"";
    NSString* nssResult_3 = @"";
    NSString* nssResult_4 = @"";
    NSString* nssTID = @"";
    NSString* nssCardNO = @"";
    NSString* nssCardType = @"";
    NSString* Temp_i = @"";
    nssReadyToShow_Data = @"";
    //nssReal_Data can recieve the data with out DLE
    
    while ((bytesAvailable = [sessionController readBytesAvailable]) > 0) {
        NSData *data = [sessionController readData:bytesAvailable];
        
        NSString* nssDatabuf = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //nssDatabuf = origianl data
        
        nssReal_Data = [nssDatabuf Recv_DLE_WithData];  //Get Realdata(3 char need to be takeoff)
        ushort R_usLen = [nssReal_Data length];
        unichar Ch_Result = [nssReal_Data characterAtIndex:0];//get 1th Character of nssReal_Data
        int Count_3B = 0;  //count the number of ";"
        
        if (Ch_Result == '\x01' ) {
            
            
            for ( int i = 3; i < R_usLen; i++)
            {
                
                unichar Character_i = [nssReal_Data characterAtIndex:i];
                Temp_i = [NSString stringWithFormat:@"%C", Character_i];//convert unichar to NSString
                
                if(Character_i == '\x3B') {
                    Count_3B++;
                    continue;
                }
                
                if (Count_3B == 0) {
                    nssTID = [nssTID stringByAppendingString:Temp_i];
                }
                else if (Count_3B == 1) {
                    nssCardNO = [nssCardNO stringByAppendingString:Temp_i];
                }
                else{  //Count_3B == 2
                    nssCardType = [nssCardType stringByAppendingString:Temp_i];
                }
            }
            
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString: [NSString stringWithFormat: @"%@\n", nssResult_1]];
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString: [NSString stringWithFormat: @"%@\n", nssTID]];
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString: [NSString stringWithFormat: @"%@\n", nssCardNO]];
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString: [NSString stringWithFormat: @"%@", nssCardType]];
            
        }
        
        else if(Ch_Result == '\x02' ){
            
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString:nssResult_2];
        }
        else if(Ch_Result == '\x03' ){
            
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString:nssResult_3];
        }
        else if(Ch_Result == '\x04' ){
            
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString:nssResult_4];
        }
        else{  //nssReal_Data = Cancel or Error
            nssReadyToShow_Data = [nssReadyToShow_Data stringByAppendingString:nssReal_Data];;
        }
        
        if (data) {
            //NSLog(@"Rev_DLE %s", [nssDatabuf UTF8String]);
            NSLog(@"Rev_DLE_ %@", data);
            NSLog(@"Rev_DLE_nssDatabuf %@", nssDatabuf);
            _totalBytesRead += bytesAvailable;
            
        }
    }
    NSString *info=[NSString stringWithFormat:@"%@", nssReadyToShow_Data];
    NSLog(@"Info %@",[NSString stringWithFormat:@"%@", nssReadyToShow_Data]);
    if([info isEqualToString:@"Transaction Cancel"])
    {
        
        [ProgressHUD showError:info];
        [self.mp200d MP200ResponseCompleted];
        
    }else{
        if(![info isEqualToString:@""]){
            NSString *responseXml=info;
            responseXml=[responseXml stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            [self.mp200d MP200ResponseCompleted];
            [self.mp200d processMP200Response:responseXml];
            
        }
        
        
    }
    
    
    [self closeMp200BlutoothSession];
    
}
-(void)_accessoryDidConnect:(id)sender
{
    [ProgressHUD showSuccess:@"Device Connected"];
}
-(void)_accessoryDidDisconnect:(id)sender
{
    [ProgressHUD showError:@"Device Disconnected"];
}
-(void)closeMp200BlutoothSession{
    NSLog(@"6. closeMp200BlutoothSession");
    
    [self.bltController closeSession];
}
@end
