







#import <Foundation/Foundation.h>
#import "EADSessionController.h"
#import "DELProtocol.h"


@protocol mp200delegate <NSObject>

-(void)MP200ResponseCompleted;
-(void)processMP200Response:(NSString *)xmlResponse;

@end

@interface MP200 : NSObject

@property (nonatomic, strong) EADSessionController *bltController;
@property (nonatomic, strong) EAAccessory *accessory;
@property (weak) id <mp200delegate> mp200d;

-(void)MP200BTCommunicationSetup;
- (void)sendHex:(NSString *)amount :(NSString *)transType;
- (BOOL)launchMP200Device;
- (void)closeMp200BlutoothSession;
@end
