//
//  VoidViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/9/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "VoidViewController.h"
#import "ThemeColor.h"
#import "UIView+Toast.h"
#import "ActivityViewController.h"
#import "SupportViewController.h"
#import "PaymentSelectionViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "SearchViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "XMLDictionary.h"
@interface VoidViewController ()
{
    NSString *refNum;
    NSString *refAmount;
    NSString *resultCode;
    NSString *mcsTransactionID;
    NSString *resultDetail;
    NSString *mcsacctID;
}
@end

@implementation VoidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
    [self setfield];
    [self styleme];
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.blurView addGestureRecognizer: tapRec];
    // Do any additional setup after loading the view.
}
-(void)tap:(UITapGestureRecognizer *)tapRec{
    self.blurView.hidden=true;
    self.refund_Price_view.hidden=true;
    
}
-(void)styleme{
    self.view.backgroundColor=[UIColor whiteColor];
    self.send_rec_btn.backgroundColor=[ThemeColor changeGraphColor];
    
    self.send_rec_btn.layer.cornerRadius = self.send_rec_btn.bounds.size.width/2;
    self.send_rec_btn.layer.masksToBounds = YES;
    
    self.send_btn.backgroundColor=[ThemeColor changeGraphColor];
    self.send_btn.layer.cornerRadius = self.send_btn.bounds.size.width/2;
    self.send_btn.layer.masksToBounds = YES;
    
    self.sendReceiptLbl.textColor=[ThemeColor changeGraphColor];
    
    self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.creditBtn.layer.cornerRadius = 10;
    self.creditBtn.layer.masksToBounds = YES;
    
    self.tickBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.tickBtn.layer.cornerRadius = self.tickBtn.bounds.size.width/2;
    self.tickBtn.layer.masksToBounds = YES;
    
    
    self.refundBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.refundBtn.layer.cornerRadius = self.refundBtn.bounds.size.width/2;
    self.refundBtn.layer.masksToBounds = YES;
}
-(void)setfield{
    if([_date isEqualToString:@"<null>"]||[_date isEqualToString:@""])
        _date=@"N/A";
    
    if([_transType isEqualToString:@"<null>"]||[_transType isEqualToString:@""])
        _transType=@"N/A";
    
    if([_customerName isEqualToString:@"<null>"]||[_customerName isEqualToString:@""])
        _customerName=@"N/A";
    
    if([_amountTxt isEqualToString:@"<null>"]||[_amountTxt isEqualToString:@""])
        _amountTxt=@"N/A";
    
    if([_cardTypetxt isEqualToString:@"<null>"]||[_cardTypetxt isEqualToString:@""])
        _cardTypetxt=@"N/A";
    
    if([_cardNumtxt isEqualToString:@"<null>"]||[_cardNumtxt isEqualToString:@""])
        _cardNumtxt=@"N/A";
    
    if([_historyIDtxt isEqualToString:@"<null>"]||[_historyIDtxt isEqualToString:@""])
        _historyIDtxt=@"N/A";
    
    if([_stationID isEqualToString:@"<null>"]||[_stationID isEqualToString:@""])
        _stationID=@"N/A";
    
    if([_merchantID isEqualToString:@"<null>"]||[_merchantID isEqualToString:@""])
        _merchantID=@"N/A";
    
    if([_authCodetxt isEqualToString:@"<null>"]||[_authCodetxt isEqualToString:@""])
        _authCodetxt=@"N/A";
    
    
    self.datelbl.text=_date;
    self.saleType.text=_transType;
    self.custName.text=_customerName;
    self.amount.text=_amountTxt;
    self.cardType.text=_cardTypetxt;
    self.cardNum.text=_cardNumtxt;
    self.historyID.text=_historyIDtxt;
    self.orderID.text=_stationID;
    self.acctID.text=_merchantID;
    self.subID.text=_applicationID;
    self.authCode.text=_authCodetxt;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotate {
    return NO;
}


- (IBAction)showView:(id)sender {
//    [self.view makeToast:@"Transaction has been successfully Voided"
//                duration:3.0
//                position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                   title:nil
//                   image:nil
//                   style:nil
//              completion:^(BOOL didTap) {
//                  if (didTap) {
//                      NSLog(@"completion from tap");
//                  } else {
//                      NSLog(@"completion without tap");
//                  }
//              }];;
    //test.mycardstorage.com
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to Void this transaction" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
        NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
        NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
        NSString *soapmessage = [NSString stringWithFormat:@ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                 "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                                 "<soap12:Header>\n"
                                 "<AuthHeader xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<UserName>MFUser</UserName>\n"
                                 "<Password>slYC8T#fhnK0tLp5</Password>\n"
                                 "</AuthHeader>\n"
                                 "</soap12:Header>\n"
                                 "<soap12:Body>\n"
                                 "<CreditVoid_Token_Soap xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<creditCardVoid>\n"
                                 "<ServiceSecurity>\n"
                                 "<ServiceUserName>MF</ServiceUserName>\n"
                                 "<ServicePassword>kZJ33HgBhH$NFFdvE</ServicePassword>\n"
                                 "<MCSAccountID>%@</MCSAccountID>\n"
                                 "</ServiceSecurity>\n"
                                 "<TokenData>\n"
                                 "</TokenData>\n"
                                 "<TransactionData>\n"
                                 "<Amount>%@</Amount>\n"
                                 "<ReferenceNumber></ReferenceNumber>\n"
                                 "<MCSTransactionID>%@</MCSTransactionID>\n"
                                 "<GatewayID>1</GatewayID>\n"
                                 "</TransactionData>\n"
                                 "</creditCardVoid>\n"
                                 "</CreditVoid_Token_Soap>\n"
                                 "</soap12:Body>\n"
                                 "</soap12:Envelope>\n",mcsacctID,[NSString stringWithFormat:@"%.2f",[self.amountTxt floatValue]],self.historyIDtxt];
        
        
        
        
        NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
        
        
        [finalRequest addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [finalRequest setHTTPMethod:@"POST"];
        [finalRequest setHTTPBody:soapdata];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
        NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"Response string: %@",fetchedXML);
            
            NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
            NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
            NSDictionary *credit=[soap valueForKey:@"CreditVoid_Token_SoapResponse"];
            NSDictionary *CreditSale_SoapResult=[credit valueForKey:@"CreditVoid_Token_SoapResult"];
            refAmount=[CreditSale_SoapResult valueForKey:@"Amount"];
            mcsTransactionID=[CreditSale_SoapResult valueForKey:@"MCSTransactionID"];
            refNum=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
            NSDictionary *result=[CreditSale_SoapResult valueForKey:@"Result"];
            resultCode=[result valueForKey:@"ResultCode"];
            resultDetail=[result valueForKey:@"ResultDetail"];
            NSLog(@"%@",resultCode);
            //     amountTxt=[CreditSale_SoapResult valueForKey:@"Amount"];
            //     MCSTransactionID=[CreditSale_SoapResult valueForKey:@"MCSTransactionID"];
            //     ProcessorApprovalCode=[CreditSale_SoapResult valueForKey:@"ProcessorApprovalCode"];
            //     ProcessorTransactionID=[CreditSale_SoapResult valueForKey:@"ProcessorTransactionID"];
            //     ReferenceNumber=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
            //     if([resultCode isEqualToString:@"0"])
            //     {
            //     [self performSegueWithIdentifier:@"signApproved" sender:self];
            //     //[self performSegueWithIdentifier:@"realSign" sender:self];
            //     }
            //     else
            //     {
            //     [self performSegueWithIdentifier:@"pinDeclined" sender:self];
            //
            //     }
            if([resultCode isEqualToString:@"0"])
            {
                // [self performSegueWithIdentifier:@"signApproved" sender:self];
                [self performSegueWithIdentifier:@"approved" sender:self];
            }
            else
            {
                [self performSegueWithIdentifier:@"declined" sender:self];
            }
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSLog(@"dictionary: %@", xmlDoc);
        }];
        [task resume];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
   
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    ApprovedViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.customerName=self.customerName;
   // vcToPushTo.amountTxt=refAmount;
    vcToPushTo.amountTxt=_amountTxt;
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",mcsTransactionID];
    vcToPushTo.date=self.date;
    vcToPushTo.cardNumtxt=self.cardNumtxt;
    vcToPushTo.cardTypetxt=self.cardTypetxt;
    vcToPushTo.authCodetxt=resultDetail;
    vcToPushTo.merchantID=self.merchantID;
    vcToPushTo.appID=self.applicationID;
    vcToPushTo.transType=@"Void";
    vcToPushTo.acctTypeID=@"N/A";
    
    DeclindedViewController *dec = segue.destinationViewController;
    dec.customerName=self.customerName;
    dec.amountTxt=_amountTxt;
    dec.historyIDtxt=@"N/A";
   // dec.historyIDtxt=[NSString stringWithFormat:@"%@",mcsTransactionID];
    dec.date=self.date;
    dec.cardNumtxt=self.cardNumtxt;
    dec.cardTypetxt=self.cardTypetxt;
    if([resultDetail isEqualToString:@""]||resultDetail.length==0)
    {
        dec.authCodetxt=@"Transaction not found";
    }
    else
    dec.authCodetxt=resultDetail;
    dec.merchantID=self.merchantID;
    dec.appID=self.applicationID;
    dec.transType=@"Void";
    dec.acctTypeID=@"N/A";
    
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)activities:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)search:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *myVC = (SearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"search"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
@end
