//
//  VoidRefundMainViewController.h
//  MFmPay
//
//  Created by Prism Pay on 5/9/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface VoidRefundMainViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *records;
@property (weak, nonatomic) IBOutlet UIImageView *imgRefund;
@property (weak, nonatomic) IBOutlet UILabel *RefundnVoidLbl;
- (IBAction)showRefund:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgVoid;
@property (weak, nonatomic) IBOutlet UILabel *voidLbl;
@property (weak, nonatomic) IBOutlet UIButton *creditBtn;
- (IBAction)activityAction:(id)sender;
- (IBAction)supportAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *no_data_view;
- (IBAction)settings:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *headerView;

- (IBAction)search:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *refundLbl;
- (IBAction)showVoid:(id)sender;

- (IBAction)payAction:(id)sender;

//Get values
@property (strong, nonatomic)NSString *customerNamestr;
@property (strong, nonatomic)NSString *transTypeID;
@property (strong, nonatomic)NSString *startDate;
@property (strong, nonatomic)NSString *endDate;
@property (weak, nonatomic) IBOutlet UIView *blank_view;
@property (strong, nonatomic)NSString *transID;
@end
