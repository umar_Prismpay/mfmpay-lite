//
//  DropDownTableViewCell.h
//  MFmPay
//
//  Created by Prism Pay on 5/24/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtLabel;

@end
