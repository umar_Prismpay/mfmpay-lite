//
//  DeclindedViewController.h
//  MFmPay
//
//  Created by Prism Pay on 4/28/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface DeclindedViewController : BaseViewController
@property(nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UILabel *datelbl;
@property (weak, nonatomic) IBOutlet UILabel *saleType;
@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *cardType;
@property (weak, nonatomic) IBOutlet UILabel *cardNum;
@property (weak, nonatomic) IBOutlet UILabel *historyID;
@property (weak, nonatomic) IBOutlet UILabel *orderID;
@property (weak, nonatomic) IBOutlet UILabel *acctID;
@property (weak, nonatomic) IBOutlet UILabel *subID;
@property (weak, nonatomic) IBOutlet UILabel *authCode;
- (IBAction)creditAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *amount;

- (IBAction)settings:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
- (IBAction)activity:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *activityBtn;
- (IBAction)payAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *creditBtn;

//Get values
@property (strong, nonatomic)NSString *customerName;
@property (strong, nonatomic)NSString *status;
@property (strong, nonatomic)NSString *amountTxt;
@property (strong, nonatomic)NSString *date;
@property (strong, nonatomic)NSString *transType;
@property (strong, nonatomic)NSString *cardTypetxt;
@property (strong, nonatomic)NSString *authCodetxt;
@property (strong, nonatomic)NSString *cardNumtxt;
@property (strong, nonatomic)NSString *historyIDtxt;
@property (strong, nonatomic)NSString *appID;
@property (strong, nonatomic)NSString *merchantID;
@property (strong, nonatomic)NSString *acctTypeID;
@end
