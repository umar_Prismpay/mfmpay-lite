//
//  ThemeColor.m
//  MFmPay
//
//  Created by Prism Pay on 4/25/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "ThemeColor.h"

@implementation ThemeColor
+(UIColor *)changeGraphColor
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *theme=[prefs objectForKey:@"theme"];
    if([theme isEqualToString:@"darkGreen"])
    {
        //return [UIColor colorWithRed:0.243 green:0.478 blue:0.518 alpha:1];
        return [UIColor colorWithRed:0.66 green:0.09 blue:0.49 alpha:1.0];
    }else if([theme isEqualToString:@"lightBlue"])
    {
        return [UIColor colorWithRed:0 green:0.537 blue:1 alpha:1];
    }else if([theme isEqualToString:@"purple"]){
        return [UIColor colorWithRed:0.243 green:0.478 blue:0.518 alpha:1];

        //return [UIColor colorWithRed:0.659 green:0.094 blue:0.49 alpha:1];
    }else if([theme isEqualToString:@"gray"]){
        return [UIColor colorWithRed:0.424 green:0.424 blue:0.424 alpha:1];
    }
    else if([theme isEqualToString:@"darkBlue"]){
        return [UIColor colorWithRed:0.118 green:0 blue:1 alpha:1];
    }
    else if([theme isEqualToString:@"lameBlue"]){
        return [UIColor colorWithRed:0 green:0.51 blue:0.745 alpha:1];
    }
    else if([theme isEqualToString:@"parrotGreen"]){
        return [UIColor colorWithRed:0.165 green:0.631 blue:0.196 alpha:1];
    }
    else if([theme isEqualToString:@"red"]){
        return [UIColor colorWithRed:0.933 green:0.078 blue:0.039 alpha:1];
    }
    else
        return nil;
}
+(UIImage *)setPayImage
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *theme=[prefs objectForKey:@"theme"];
    if([theme isEqualToString:@"darkGreen"])
    {
        return [UIImage imageNamed:@"PayButton_pink"];
    }else if([theme isEqualToString:@"lightBlue"])
    {
        return [UIImage imageNamed:@"Blue_Pay_Icon"];
    }else if([theme isEqualToString:@"purple"]){
        
        return [UIImage imageNamed:@"Pay_Icon_green"];
    }else if([theme isEqualToString:@"gray"]){
        return [UIImage imageNamed:@"Grey_Pay_Icon"];
    }
    else if([theme isEqualToString:@"darkBlue"]){
        return [UIImage imageNamed:@"Dark_Blue_Pay_Icon"];
    }
    else if([theme isEqualToString:@"lameBlue"]){
        return [UIImage imageNamed:@"Sea_Green_Icon"];
    }
    else if([theme isEqualToString:@"parrotGreen"]){
        return [UIImage imageNamed:@"Green_Pay_Icon"];
    }
    else if([theme isEqualToString:@"red"]){
       return [UIImage imageNamed:@"Red_Pay_Icon"];
    }
    else
        return nil;
}
@end
