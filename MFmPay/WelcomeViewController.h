//
//  WelcomeViewController.h
//  MFmPay
//
//  Created by Muhammad Umar on 12/16/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *existingBTn;
- (IBAction)linked:(id)sender;
- (IBAction)signIn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *AcctBtn;
@end
