//
//  main.m
//  MFmPay
//
//  Created by Prism Pay on 4/19/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
