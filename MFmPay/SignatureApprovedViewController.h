//
//  SignatureApprovedViewController.h
//  MFmPay
//
//  Created by Prism Pay on 6/13/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>
@interface SignatureApprovedViewController : BaseViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *label_view;
@property (weak, nonatomic) IBOutlet UIButton *smsBtn;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *receiptBtn;
- (IBAction)send_sms_email:(id)sender;
- (IBAction)payAction:(id)sender;
- (IBAction)send_email:(id)sender;
- (IBAction)send_sms:(id)sender;
- (IBAction)settings:(id)sender;
- (IBAction)support:(id)sender;
- (IBAction)credit:(id)sender;
- (IBAction)activity:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *send_receiptBtn;
- (IBAction)show_receipt:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *receiptView;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *mobileNum;
@property (weak, nonatomic) IBOutlet UILabel *approvalCode;

@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UILabel *refNumber;

@property (weak, nonatomic) IBOutlet UILabel *mcsTransID;
@property (weak, nonatomic) IBOutlet UILabel *procTransID;
@property (weak, nonatomic) IBOutlet UILabel *sendReceiptLbl;

//Get values
@property (strong, nonatomic)NSString *MCSTransID;
@property (strong, nonatomic)NSString *ProceTransID;
@property (strong, nonatomic)NSString *amounttxt;
@property (strong, nonatomic)NSString *RefNum;
@property (strong, nonatomic)NSString *ProcAppCode;
@property(weak, nonatomic) NSString * saleType;
@end
