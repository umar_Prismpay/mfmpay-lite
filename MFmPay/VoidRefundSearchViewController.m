//
//  VoidRefundSearchViewController.m
//  MFmPay
//
//  Created by Prism Pay on 6/20/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "VoidRefundSearchViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "ThemeColor.h"
#import "HistoryTableViewCell.h"
#import "UIView+Toast.h"
#import "VoidViewController.h"
#import "RefundViewController.h"
#import "SupportViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "SearchViewController.h"
#import "VoidRefundSearchViewController.h"
#import "VoidRefundMainViewController.h"
#import "PaymentSelectionViewController.h"
@interface VoidRefundSearchViewController ()
{
//    BOOL isRefund;
//    BOOL isVoid;
    NSString *url;
    NSString *pageCount;
    int count;
    NSMutableArray *customerName;
    NSMutableArray *status;
    NSMutableArray *amount;
    NSMutableArray *date;
    NSMutableArray *transType;
    NSMutableArray *cardType;
    NSMutableArray *authCode;
    NSMutableArray *cardNum;
    NSMutableArray *historyID;
    NSMutableArray *numOfTrans;
    NSMutableArray *amountofTrans;
    NSMutableArray *stationID;
    NSMutableArray *merchantID;
    NSMutableArray *applicationID;
    AFHTTPSessionManager *manager;
    NSIndexPath *pathforseague;
    NSString *mcsacctID;
}
@end

@implementation VoidRefundSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
//    isRefund=true;
//    isVoid=false;
    // Do any additional setup after loading the view.
    if(_isRefund)
        self.RefundnVoidLbl.text=@"Refund Transaction";
    else
        self.RefundnVoidLbl.text=@"Void Transaction";
    url = [NSString stringWithFormat:@"%@Report/Transaction",sharedManager.url];
    //demo.prismpay.com
    pageCount=@"50";
    count=50;
    [self styleme];
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(hidehud) userInfo:nil repeats:NO];
    [self getService];
    
}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)styleme{
    self.view.backgroundColor=[UIColor whiteColor];
    self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.headerView.backgroundColor=[ThemeColor changeGraphColor];
    self.creditBtn.layer.cornerRadius = 10;
    self.creditBtn.layer.masksToBounds = YES;
    
}
-(void)getService{
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    //Date work
    
//    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
//    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
//    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
//    
//    NSDate *now = [DateFormatter dateFromString:endDate];
//    // NSDate *now = [NSDate date];
//    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
//    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
//    NSLog(@"7 days ago: %@", startDate);
    
    //eNds here
    if(count<51)
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
           manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
//            NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.startDate,@"StartDate",self.endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",@"0",@"TransactionStatusID", nil];
            
             NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:mcsacctID,@"MCSAccountID",self.startDate,@"StartDate",self.endDate,@"EndDate",self.transTypeID,@"GatewayTransactionID",@"1",@"TransactionTypeID",@"0",@"TransactionStatusID",self.customerNamestr,@"CardHolderName",pageCount,@"PageCount",@"1",@"PageIndex", nil];
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithDictionary:params];
            
            if([self.transTypeID isEqualToString:@""])
                [dic removeObjectForKey:@"TransactionID"];
            if([self.customerNamestr isEqualToString:@""])
                [dic removeObjectForKey:@"Name"];
//            if([self.transID isEqualToString:@""])
//                [dic removeObjectForKey:@"TransactionID"];
            if([self.startDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
            {
                [dic removeObjectForKey:@"StartDate"];
                [dic setObject:startDate forKey:@"StartDate"];
            }
            if([self.endDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
            {
                [dic removeObjectForKey:@"EndDate"];
                [dic setObject:endDate forKey:@"EndDate"];
            }
            params=dic;
            
            
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                customerName=[[NSMutableArray alloc]init];
                status=[[NSMutableArray alloc]init];
                amount=[[NSMutableArray alloc]init];
                date=[[NSMutableArray alloc]init];
                transType=[[NSMutableArray alloc]init];
                cardType=[[NSMutableArray alloc]init];
                authCode=[[NSMutableArray alloc]init];
                cardNum=[[NSMutableArray alloc]init];
                historyID=[[NSMutableArray alloc]init];
                applicationID=[[NSMutableArray alloc]init];
                stationID=[[NSMutableArray alloc]init];
                merchantID=[[NSMutableArray alloc]init];
                NSArray *arr=[responseObject valueForKey:@"Object"];
                
                 if (arr != (id)[NSNull null] && [arr count] != 0)
                for(NSArray *dic in arr)
                {
                      if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                          [customerName addObject:@"N/A"];
                    else
                    [customerName addObject:[dic valueForKey:@"CardHolderName"]];
                    
                    
                    if([dic valueForKey:@"TransactionStatusID"]== (id)[NSNull null])
                        [status addObject:@"N/A"];
                    else
                    [status addObject:[dic valueForKey:@"TransactionStatusID"]];
                    
                    
                    if([dic valueForKey:@"Amount"]== (id)[NSNull null])
                        [amount addObject:@"N/A"];
                    else
                    [amount addObject:[dic valueForKey:@"Amount"]];
                    
                    
                    if([dic valueForKey:@"TransactionDate"]== (id)[NSNull null])
                        [date addObject:@"N/A"];
                    else
                    [date addObject:[dic valueForKey:@"TransactionDate"]];
                    
                    
                    
                    if([dic valueForKey:@"TransactionTypeName"]== (id)[NSNull null])
                        [transType addObject:@"N/A"];
                    else
                    [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
                    
                    
                    
                    if([dic valueForKey:@"CardTypeName"]== (id)[NSNull null])
                        [cardType addObject:@"N/A"];
                    else
                    [cardType addObject:[dic valueForKey:@"CardTypeName"]];
                    
                    
                    
                    if([dic valueForKey:@"AuthCode"]== (id)[NSNull null])
                        [authCode addObject:@"N/A"];
                    else
                    [authCode addObject:[dic valueForKey:@"AuthCode"]];
                    
                    
                    
                    if([dic valueForKey:@"Last4Digit"]== (id)[NSNull null])
                        [cardNum addObject:@"N/A"];
                    else
                    [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
                    
                    
                    
                    if([dic valueForKey:@"GatewayTransactionID"]== (id)[NSNull null])
                        [historyID addObject:@"N/A"];
                    else
                    [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
                    
                    
                    
                    if([dic valueForKey:@"ApplicationID"]== (id)[NSNull null])
                        [applicationID addObject:@"N/A"];
                    else
                    [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
                    
                    
                    
                    if([dic valueForKey:@"StationID"]== (id)[NSNull null])
                        [stationID addObject:@"N/A"];
                    else
                    [stationID addObject:[dic valueForKey:@"StationID"]];
                    
                    
                    
                    
                    if([dic valueForKey:@"MerchantID"]== (id)[NSNull null])
                        [merchantID addObject:@"N/A"];
                    else
                    [merchantID addObject:[dic valueForKey:@"MerchantID"]];
                    
                }
                
                
                if([customerName count]>0)
                    self.no_data_view.hidden=true;
                if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
                while (count<[[responseObject valueForKey:@"Custom"]intValue])
                    
                    
                {
                    
                    count+=50;
                    pageCount=[NSString stringWithFormat:@"%d",count];
                    
                    [self getService1];
                    
                    
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.records reloadData];
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
            
        });
    });
    
}
-(void)getService1{
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
    NSLog(@"7 days ago: %@", startDate);
    
    //Date work
    
//    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
//    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
//    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
//    
//    NSDate *now = [DateFormatter dateFromString:endDate];
//    // NSDate *now = [NSDate date];
//    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
//    NSString *startDate = [DateFormatter stringFromDate:sevenDaysAgo];
//    NSLog(@"7 days ago: %@", startDate);
    
    
   manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
    
    //  NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:startDate,@"StartDate",endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex", nil];
    
   NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.startDate,@"StartDate",self.endDate,@"EndDate",pageCount,@"PageCount",@"1",@"PageIndex",self.customerNamestr,@"Name",self.transTypeID,@"GatewayTransactionID",@"0",@"TransactionTypeID",mcsacctID,@"MCSAccountID", nil];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc]initWithDictionary:params];
    
    if([self.transTypeID isEqualToString:@""])
        [dic removeObjectForKey:@"TransactionID"];
    if([self.customerNamestr isEqualToString:@""])
        [dic removeObjectForKey:@"Name"];
//    if([self.transID isEqualToString:@""])
//        [dic removeObjectForKey:@"TransactionID"];
    if([self.startDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [dic removeObjectForKey:@"StartDate"];
        [dic setObject:startDate forKey:@"StartDate"];
    }
    if([self.endDate isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [dic removeObjectForKey:@"EndDate"];
        [dic setObject:endDate forKey:@"EndDate"];
    }
    params=dic;
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        customerName=[[NSMutableArray alloc]init];
        status=[[NSMutableArray alloc]init];
        amount=[[NSMutableArray alloc]init];
        date=[[NSMutableArray alloc]init];
        transType=[[NSMutableArray alloc]init];
        cardType=[[NSMutableArray alloc]init];
        authCode=[[NSMutableArray alloc]init];
        cardNum=[[NSMutableArray alloc]init];
        historyID=[[NSMutableArray alloc]init];
        applicationID=[[NSMutableArray alloc]init];
        stationID=[[NSMutableArray alloc]init];
        merchantID=[[NSMutableArray alloc]init];
        NSArray *arr=[responseObject valueForKey:@"Object"];
        
        
        for(NSArray *dic in arr)
        {
            if([dic valueForKey:@"CardHolderName"]== (id)[NSNull null])
                [customerName addObject:@"N/A"];
            else
                [customerName addObject:[dic valueForKey:@"CardHolderName"]];
            
            
            if([dic valueForKey:@"TransactionStatusID"]== (id)[NSNull null])
                [status addObject:@"N/A"];
            else
                [status addObject:[dic valueForKey:@"TransactionStatusID"]];
            
            
            if([dic valueForKey:@"Amount"]== (id)[NSNull null])
                [amount addObject:@"N/A"];
            else
                [amount addObject:[dic valueForKey:@"Amount"]];
            
            
            if([dic valueForKey:@"TransactionDate"]== (id)[NSNull null])
                [date addObject:@"N/A"];
            else
                [date addObject:[dic valueForKey:@"TransactionDate"]];
            
            
            
            if([dic valueForKey:@"TransactionTypeName"]== (id)[NSNull null])
                [transType addObject:@"N/A"];
            else
                [transType addObject:[dic valueForKey:@"TransactionTypeName"]];
            
            
            
            if([dic valueForKey:@"CardTypeName"]== (id)[NSNull null])
                [cardType addObject:@"N/A"];
            else
                [cardType addObject:[dic valueForKey:@"CardTypeName"]];
            
            
            
            if([dic valueForKey:@"AuthCode"]== (id)[NSNull null])
                [authCode addObject:@"N/A"];
            else
                [authCode addObject:[dic valueForKey:@"AuthCode"]];
            
            
            
            if([dic valueForKey:@"Last4Digit"]== (id)[NSNull null])
                [cardNum addObject:@"N/A"];
            else
                [cardNum addObject:[NSString stringWithFormat:@"XXXX-XXXX-%@",[dic valueForKey:@"Last4Digit"]]];
            
            
            
            if([dic valueForKey:@"GatewayTransactionID"]== (id)[NSNull null])
                [historyID addObject:@"N/A"];
            else
                [historyID addObject:[dic valueForKey:@"GatewayTransactionID"]];
            
            
            
            if([dic valueForKey:@"ApplicationID"]== (id)[NSNull null])
                [applicationID addObject:@"N/A"];
            else
                [applicationID addObject:[dic valueForKey:@"ApplicationID"]];
            
            
            
            if([dic valueForKey:@"StationID"]== (id)[NSNull null])
                [stationID addObject:@"N/A"];
            else
                [stationID addObject:[dic valueForKey:@"StationID"]];
            
            
            
            
            if([dic valueForKey:@"MerchantID"]== (id)[NSNull null])
                [merchantID addObject:@"N/A"];
            else
                [merchantID addObject:[dic valueForKey:@"MerchantID"]];
            
        }
        if( [responseObject valueForKey:@"Custom"] != (id)[NSNull null])
        while (count<[[responseObject valueForKey:@"Custom"]intValue])
            
            
        {
            
            count+=50;
            pageCount=[NSString stringWithFormat:@"%d",count];
            
            [self getService1];
            
            
        }
        [self.records reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"error: %@", error);
    }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ customerName count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"Pos";
    
    
    HistoryTableViewCell *cell = (HistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //    if([[[ActivityData statusArr]objectAtIndex:indexPath.row]isEqualToString:@"Declined"])
    //        cell.status_lbl.textColor=[UIColor redColor];
    NSString *str;
    
    str = [NSString stringWithFormat:@"%@",[status objectAtIndex:indexPath.row]];
    if([str isEqualToString:@"0"] )
    {
        
        //[status replaceObjectAtIndex:indexPath.row withObject:@"Approved"];
        str=@"Approved";
    }
    else
    {
        // [status replaceObjectAtIndex:indexPath.row withObject:@"Declined"];
        //  cell.status_lbl.textColor=[UIColor redColor];
        str=@"Declined";
        str=@"Approved";
    }
    
    cell.status_lbl.text=str;
    cell.customer_name_lbl.textColor=[ThemeColor changeGraphColor];
    cell.customer_name_lbl.text=[ customerName objectAtIndex:indexPath.row];
    
    NSString *string = [date objectAtIndex:indexPath.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
    //  [date replaceObjectAtIndex:indexPath.row withObject:finalDate];
    cell.date_lbl.text=finalDate;
    
    
    cell.price_lbl.text=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:indexPath.row]floatValue ]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    pathforseague=indexPath;
    if(_isRefund)
        [self performSegueWithIdentifier:@"Searchrefund" sender:self];
    else
        [self performSegueWithIdentifier:@"Searchvoid" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
   // NSIndexPath *path = [self.records indexPathForSelectedRow];
    RefundViewController *vcToPushTo = segue.destinationViewController;
    vcToPushTo.index = pathforseague.row;
    vcToPushTo.customerName=[customerName objectAtIndex:pathforseague.row];
    vcToPushTo.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
  
    NSString *string = [date objectAtIndex:pathforseague.row];
    NSString *match = @"T";
    NSString *preTel;
    
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner scanUpToString:match intoString:&preTel];
    
    [scanner scanString:match intoString:nil];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDate *yourDate = [dateFormatter dateFromString:preTel];
    dateFormatter.dateFormat = @"MMM/dd/yyyy";
    NSString *finalDate=[dateFormatter stringFromDate:yourDate];
    
    vcToPushTo.date=finalDate;
    vcToPushTo.transType=[transType objectAtIndex:pathforseague.row];
    vcToPushTo.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.cardTypetxt=@"N/A";
    else
        vcToPushTo.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        vcToPushTo.authCodetxt=@"N/A";
    else
        vcToPushTo.authCodetxt=[authCode objectAtIndex:pathforseague.row];
    //vcToPushTo.status=[status objectAtIndex:path.row];
    vcToPushTo.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    vcToPushTo.applicationID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    vcToPushTo.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    vcToPushTo.stationID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
    
    
    VoidViewController *dec = segue.destinationViewController;
    dec.index = pathforseague.row;
    dec.customerName=[customerName objectAtIndex:pathforseague.row];
    dec.amountTxt=[NSString stringWithFormat:@"$%.02f",[[amount objectAtIndex:pathforseague.row]floatValue ]];
    dec.date=finalDate;
    dec.transType=[transType objectAtIndex:pathforseague.row];
    dec.cardNumtxt=[cardNum objectAtIndex:pathforseague.row];
    if([cardType objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.cardTypetxt=@"N/A";
    else
        dec.cardTypetxt=[cardType objectAtIndex:pathforseague.row];
    if([authCode objectAtIndex:pathforseague.row]== (id)[NSNull null])
        dec.authCodetxt=@"N/A";
    else
        dec.authCodetxt=[authCode objectAtIndex:pathforseague.row];
    //   dec.status=[status objectAtIndex:path.row];
    dec.historyIDtxt=[NSString stringWithFormat:@"%@",[historyID objectAtIndex:pathforseague.row]];
    dec.applicationID=[NSString stringWithFormat:@"%@",[applicationID objectAtIndex:pathforseague.row]];
    dec.merchantID=[NSString stringWithFormat:@"%@",[merchantID objectAtIndex:pathforseague.row]];
    dec.stationID=[NSString stringWithFormat:@"%@",[stationID objectAtIndex:pathforseague.row]];
}

#pragma buttonACtions
//-(void)hidehud{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//}
- (IBAction)showRefund:(id)sender {
//    isRefund=true;
//    isVoid=false;
    self.RefundnVoidLbl.text=@"Refund Transaction";
    self.refundLbl.hidden=false;
    self.imgRefund.image=[UIImage imageNamed:@"Refund_Icon"];
    self.imgVoid.image=[UIImage imageNamed:@"Void_Icon_deactivate"];
    self.voidLbl.hidden=true;
}

- (IBAction)showVoid:(id)sender {
    
//    isRefund=false;
//    isVoid=true;
    self.RefundnVoidLbl.text=@"Void Transaction";
    self.refundLbl.hidden=true;
    self.imgRefund.image=[UIImage imageNamed:@"Refund_Icon_deactivate"];
    self.imgVoid.image=[UIImage imageNamed:@"Void_Icon"];
    self.voidLbl.hidden=false;
}


- (IBAction)activityAction:(id)sender {
  [manager.operationQueue cancelAllOperations];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)supportAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)search:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *myVC = (SearchViewController *)[storyboard instantiateViewControllerWithIdentifier:@"search"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
