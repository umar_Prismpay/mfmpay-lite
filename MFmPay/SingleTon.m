//
//  SingleTon.m
//  Resturant
//
//  Created by Faisal on 10/16/14.
//  Copyright (c) 2014 PrismPayTech. All rights reserved.
//

#import "SingleTon.h"

@implementation SingleTon


#pragma mark Singleton Methods

+ (id)sharedManager {
    static SingleTon *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        self.ItemCount=0;
        self.itemSelected=0;
        self.items=[[NSMutableArray alloc] init];
        self.orderMessages=[[NSMutableArray alloc] init];
        self.restaurantData=[[NSArray alloc] init];
        self.items_name=[[NSMutableArray alloc]init];
        self.items_quantity=[[NSMutableArray alloc]init];
        self.items_price=[[NSMutableArray alloc]init];
        self.taxApplicable=[[NSMutableArray alloc]init];
        self.paymentModeCash=YES;
        self.paymentModeCreditCard=NO;
        self.orderPlace=NO;
        self.isOrderMessageWindowOpen=NO;
        self.items_collection=[[NSMutableArray alloc]init];
        self.selectedBeaconMinor=0;
        self.selectedBeaconMajor=0;
        self.url = @"https://reporting.prismpay.com/API/";
        self.selectedAttrIndex=0;
        self.transUrl = @"prod.prismpay";
        
    }
    return self;
}
- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
