//
//  ActivityViewController.h
//  MFmPay
//
//  Created by Prism Pay on 4/21/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import "SimpleBarChart.h"
#import "ThemeColor.h"
#import <MessageUI/MessageUI.h>
@interface ActivityViewController : BaseViewController<SimpleBarChartDataSource, SimpleBarChartDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    NSArray *_values;
    
    SimpleBarChart *_chart;
    
    NSArray *_barColors;
    NSInteger _currentBarColor;
   
}



@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIView *graphView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *graphLabel;
@property (weak, nonatomic) IBOutlet UILabel *historyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTrailing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewLeading;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;


@property (weak, nonatomic) IBOutlet UIButton *graphBtn;
@property (weak, nonatomic) IBOutlet UIButton *historyBtn;
- (IBAction)showHistory:(id)sender;

- (IBAction)showGraph:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *activityBtn;
@property (weak, nonatomic) IBOutlet UIButton *creditBtn;
@property (weak, nonatomic) IBOutlet UIButton *settingsBtn;
@property (weak, nonatomic) IBOutlet UIButton *supportBtn;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
- (IBAction)payAction:(id)sender;
- (IBAction)creditAction:(id)sender;
- (IBAction)settingsAction:(id)sender;
- (IBAction)supportAction:(id)sender;
- (IBAction)activityAction:(id)sender;
- (IBAction)sendReceipt:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *graphImage;
@property (weak, nonatomic) IBOutlet UIImageView *historyImage;
@property (weak, nonatomic) IBOutlet UIView *no_data_view;

@property (weak, nonatomic) IBOutlet UIButton *sendRecieptBtn;
@property (weak, nonatomic) IBOutlet UILabel *numberofSalesLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceSale;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *csvView;
@property (weak, nonatomic) IBOutlet UILabel *sendTranLbl;
@property (weak, nonatomic) IBOutlet UITextField *csvEmailField;
@property (weak, nonatomic) IBOutlet UIButton *csvBtn;
- (IBAction)sendCsv:(id)sender;

- (IBAction)search:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *endDate;

@property (weak, nonatomic) IBOutlet UILabel *startDate;
@property (weak, nonatomic) IBOutlet UIImageView *top_bar;
@property (weak, nonatomic) IBOutlet UIImageView *top_image;

@end
