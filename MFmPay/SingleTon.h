//
//  SingleTon.h
//  Resturant
//
//  Created by Faisal on 10/16/14.
//  Copyright (c) 2014 PrismPayTech. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface SingleTon : NSObject


@property (nonatomic) int itemCount;
@property (nonatomic) int itemSelected;

@property (nonatomic) BOOL paymentModeCash;
@property (nonatomic) BOOL paymentModeCreditCard;
@property (nonatomic) BOOL orderPlace;
@property (nonatomic) BOOL isOrderMessageWindowOpen;
@property (nonatomic, strong) NSMutableArray * items;
@property (nonatomic, strong) NSArray * restaurantData;
@property(nonatomic,strong) NSMutableArray *items_name;
@property(nonatomic,strong) NSMutableArray *items_price;
@property(nonatomic,strong) NSMutableArray *items_quantity;
@property (nonatomic, strong) NSMutableArray * orderMessages;
@property (nonatomic, strong)NSString *stockQuantity;
@property(nonatomic,strong)NSMutableArray *items_collection;
@property(nonatomic,strong)NSString * taxRate;
@property(nonatomic,strong)NSMutableArray *taxApplicable;
@property (nonatomic) int selectedBeaconMinor;
@property (nonatomic) int selectedBeaconMajor;
@property(nonatomic,strong)NSString *nameCustomer;
@property (nonatomic) int iMagDeviceConnected;
@property (nonatomic) int UniMagDeviceConnected;
@property (nonatomic) NSString *cardDeviceName;
@property(nonatomic,strong) NSString * url;

@property(nonatomic) NSString *customerName;
@property (nonatomic) int selectedAttrIndex;

@property (nonatomic, strong) NSString * selectedOrderId;
@property(nonatomic,strong) NSString * transUrl;



+ (id)sharedManager;
@end
