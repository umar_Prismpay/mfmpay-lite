//
//  AppDelegate.h
//  MFmPay
//
//  Created by Prism Pay on 4/19/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iMag.h"
#import "uniMag.h"

#import "MBProgressHUD.h"
#import "objc/message.h"
@class iMag;
@class uniMag;
#import "ProgressHUD.h"
@class EAAccessory;
@class EASession;
typedef enum {
    MPAWakeUp = 0,
    MPANomal,
    MPASetAccessory,
    MPAPayment,
    MPAContinuePayment,
    MPASignature,
    MPAConfirm,
    MPADone,
    MFPXml,
}ActionStatus;

@class MBProgressHUD;


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    int secondKey;
    ActionStatus status;
    NSString *receivedData;
    int LandCapeFirstTime;
    NSMutableData *_writeData;
    NSMutableData *_readData;
    NSString *deviceName;
    NSString *deviceVersion;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) iMag *iMagSwipper;
@property (strong, nonatomic) uniMag *uniMagSwipper;
@property (assign, nonatomic) BOOL shouldRotate;
//- (void)setAccessory;
//- (void)sendXmlAction;
//-(void)sendXmlActionAmount:(NSString *)amount;
@property (nonatomic) BOOL isPresented;
@property (nonatomic, retain) EAAccessory *accessory;
@property (nonatomic, retain) EASession *session;

@property (strong, nonatomic) NSString *logText;

@property (nonatomic) BOOL screenIsPortraitOnly;
@property (nonatomic) BOOL screenIsLansCapeOnly;
//landCape forcing
//-(void)forceLandCape:(UIView *)v;
//-(void)reverseLandCape;
- (void)starthud;
- (void)stophud;
@property (nonatomic, retain)  MBProgressHUD *HUD;

@end

