//
//  CreditCardSaleViewController.h
//  MFmPay
//
//  Created by Prism Pay on 4/21/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import "BKCardNumberField.h"
#import "BKCardExpiryField.h"
#import "BKCurrencyTextField.h"
#import "BKCardNumberLabel.h"
#import "MP200.h"
@interface CreditCardSaleViewController : BaseViewController<UITextFieldDelegate,UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource, mp200delegate>{

    MP200 *mp200Device;
}
- (IBAction)activity:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *terminalHeader;
@property (weak, nonatomic) IBOutlet UIButton *terminalBtn;

- (IBAction)support:(id)sender;
- (IBAction)credit:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *amount;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UIView *terminalListView;

@property (weak, nonatomic) IBOutlet UIPickerView *terminalList;
@property (weak, nonatomic) IBOutlet UITextView *address;

@property (weak, nonatomic) IBOutlet UITextField *zipcode;
@property (weak, nonatomic) IBOutlet BKCardNumberField *cardnum;
- (IBAction)selectTerminal:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *cvv;
@property (weak, nonatomic) IBOutlet BKCardExpiryField *expDate;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
- (IBAction)settings:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *borderLine;
@property (weak, nonatomic) IBOutlet UIButton *proceed_Btn;
- (IBAction)process_transaction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *tip_header;

@property (weak, nonatomic) IBOutlet UIButton *tip_process_btn;
- (IBAction)tip_process:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *tip_view;
@property (weak, nonatomic) IBOutlet UIView *blur_view;
@property (weak, nonatomic) IBOutlet UIPickerView *tip_picker;
@property (weak, nonatomic) IBOutlet UILabel *bill_lbl;
@property (weak, nonatomic) IBOutlet UILabel *tip_lbl;
@property (weak, nonatomic) IBOutlet UILabel *total_lbl;
@property (weak, nonatomic) IBOutlet UIButton *credit_cardBtn;
- (IBAction)pay_with_card:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
- (IBAction)reset:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *pin_view;
@property (weak, nonatomic) IBOutlet UILabel *pin_total;
@property (weak, nonatomic) IBOutlet UITextField *pin1;
@property (weak, nonatomic) IBOutlet UITextField *pin2;
@property (weak, nonatomic) IBOutlet UITextField *pin3;
@property (weak, nonatomic) IBOutlet UITextField *pin4;
@property (weak, nonatomic) IBOutlet UIButton *pinBtn;
- (IBAction)pin_process:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *hardware_view;
@property (weak, nonatomic) IBOutlet UIView *hardware_header;
- (IBAction)mp200Emv:(id)sender;
- (IBAction)mp200Swipe:(id)sender;
- (IBAction)uniMag:(id)sender;
- (IBAction)iMag:(id)sender;
- (IBAction)paxD20:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *pin_back;
- (IBAction)back:(id)sender;

//Card
@property (nonatomic, strong, readonly) BKCardNumberFormatter *cardNumberFormatter;
//hardware

@property (nonatomic, retain) NSString * ccnum;
@property (nonatomic, retain) NSString * exDate;
@property (nonatomic, retain) NSString * CVV2;
@property (nonatomic, retain) NSString * expYear;
@property (nonatomic, retain) NSString * expMonth;
@property (nonatomic, retain) NSString * swipedata;
@end
