//
//  AppDelegate.m
//  MFmPay
//
//  Created by Prism Pay on 4/19/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "AppDelegate.h"
#import "SingleTon.h"
#import "SignatureViewController.h"
#import "SignInViewController.h"
#import "ActivityViewController.h"
#import "WelcomeViewController.h"
@interface AppDelegate ()<NSStreamDelegate,EAAccessoryDelegate,UIAlertViewDelegate>
{
     // SingleTon *sharedManager;
}
@end

@implementation AppDelegate
//- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    if ([self.window.rootViewController.presentedViewController isKindOfClass:[SignatureViewController class]])
//    {
//        SignatureViewController *secondController = (SignatureViewController *) self.window.rootViewController.presentedViewController;
//        
//        if (secondController.isPresented) // Check current controller state
//        {
//            return UIInterfaceOrientationMaskLandscape;
//        }
//        else return UIInterfaceOrientationMaskPortrait;
//    }
//    else return UIInterfaceOrientationMaskPortrait;
//}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[prefs objectForKey:@"theme"]);
    NSString *theme=[prefs objectForKey:@"theme"];
    if(theme==nil)
    [prefs setObject:@"purple" forKey:@"theme"];
    
 
    sharedManager=[SingleTon sharedManager];
    sharedManager.iMagDeviceConnected=0;
    sharedManager.UniMagDeviceConnected=0;
    sharedManager.cardDeviceName=@"";
    NSString * demo = [prefs valueForKey:@"demo"];
    if([demo isEqualToString:@"Yes"])
    {
    sharedManager.url = @"https://demo.prismpay.com/API/";;
        sharedManager.transUrl = @"test.mycardstorage";
    }
    else
    {
        sharedManager.url = @"https://reporting.prismpay.com/API/";
        sharedManager.transUrl = @"prod.prismpay";
    }
    
    
    //iMag
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iMagConnected:) name:@"iMagDidConnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iMagDisconnected:) name:@"iMagDidDisconnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iMagReceivedData:) name:@"iMagDidReceiveDataNotification" object:nil];
    
    //unimag
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagDidConnectNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagDidDisconnectNotification object:nil];
    
    
    // unimag physical attachment related
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagAttachmentNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagDetachmentNotification object:nil];
    
    
    //unimag swipe related
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagSwipeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagTimeoutSwipeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagDataProcessingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagInvalidSwipeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:uniMagDidReceiveDataNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recevieUniMag:) name:@"requestforswipe" object:nil];
    
    
    
    
    self.iMagSwipper = [[iMag alloc] init];
    
    self.uniMagSwipper=[[uniMag alloc] init];
    self.uniMagSwipper.readerType=UMREADER_UNIMAG_PRO;
    
    [self.uniMagSwipper setAutoConnect:YES];
    [self.uniMagSwipper setAutoAdjustVolume:YES];

    
    NSString *check = [prefs valueForKey:@"signIn"];
    if([check isEqualToString:@"yes"])
    {
        ActivityViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"activity"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;
    
    }else
    {
        SignInViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignInViewController"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;
        
        
       
    }
    
    return YES;
}
#pragma mark - iMagNotifications
- (void) iMagConnected:(NSNotification *)notification
{
    
    if ([[notification object] isKindOfClass:[EAAccessory class]])
    {
        EAAccessory *acc = [notification object];
        self.iMagSwipper = acc.delegate;
        deviceName=acc.name;
        deviceVersion=acc.firmwareRevision;
        NSString *deviceInfo=[NSString stringWithFormat: @"--Device Connected \n\n %@ %@", deviceName, deviceVersion];
        [ProgressHUD showSuccess:deviceInfo Interaction:YES];
        
        sharedManager.iMagDeviceConnected=1;
        sharedManager.UniMagDeviceConnected=0;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDeviceStatus" object:nil userInfo:nil];
        
    }
    else
    {
    }
}

- (void) iMagDisconnected:(NSNotification *)notification
{
    NSString *deviceInfo=[NSString stringWithFormat: @"Device Diconnected \n\n %@ %@", deviceName, deviceVersion];
    if(deviceName!=Nil)
    {
        [ProgressHUD showError:deviceInfo Interaction:YES];
        sharedManager.iMagDeviceConnected=0;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDeviceStatus" object:nil userInfo:nil];
    }
    
}
- (void) iMagReceivedData:(NSNotification *)notification
{
    [ProgressHUD show:@"iMagReceivedData 1"];
    
    NSData *data = [notification object];
    NSString *log = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if([log length] > 0)
    {
        NSArray *allCardData=[log componentsSeparatedByString:@"?"];
        if([allCardData count] > 0)
        {
            [ProgressHUD show:@"iMagReceivedData 2"];
            
            NSArray *cardInfo=[[allCardData objectAtIndex:0] componentsSeparatedByString:@"^"];
            if([cardInfo count]==3)
            {
                [ProgressHUD show:@"iMagReceivedData 3"];
                
                NSString *cardNumber=[[cardInfo objectAtIndex:0] stringByReplacingOccurrencesOfString:@"%B" withString:@""];
                NSString *cardName=[cardInfo objectAtIndex:1];
                NSString *cardExp=[cardInfo objectAtIndex:2];
                NSString *expYear=[NSString stringWithFormat:@"20%@",[cardExp substringWithRange:NSMakeRange(0, 2)]];
                NSString *expMonth=[cardExp substringWithRange:NSMakeRange(2, 2)];
                NSString *swipedata;
                NSString *code = [expYear substringFromIndex: [expYear length] - 2];
                if([allCardData count]==2)
                {
                    swipedata=[NSString stringWithFormat:@";%@=%@%@?",cardNumber,code,expMonth];
                    //                    [helper showAlert:@"Card Info" message:swipedata];
                }
                else
                    swipedata=[NSString stringWithFormat:@"%@?",[allCardData objectAtIndex:1]];
                
                //[helper showAlert:@"Card Info" message:[NSString stringWithFormat:@"getCardInfo('%@','%@','%@','%@');",cardName,cardNumber,expMonth,expYear]];
                
                
                NSDictionary *cardInfo2=[[NSDictionary alloc] initWithObjectsAndKeys:
                                         cardName,@"ccname",
                                         cardNumber,@"ccnum",
                                         expMonth,@"expmonth",
                                         expYear,@"expyear",
                                         swipedata,@"swipedata",
                                         nil];
                
                //[helper showAlert:@"Message 2" message:[NSString stringWithFormat:@"%@",cardInfo2]];
                
                //[[NSNotificationCenter defaultCenter] postNotificationName:@"iMagSendCardInfo" object:cardInfo2];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"iMagSendCardInfo" object:nil userInfo:cardInfo2];
               
                
                [ProgressHUD showSuccess:@"Success" Interaction:YES];
                
            }else{
                
                [ProgressHUD showError:@"Invalid data readed" Interaction:YES];
            }
            
        }else{
            [ProgressHUD showError:@"Card not Swiped Properly" Interaction:YES];
            
        }
        
    }else{
      
        [ProgressHUD showError:@"Invalid card information" Interaction:YES];
        
    }
    
    //NSLog(@"%@  =>>  %@", data, log);
    // NSLog(@"Received Data : %@",[NSString stringWithFormat:@"Received next bytes: %@ \n\n UTF8 interpretation: %@", data, log]);
}


#pragma mark Unimag
#pragma mark Unimag
-(void)recevieUniMag:(NSNotification *)notification
{
    NSLog(@"Noti Name %@",notification.name);
    NSLog(@"=====================================");
    
    if([[notification name] isEqualToString:@"uniMagAttachmentNotification"])
    {
        sharedManager.iMagDeviceConnected=0;
        sharedManager.UniMagDeviceConnected=0;
        //[ProgressHUD showError:@"Unimag\nConnecting..." Interaction:YES];
        [ProgressHUD showError:@"Unimag\n\Connecting...\n\nIf not connected in 10 secs please reconnect it" Interaction:YES];
        
        
        
        
        
        
    }else if([[notification name] isEqualToString:@"uniMagDetachmentNotification"])
    {
        sharedManager.iMagDeviceConnected=0;
        sharedManager.UniMagDeviceConnected=0;
        
        
        NSString *deviceInfo=[NSString stringWithFormat: @"Device Removed \n\n %@ %@", unimagDeviceName, unimagDeviceVersion];
        [ProgressHUD showError:deviceInfo Interaction:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDeviceStatus" object:nil userInfo:nil];
    }
    else if([[notification name] isEqualToString:@"uniMagDidConnectNotification"])
    {
        sharedManager.iMagDeviceConnected=0;
        sharedManager.UniMagDeviceConnected=1;
        
        [self.uniMagSwipper setAutoConnect:YES];
        NSString *deviceInfo=[NSString stringWithFormat: @"Device Connected \n\n %@ %@", unimagDeviceName, unimagDeviceVersion];
        [ProgressHUD showSuccess:deviceInfo Interaction:YES];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDeviceStatus" object:nil userInfo:nil];
        
        
        
    }else if([[notification name] isEqualToString:@"uniMagDidDisconnectNotification"])
    {
        sharedManager.iMagDeviceConnected=0;
        sharedManager.UniMagDeviceConnected=0;
        
        NSString *deviceInfo=[NSString stringWithFormat: @"Device Removed \n\n %@ %@", unimagDeviceName, unimagDeviceVersion];
        [ProgressHUD showError:deviceInfo Interaction:YES];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendDeviceStatus" object:nil userInfo:nil];
        
        
    }else if([[notification name] isEqualToString:@"uniMagInvalidSwipeNotification"])
    {
        [ProgressHUD showError:@"Invalid Swipe Data" Interaction:YES];
        
    }
    else if([[notification name] isEqualToString:@"requestforswipe"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:uniMagSwipeNotification object:nil userInfo:nil];
        
    }
    else if([[notification name] isEqualToString:@"uniMagSwipeNotification"])
    {
        [ProgressHUD show:@"Please Swipe a Card" Interaction:YES];
        
        [self.uniMagSwipper setSwipeTimeoutDuration:60*5];
        [self.uniMagSwipper requestSwipe];
        
    }
    else if([[notification name] isEqualToString:@"uniMagDidReceiveDataNotification"])
    {
        NSData *data = [notification object];
        NSString *log = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if([log length] > 0)
        {
            
            NSArray *allCardData=[log componentsSeparatedByString:@"?"];
            
            
            if([allCardData count] > 0)
            {
                NSArray *cardInfo=[[allCardData objectAtIndex:0] componentsSeparatedByString:@"^"];
                if([cardInfo count]==3)
                {
                    NSString *cardNumber=[[cardInfo objectAtIndex:0] stringByReplacingOccurrencesOfString:@"%B" withString:@""];
                    NSString *cardName=[cardInfo objectAtIndex:1];
                    NSString *cardExp=[cardInfo objectAtIndex:2];
                    NSString *expYear=[NSString stringWithFormat:@"20%@",[cardExp substringWithRange:NSMakeRange(0, 2)]];
                    NSString *expMonth=[cardExp substringWithRange:NSMakeRange(2, 2)];
                    NSString *swipedata;
                    NSString *code = [expYear substringFromIndex: [expYear length] - 2];
                    if([allCardData count]==2)
                    {
                        swipedata=[NSString stringWithFormat:@";%@=%@%@?",cardNumber,code,expMonth];
                    }
                    else
                        swipedata=[NSString stringWithFormat:@"%@?",[allCardData objectAtIndex:1]];
                    
                    
                    //[helper showAlert:@"Card Info" message:[NSString stringWithFormat:@"getCardInfo('%@','%@','%@','%@');",cardName,cardNumber,expMonth,expYear]];
                    NSDictionary *cardInfo2=[[NSDictionary alloc] initWithObjectsAndKeys:
                                             cardName,@"ccname",
                                             cardNumber,@"ccnum",
                                             expMonth,@"expmonth",
                                             expYear,@"expyear",
                                             swipedata,@"swipedata",
                                             nil];
                    
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"iMagSendCardInfo" object:nil userInfo:cardInfo2];
                    
                    
                    [ProgressHUD showSuccess:@"Success" Interaction:YES];
                    
                }else{
                    [ProgressHUD showError:@"Invalid data readed" Interaction:YES];
                }
                
            }else{
                [ProgressHUD showError:@"Card not Swiped Properly" Interaction:YES];
                
            }
            
        }else{
         
            [ProgressHUD showError:@"Invalid card information" Interaction:YES];
            
        }
        
        
        NSLog(@"Noti Data %@",log);
        
    }else{
        //nothing to do
    }
    
    
    
    
}
#pragma mark - hud
- (void)starthud
{
    NSLog(@"starthud");
    self.HUD = [[MBProgressHUD alloc] initWithView:self.window];
    self.HUD.labelText = @"Processing";
    self.HUD.detailsLabelText = @"Please wait...";
    
    
    [self.window addSubview:self.HUD];
    
    [self.HUD show:YES];
    
    
    
    [self.HUD bringSubviewToFront:self.HUD];
}
- (void)stophud
{
    NSLog(@"stophud");
    [self.HUD  hide:YES];
    [self.HUD removeFromSuperview];
    self.HUD = nil;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
