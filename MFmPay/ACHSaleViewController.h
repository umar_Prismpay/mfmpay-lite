//
//  ACHSaleViewController.h
//  MFMPay_iPhone
//
//  Created by Muhammad Umar on 3/6/18.
//  Copyright © 2018 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHSaleViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
@property(weak, nonatomic) NSString * soapAction;
@property (weak, nonatomic) IBOutlet UITextField *txtAccountTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtRoutingNumber;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switchAccountType;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switchAccountHolderType;
- (IBAction)accountType:(id)sender;
- (IBAction)accountHolderType:(id)sender;
- (IBAction)processTransaction:(id)sender;
- (IBAction)clearSale:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)creditAction:(id)sender;

- (IBAction)payAction:(id)sender;
- (IBAction)settings:(id)sender;
- (IBAction)support:(id)sender;
- (IBAction)credit:(id)sender;
- (IBAction)activity:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtCheckNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnActivity;
@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
@property (weak, nonatomic) IBOutlet UIButton *btnSupport;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnProcess;
@property (weak, nonatomic) IBOutlet UIButton *btnClearSale;

@end
