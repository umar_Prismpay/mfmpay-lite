//
//  BaseViewController.h
//  MFmPay
//
//  Created by Prism Pay on 4/19/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeColor.h"
#import "Reachability.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "SingleTon.h"
@class Reachability;

@interface BaseViewController : UIViewController<MFMessageComposeViewControllerDelegate>
{
Reachability* internetReachable;
Reachability* hostReachable;
}
@property(assign)BOOL hostActive;
@property(assign)BOOL internetActive;
@property(weak,nonatomic) NSString * Liveurl;
@property(weak,nonatomic) NSString * Demourl;
@property(strong,nonatomic) SingleTon * sharedManager;
-(void) checkNetworkStatus:(NSNotification *)notice;

@end
