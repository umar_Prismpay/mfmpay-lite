//
//  ConnectorListModel.m
//  MFmPay
//
//  Created by Muhammad Umar on 02/11/2017.
//  Copyright © 2017 Merchant First. All rights reserved.
//

#import "ConnectorListModel.h"

@implementation ConnectorListModel

- (instancetype)init: (int)terminalID name: (NSString*) Name
{
    self = [super init];
    if (self) {
        self.TerminalDetailID = terminalID;
        self.TerminalName = Name;
    }
    return self;
}
@end
