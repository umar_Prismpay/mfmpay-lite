//
//  SettingsViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/13/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SettingsViewController.h"
#import "SupportViewController.h"
#import "ActivityViewController.h"
#import "VoidRefundMainViewController.h"
#import "CreditCardSaleViewController.h"
#import "SignInViewController.h"
#import "PaymentSelectionViewController.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleMe];
    [self showTick];
    self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];
    NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
    if ([[prefs valueForKey:@"demo"]isEqualToString:@"No"])
    {
        self.btnSwitch.on = true;
        _btnSwitch.thumbTintColor =UIColor.whiteColor;
    }
    else
    {
        self.btnSwitch.on = false;
        _btnSwitch.tintColor = [ThemeColor changeGraphColor];
        _btnSwitch.thumbTintColor =[ThemeColor changeGraphColor];
    }

    // Do any additional setup after loading the view.
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)styleMe{
    self.settingsBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.btnSwitch.onTintColor = [ThemeColor changeGraphColor];
    self.settingsBtn.layer.cornerRadius = 10;
    self.settingsBtn.layer.masksToBounds = YES;
    
    self.c1.layer.cornerRadius = 10;
    self.c1.layer.masksToBounds = YES;
    
    self.c2.layer.cornerRadius = 10;
    self.c2.layer.masksToBounds = YES;
    
    self.c3.layer.cornerRadius = 10;
    self.c3.layer.masksToBounds = YES;
    
    self.c4.layer.cornerRadius = 10;
    self.c4.layer.masksToBounds = YES;
    
    self.c5.layer.cornerRadius = 10;
    self.c5.layer.masksToBounds = YES;
    
    self.c6.layer.cornerRadius = 10;
    self.c6.layer.masksToBounds = YES;
    
    self.c7.layer.cornerRadius = 10;
    self.c7.layer.masksToBounds = YES;
    
    self.c8.layer.cornerRadius = 10;
    self.c8.layer.masksToBounds = YES;
    
    self.wizardBtn.layer.shadowOpacity = 0.8;
    self.wizardBtn.layer.shadowRadius = 12;
    self.wizardBtn.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)showTick
{
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    NSString *theme=[prefs objectForKey:@"theme"];
    if([theme isEqualToString:@"darkGreen"])
    {
        
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=false;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

        
    }else if([theme isEqualToString:@"lightBlue"])
    {
        self.tick1.hidden=false;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }else if([theme isEqualToString:@"purple"]){
        
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=false;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }else if([theme isEqualToString:@"gray"]){
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=false;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }
    else if([theme isEqualToString:@"darkBlue"]){
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=false;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }
    else if([theme isEqualToString:@"lameBlue"]){
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=false;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }
    else if([theme isEqualToString:@"parrotGreen"]){
        self.tick1.hidden=true;
        self.tick2.hidden=true;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=false;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }
    else if([theme isEqualToString:@"red"]){
        self.tick1.hidden=true;
        self.tick2.hidden=false;
        self.tick3.hidden=true;
        self.tick4.hidden=true;
        self.tick5.hidden=true;
        self.tick6.hidden=true;
        self.tick7.hidden=true;
        self.tick8.hidden=true;
        self.wizardBtn.backgroundColor=[ThemeColor changeGraphColor];

    }
}
- (IBAction)lightBlue:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"lightBlue" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)red:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"red" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)purple:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"purple" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)mfmGreen:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"darkGreen" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)gray:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"gray" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)darkBlue:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"darkBlue" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)lameBlue:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"lameBlue" forKey:@"theme"];
    [self viewDidLoad];
}

- (IBAction)parrotGreen:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    [prefs setObject:@"parrotGreen" forKey:@"theme"];
    [self viewDidLoad];
}
- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
 
}
- (IBAction)runWizard:(id)sender {
    [self purple:self];
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    [prefs setValue:@"no" forKey:@"signIn"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SignInViewController *myVC = (SignInViewController *)[storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)switchAction:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    if (_btnSwitch.on) {
        [prefs setValue:@"No" forKey:@"demo"];
        sharedManager.url = @"https://reporting.prismpay.com/API/";
        sharedManager.transUrl = @"prod.prismpay";
        _btnSwitch.thumbTintColor =UIColor.whiteColor;
        
    }
    else
    {
        [prefs setValue:@"Yes" forKey:@"demo"];
        sharedManager.url = @"https://demo.prismpay.com/API/";
        sharedManager.transUrl = @"test.mycardstorage";
        _btnSwitch.tintColor = [ThemeColor changeGraphColor];
        _btnSwitch.thumbTintColor =[ThemeColor changeGraphColor];
    }
}
@end
