//
//  SupportViewController.h
//  MFmPay
//
//  Created by Prism Pay on 5/4/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>

@interface SupportViewController : BaseViewController<UITextViewDelegate,UIWebViewDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *supportBtn;
@property (weak, nonatomic) IBOutlet UIView *headerView;
- (IBAction)call:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *mailBtn;
- (IBAction)mail_action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;
- (IBAction)showSupport:(id)sender;
- (IBAction)showFAQ:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *faqImgView;
@property (weak, nonatomic) IBOutlet UIImageView *supportimgView;
@property (weak, nonatomic) IBOutlet UILabel *supportLbl;
@property (weak, nonatomic) IBOutlet UILabel *faqLbl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)creditAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *supportheaderlbl;
- (IBAction)payAction:(id)sender;
- (IBAction)settings:(id)sender;


@end
