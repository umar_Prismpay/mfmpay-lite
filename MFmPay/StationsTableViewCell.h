//
//  StationsTableViewCell.h
//  MFmPay
//
//  Created by Muhammad Umar on 10/05/2018.
//  Copyright © 2018 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *stationsLbl;

@end
