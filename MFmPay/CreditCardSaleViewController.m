//
//  CreditCardSaleViewController.m
//  MFmPay
//
//  Created by Prism Pay on 4/21/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "CreditCardSaleViewController.h"
#import "SettingsViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "VoidRefundMainViewController.h"
#import "SupportViewController.h"
#import "Luhn.h"
#import "UIView+Toast.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "XMLDictionary.h"
#import "SignatureApprovedViewController.h"
#import "RealSignatureApprovedViewController.h"
#import "PinDeclinedViewController.h"
#import "ProgressHUD.h"
#import "BKCardNumberField.h"
#import "EADSessionController.h"
#import "DELProtocol.h"
#import "BKMoneyUtils.h"
#import "ConnectorListModel.h"
#import "PaymentSelectionViewController.h"
@interface CreditCardSaleViewController ()
{
    float totallbl;
    BOOL isSwipe;
    BOOL isEmv;
    BOOL isValid;
    NSMutableArray *values;
    NSMutableArray *percent;
    float total;
    NSString *last4;
    NSMutableArray *modelListArray;
    ConnectorListModel * model;
    NSString *resultCode;
    NSString *amountTxt;
    NSString *MCSTransactionID;
    NSString *ProcessorApprovalCode;
    NSString *ProcessorTransactionID;
    NSString *ReferenceNumber;
    NSString *cardNumber;
    NSString * mcsacctID;
    NSString * sessionKey;
    NSMutableArray * deviceNames;
    NSMutableArray * deviceIDs;
    NSString *deviceID;
    NSString *token;
    NSString *nickName;
    NSString *cardTypeID;

}
@property (nonatomic, strong) EADSessionController *bltController;
@property (nonatomic, strong) EAAccessory *accessory;
@property (strong, nonatomic) NSString *communicationLog;

@end


@implementation CreditCardSaleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    mcsacctID = [prefs valueForKey:@"MCSAccountID"];
    //setup mp200 device
    mp200Device=[MP200 new];
    token = @"";
    mp200Device.mp200d=self;
    [mp200Device MP200BTCommunicationSetup];
 
    modelListArray = [[NSMutableArray alloc]init];
    [modelListArray addObject:@"No Device to show"];
    deviceID = @"";
    [self setNumKeyPad];
    [self hardwareSetup];
    // Do any additional setup after loading the view.
    values=[[NSMutableArray alloc]init];
    percent=[[NSMutableArray alloc]init];
    
    isSwipe=false;
    isEmv=false;
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.blur_view addGestureRecognizer: tapRec];
    self.zipcode.delegate=self;
    self.cardnum.delegate=self;
    self.cardnum.showsCardLogo = YES;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCreditCardInfo:) name:@"iMagSendCardInfo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDeviceIcon) name:@"sendDeviceStatus" object:nil];
    
    self.cvv.delegate=self;
    [self styleME];
    isValid=false;
    self.amount.delegate=self;
    [self.amount addTarget:self action:@selector(editMe:) forControlEvents:UIControlEventEditingChanged];
  
}
-(void)viewWillAppear:(BOOL)animated
{
    if( self.name.text == (id)[NSNull null] || [self.name.text isEqualToString:@"(null) (null)"])
    {
            self.expDate.text = @"";
            self.name.text = @"";
    }

}
-(void)viewWillDisappear:(BOOL)animated{
    [mp200Device closeMp200BlutoothSession];

}
-(void)updateDeviceIcon
{
//    sharedManager=[SingleTon sharedManager];
    //self.readytoswipe.hidden=YES;
    if(sharedManager.iMagDeviceConnected==1)
    {
        //self.deviceImage.image=[UIImage imageNamed:icon_imag];
       // self.readytoswipe.hidden=YES;
    }else if (sharedManager.UniMagDeviceConnected==1)
    {
      //  self.deviceImage.image=[UIImage imageNamed:icon_unimag];
       // self.readytoswipe.hidden=NO;
    }else{
      //  self.deviceImage.image=[UIImage imageNamed:@""];
        
    }
    
}
-(void)hardwareSetup{
//    sharedManager=[SingleTon sharedManager];
    if(sharedManager.iMagDeviceConnected==1)
    {
        [ProgressHUD showSuccess:NSLocalizedString(@"Device is connected please swipe a card", @"Device is connected please swipe a card") Interaction:YES];
    }else if(sharedManager.UniMagDeviceConnected==1)
    {
        [ProgressHUD showSuccess:NSLocalizedString(@"Device is connected \n\n Press Ready to Swipe Button", @"Device is connected \n\n Press Ready to Swipe Button") Interaction:YES];
        
    }
    
    
    
   
    
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDeviceIcon) name:@"sendDeviceStatus" object:nil];
}
-(void)getCreditCardInfo:(NSNotification *)notification
{
  
   

    NSDictionary *userInfo = notification.userInfo;
    
    NSString *ccname=[userInfo objectForKey:@"ccname"];
    NSString *ccnum=[userInfo objectForKey:@"ccnum"];
    NSString *expyear=[userInfo objectForKey:@"expyear"];
    NSString *expmonth=[userInfo objectForKey:@"expmonth"];
    NSString *swipedata=[userInfo objectForKey:@"swipedata"];
   // self.cardnum.text=[ccnum substringFromIndex:MAX((int)[ccnum length]-4, 0)];
    // self.txtFCard.text=[NSString stringWithFormat:@"%@",[ccnum substringWithRange:NSMakeRange(12, 4)]];
    self.name.text=ccname;
    [self.cardnum setCardNumber:ccnum];
    //    self.lblBillingAdress.text=[NSString stringWithFormat:@"%@",[ccnum substringWithRange:NSMakeRange(12, 4)]];
    self.ccnum=ccnum;

    [self textFieldDidEndEditing:self.cardnum];

    self.expDate.text=[NSString stringWithFormat:@"%@/%@",expmonth,expyear];
    self.expMonth=expmonth;
    self.expYear=expyear;
    

    self.swipedata =swipedata;
    
}

-(void)tap:(UITapGestureRecognizer *)tapRec{
    self.blur_view.hidden=true;
    self.tip_view.hidden=true;
    self.terminalListView.hidden = true;
    self.hardware_view.hidden=true;
   
    
}
-(void)editMe:(id)sender{
    NSString* strPrice = [sender text];
    static BOOL toggle = NO;
    if (toggle) {
        toggle = NO;
    }
    [percent removeAllObjects];
    [values removeAllObjects];
    strPrice = [strPrice stringByReplacingOccurrencesOfString:@"." withString:@""];
    toggle = YES;
    self.amount.text = [@"" stringByAppendingFormat:@"%0.2f", [strPrice floatValue]/100.0];
}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)styleME{
    
    self.pin1.delegate=self;
    self.pin2.delegate=self;
    self.pin3.delegate=self;
    self.pin4.delegate=self;
    
    self.hardware_header.backgroundColor=[ThemeColor changeGraphColor];
    self.terminalBtn.backgroundColor = [ThemeColor changeGraphColor];
    self.terminalHeader.backgroundColor = [ThemeColor changeGraphColor];
    self.proceed_Btn.backgroundColor=[ThemeColor changeGraphColor];
    self.proceed_Btn.layer.cornerRadius = self.proceed_Btn.bounds.size.width/2;
    self.proceed_Btn.layer.masksToBounds = YES;
    
    self.credit_cardBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.credit_cardBtn.layer.cornerRadius = self.proceed_Btn.bounds.size.width/2;
    self.credit_cardBtn.layer.masksToBounds = YES;
    
    
    self.pinBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.pinBtn.layer.cornerRadius = self.pinBtn.bounds.size.width/2;
    self.pinBtn.layer.masksToBounds = YES;
    
    
    self.pin_total.textColor=[ThemeColor changeGraphColor];
    self.tip_header.backgroundColor=[ThemeColor changeGraphColor];
    self.tip_process_btn.backgroundColor=[ThemeColor changeGraphColor];
    self.tip_process_btn.layer.cornerRadius = self.proceed_Btn.bounds.size.width/2;
    self.tip_process_btn.layer.masksToBounds = YES;
    
    
    self.resetBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.resetBtn.layer.cornerRadius =  self.resetBtn.bounds.size.width/2;
    self.resetBtn.layer.masksToBounds = YES;
    
    self.borderLine.backgroundColor=[ThemeColor changeGraphColor];
    self.name.delegate=self;
    self.address.delegate=self;
    self.address.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    self.address.layer.borderWidth=0.3;
    
    self.address.layer.cornerRadius = 5;
    self.address.layer.masksToBounds = YES;
    
   [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.address.text=@"";
    self.address.textColor=[UIColor blackColor];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    if([self.address.text isEqualToString:@""])
    {
        self.address.text=@"Address";
        self.address.textColor=[UIColor colorWithRed:0.82 green:0.85 blue:0.86 alpha:1.0];
    }
}




-(void) getListofConnectors
{
    
    if([self.amount.text isEqualToString:@""]||[self.amount.text isEqualToString:@"0.00"])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Amount greater than 0.00", @"Please Enter Amount greater than 0.00")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
    }
    else
    {
        deviceNames = [[NSMutableArray alloc]init];
        [deviceNames addObject:@"No Device available" ];
        deviceIDs = [[NSMutableArray alloc]init];
        [deviceIDs addObject:@"!"];
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [manager.requestSerializer setValue:@"/CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
            
           
            
            NSString *str=[NSString stringWithFormat:@"%@Mobile/TerminalsByAccount?mcsAccountID=%@",sharedManager.url,mcsacctID];
            
            
//            NSString *str=[NSString stringWithFormat:@"https://demo.prismpay.com/API/Mobile/TerminalsByAccount?mcsAccountID=%@",mcsacctID];
            [manager GET:str parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                
                NSArray *arr=[responseObject valueForKey:@"Object"];
                NSLog(@"%@",arr);
                
            
                for (id obj in arr)
                {
                     [deviceNames addObject:[obj valueForKey:@"TerminalName"]];
                    [deviceIDs addObject:[obj valueForKey:@"SerialNumber"]];
                    
                }
             
                self.blur_view.hidden=false;
                self.terminalListView.hidden = false;
                [self.terminalList reloadAllComponents];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                NSLog(@"error: %@", error);
            }];
        });
    });
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if(textField==self.cardnum)
//    {
//        if(self.cardnum.cardNumber.length>16)
//        {
//            [self.cardnum resignFirstResponder];
//            [self.cvv becomeFirstResponder];
//        }
//        else if(self.cardnum.cardNumber.length==16)
//        {
//            [self.cardnum resignFirstResponder];
//            [self.cvv becomeFirstResponder];
//        }
//    }
    if(textField==self.zipcode)
    {
        if(self.zipcode.text.length==5)
        {
            [self.zipcode resignFirstResponder];
            [self.cardnum becomeFirstResponder];
        }
    }
    if(textField==self.cvv)
    {
        
    NSString *currentString = [self.cvv.text stringByReplacingCharactersInRange:range withString:string];
    NSUInteger length = [currentString length];
        if([self.cardnum.cardCompanyName isEqualToString:@"American Express"])
        {
    if (length > 4) {
//        self.cvv.enabled=false;
//        [self.view makeToast:@"CVV Cannot be greater than 4 digits"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:nil
//                       image:nil
//                       style:nil
//                  completion:nil];
        [self.expDate becomeFirstResponder];
        return NO;
    }
    }
        else
        {
            if (length > 4) {
//                self.cvv.enabled=false;
//                [self.view makeToast:@"CVV Cannot be greater than 3 digits"
//                            duration:3.0
//                            position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                               title:nil
//                               image:nil
//                               style:nil
//                          completion:nil];
                [self.expDate becomeFirstResponder];
                
                return NO;
            }
        }

    }
    
    if(textField==self.pin1 ||textField==self.pin2 ||textField==self.pin3 ||textField==self.pin4 )
    {
        if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
            return NO;
        
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        // This 'tabs' to next field when entering digits
        if (newLength == 1) {
            if (textField == _pin1)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin2 afterDelay:0.2];
            }
            else if (textField == _pin2)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin3 afterDelay:0.2];
            }
            else if (textField == _pin3)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin4 afterDelay:0.2];
            }
        }
        //this goes to previous field as you backspace through them, so you don't have to tap into them individually
        else if (oldLength > 0 && newLength == 0) {
            if (textField == _pin4)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin3 afterDelay:0.1];
            }
            else if (textField == _pin3)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin2 afterDelay:0.1];
            }
            else if (textField == _pin2)
            {
                [self performSelector:@selector(setNextResponder:) withObject:_pin1 afterDelay:0.1];
            }
        }
        
        return newLength <= 1;
    }
    return YES;
}
- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length>0)
    if (textField == self.cardnum) {
        
//        self.cardnum.cardNumberFormatter.maskingCharacter = @"●";       // BLACK CIRCLE        25CF
//        self.cardnum.cardNumberFormatter.maskingGroupIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 3)];
        
        
        
        isValid = [Luhn validateString:self.cardnum.text];
        
//        if([self.cardnum.text isEqualToString:@"5454545454545454"])
//            isValid=true;
        
        if (isValid) {
            // process payment
            cardNumber=self.cardnum.text;
            last4=[cardNumber substringFromIndex:MAX((int)[cardNumber length]-4, 0)];
          
            
        }
        else {
            [self.view makeToast:NSLocalizedString(@"Please Enter Valid Card Number", @"Please Enter Valid Card Number")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
        }
        
        
        
        
        NSMutableString *str=[[NSMutableString alloc]init];
        for(NSUInteger i=0; i<self.cardnum.text.length-4;i++)
        {
            [str appendString:@"●"];
        }
      NSString *  result = [self.cardnum.text stringByReplacingCharactersInRange:NSMakeRange(0, self.cardnum.text.length-4) withString:str];
        
        self.cardnum.text=result;


        
    }
}
- (void)textFieldEditingChanged:(id)sender
{
  
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settings:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
-(BOOL)Validate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/yy"];
    NSDate *date1=[[NSDate alloc]init];
    date1=[dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:self.expDate.text];
    
    
       NSLog(@"%@",self.cvv.text);
       NSLog(@"%@",self.expDate.text);
    if([self.amount.text isEqualToString:@""]||[self.amount.text isEqualToString:@"0.00"])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Amount greater than 0.00", @"Please Enter Amount greater than 0.00")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if([date1 compare:dateFromString] == NSOrderedDescending)
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Valid Date",nil)
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    
    else  if([self.name.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Name", @"Please Enter Name")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
//    else  if([self.address.text isEqualToString:@""])
//    {
//        [self.view makeToast:@"Please Enter Address"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Alert"
//                       image:nil
//                       style:nil
//                  completion:nil];
//        return false;
//    }
//
//    else  if([self.zipcode.text isEqualToString:@""])
//    {
//        [self.view makeToast:@"Please Enter ZipCode"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Alert"
//                       image:nil
//                       style:nil
//                  completion:nil];
//        return false;
//    }
//    else  if([self.cvv.text isEqualToString:@""])
//    {
//        [self.view makeToast:@"Please Enter Card CVV"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Alert"
//                       image:nil
//                       style:nil
//                  completion:nil];
//        return false;
//    }
    
        else if(self.cvv.text.length<3 && ![self.cvv.text isEqualToString:@""])
        {
            [self.view makeToast:NSLocalizedString(@"Please Enter Valid Card CVV", @"Please Enter Valid Card CVV")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            return false;
        }
    else  if([self.expDate.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Card Expiry Date", @"Please Enter Card Expiry Date")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }

    else  if([self.cardnum.text isEqualToString:@""])
    {
        [self.view makeToast:NSLocalizedString(@"Please Enter Card Number", @"Please Enter Card Number")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self.cardnum.text isEqualToString:@""])
    {
        if(!isValid)
        {
            [self.view makeToast:NSLocalizedString(@"Please Enter Valid Card Number", @"Please Enter Valid Card Number")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            return false;
        }
    }
 
    
    return true;
}
- (IBAction)process_transaction:(id)sender {
    
    if([self Validate])
    {
       
        self.blur_view.hidden=false;
        self.tip_view.hidden=false;
        
        
          float totalinc=[self.amount.text floatValue];
        for(int i=0;i<=100;i++)
        {
            [percent addObject:[NSString stringWithFormat:@"%d",i]];
            [values addObject:[NSString stringWithFormat:@"%.02f",(totalinc*i)/100]];
        }
        self.bill_lbl.text=[NSString stringWithFormat:@"$%@",self.amount.text];
        self.tip_lbl.text=[NSString stringWithFormat:@"$%0.2f",[[percent objectAtIndex:0] floatValue]];
        float tot=[self.amount.text floatValue]+[[percent objectAtIndex:0] floatValue];
        self.total_lbl.text=[NSString stringWithFormat:@"$%0.2f",tot];
        total=[self.amount.text floatValue];
        totallbl=[self.amount.text floatValue];
        
        [self.tip_picker reloadAllComponents];
      
        
    }
   
}


-(void)setNumKeyPad{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad1)]];
    [numberToolbar sizeToFit];
    
    
    
    
    UIToolbar* cvvbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    cvvbar.barStyle = UIBarStyleBlack;
    cvvbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(cvvNext)]];
    [cvvbar sizeToFit];
    
    
    
    
    UIToolbar* zipbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    zipbar.barStyle = UIBarStyleBlack;
    zipbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(zipNext)]];
    [zipbar sizeToFit];
    
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar1.barStyle = UIBarStyleBlack;
    numberToolbar1.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad2)]];
    [numberToolbar1 sizeToFit];
    
    
    UIToolbar* numberToolbar2 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar2.barStyle = UIBarStyleBlack;
    numberToolbar2.items = @[
                             [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                             [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad3)]];
    [numberToolbar2 sizeToFit];
    
    
self.cvv.inputAccessoryView = cvvbar;
    self.expDate.inputAccessoryView=numberToolbar;
    self.amount.inputAccessoryView=numberToolbar1;
    self.cardnum.inputAccessoryView=numberToolbar2;
     self.zipcode.inputAccessoryView=zipbar;
    self.pin1.inputAccessoryView=numberToolbar;
    self.pin2.inputAccessoryView=numberToolbar;
    self.pin3.inputAccessoryView=numberToolbar;
    self.pin4.inputAccessoryView=numberToolbar;
}
-(void)cancelNumberPad1{
    
}
-(void)doneWithNumberPad1{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.amount resignFirstResponder];
    [self.cvv resignFirstResponder];
    [self.expDate resignFirstResponder];
    [self.cardnum resignFirstResponder];
    [self.zipcode resignFirstResponder];
    [self.pin1 resignFirstResponder];
    [self.pin2 resignFirstResponder];
    [self.pin3 resignFirstResponder];
    [self.pin4 resignFirstResponder];
}
-(void)doneWithNumberPad2{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.amount resignFirstResponder];
    [self.name becomeFirstResponder];
}

-(void)zipNext{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.zipcode resignFirstResponder];
    [self.cardnum becomeFirstResponder];
}
-(void)cvvNext{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.cvv resignFirstResponder];
    [self.expDate becomeFirstResponder];
}

-(void)doneWithNumberPad3{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.cardnum resignFirstResponder];
    [self.cvv becomeFirstResponder];
}
//#pragma uipickerview
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//    //Two columns
//    return 2;
//}
//
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//    //set number of rows
//    return 10;
//}


-(IBAction)tip_process:(id)sender
{
  
    if(isSwipe)
    {
        isSwipe=false;

        self.pin_total.text=[NSString stringWithFormat:@"$%.02f",total];
        self.blur_view.hidden=true;
        self.tip_view.hidden=true;
        if([self->mp200Device launchMP200Device]){
            [self.view setUserInteractionEnabled:NO];
            NSLog(@"%.02f",total);
            [self->mp200Device sendHex:[NSString stringWithFormat:@"%.02f",total] :@"swipeCard"];
        }
    }
    else if(isEmv)
    {
        isEmv=false;

        self.pin_total.text=[NSString stringWithFormat:@"$%.02f",total];
        self.blur_view.hidden=true;
        self.tip_view.hidden=true;
        if([self->mp200Device launchMP200Device]){
            [self.view setUserInteractionEnabled:NO];
            [self->mp200Device sendHex:[NSString stringWithFormat:@"%.02f",total] :@"insertCard"];
        }
    }
    else
    {
    self.pin_total.text=[NSString stringWithFormat:@"$%.02f",total];
    self.blur_view.hidden=true;
    self.tip_view.hidden=true;
    
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //            self.pin_view.hidden=false;
        //            self.resetBtn.hidden=true;
        //            self.pin_back.hidden=false;
        //            self.pin1.text=@"";
        //            self.pin2.text=@"";
        //            self.pin3.text=@"";
        //            self.pin4.text=@"";
        
        
        //For Signature View
        
        
        NSString *cardType;
    NSLog(@"%@",self.cardnum.cardNumberFormatter.cardPatternInfo.companyName);
    
    
    
        if([self.cardnum.cardCompanyName isEqualToString:@"MasterCard"])
        {
            cardType=@"3";
        }
        if([self.cardnum.cardCompanyName isEqualToString:@"American Express"])
        {
            cardType=@"1";
        }
        if([self.cardnum.cardCompanyName isEqualToString:@"Discover"])
        {
            cardType=@"2";
        }
        if([self.cardnum.cardCompanyName isEqualToString:@"Visa"])
        {
            cardType=@"4";
        }
        
        NSArray *substrings = [self.expDate.text componentsSeparatedByString:@"/"];
        NSString *first = [substrings objectAtIndex:0];
        NSString *second = [substrings objectAtIndex:1];
        
        
        
        NSString *fName;
        NSString *Lname;
        
        NSRange whiteSpaceRange = [self.name.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
        if (whiteSpaceRange.location != NSNotFound) {
            NSArray *nameString = [self.name.text componentsSeparatedByString:@" "];
            fName = [nameString objectAtIndex:0];
            Lname = [nameString objectAtIndex:1];
        }
        else
        {
            fName=self.name.text;
            Lname=@"";
        }
        NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
        cardNumber = [cardNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        first = [first stringByReplacingOccurrencesOfString:@" " withString:@""];
    second = [second stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *mcid=[prefs valueForKey:@"MCSID"];
        NSString * add = self.address.text;
        if ([add isEqualToString:@"Address"])
            add = @"";
    NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
        NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
        NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
        NSString *soapmessage = [NSString stringWithFormat:@ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                 "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                                 "<soap12:Header>\n"
                                 "<AuthHeader xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<UserName>MFUser</UserName>\n"
                                 "<Password>slYC8T#fhnK0tLp5</Password>\n"
                                 "</AuthHeader>\n"
                                 "</soap12:Header>\n"
                                 "<soap12:Body>\n"
                                 "<CreditSale_Soap xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<creditCardSale>\n"
                                 "<ServiceSecurity>\n"
                                 "<ServiceUserName>MF</ServiceUserName>\n"
                                 "<ServicePassword>kZJ33HgBhH$NFFdvE</ServicePassword>\n"
                                 "<MCSAccountID>%@</MCSAccountID>\n"
                                 "</ServiceSecurity>\n"
                                 "<TokenData>\n"
                                 "<Token></Token>\n"
                                 "<TokenType>5</TokenType>\n"
                                 "<Last4>%@</Last4>\n"
                                 "<CardNumber>%@</CardNumber>\n"
                                 "<CardType>%@</CardType>\n"
                                 "<ExpirationMonth>%@</ExpirationMonth>\n"
                                 "<ExpirationYear>%@</ExpirationYear>\n"
                                 "<NickName>EFGH456789</NickName>\n"
                                 "<FirstName>%@</FirstName>\n"
                                 "<LastName>%@</LastName>\n"
                                 "<StreetAddress>%@</StreetAddress>\n"
                                 "<ZipCode>%@</ZipCode>\n"
                                 "<CVV>%@</CVV>\n"
                                 "<XID></XID>\n"
                                 "<CAVV></CAVV>\n"
                                 "</TokenData>\n"
                                 "<TransactionData>\n"
                                 "<Amount>%@</Amount>\n"
                                 "<ReferenceNumber></ReferenceNumber>\n"
                                 "<TicketNumber></TicketNumber>\n"
                                 "<MCSTransactionID>0</MCSTransactionID>\n"
                                 "<GatewayID>1</GatewayID>\n"
                                 "</TransactionData>\n"
                                 "</creditCardSale>\n"
                                 "</CreditSale_Soap>\n"
                                 "</soap12:Body>\n"
                                 "</soap12:Envelope>\n",mcid,last4,cardNumber,cardType,first,second,fName,Lname,add,self.zipcode.text,self.cvv.text,[NSString stringWithFormat:@"%.2f",total]];
        
        
        
        
        NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
        
        
        [finalRequest addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        [finalRequest setHTTPMethod:@"POST"];
        [finalRequest setHTTPBody:soapdata];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
        NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"Response string: %@",fetchedXML);
            
            NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
            NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
            NSDictionary *credit=[soap valueForKey:@"CreditSale_SoapResponse"];
            NSDictionary *CreditSale_SoapResult=[credit valueForKey:@"CreditSale_SoapResult"];
            NSDictionary *result=[CreditSale_SoapResult valueForKey:@"Result"];
            resultCode=[result valueForKey:@"ResultCode"];
            amountTxt=[CreditSale_SoapResult valueForKey:@"Amount"];
            MCSTransactionID=[CreditSale_SoapResult valueForKey:@"MCSTransactionID"];
            if(![resultCode isEqualToString:@"2"])
            {
            ProcessorApprovalCode=[CreditSale_SoapResult valueForKey:@"ProcessorApprovalCode"];
            ProcessorTransactionID=[CreditSale_SoapResult valueForKey:@"ProcessorTransactionID"];
            ReferenceNumber=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
            }
            else
            {
                ProcessorApprovalCode=@"N/A";
                ProcessorTransactionID=@"N/A";
                ReferenceNumber=@"N/A";
            }
            if([resultCode isEqualToString:@"0"])
            {
                // [self performSegueWithIdentifier:@"signApproved" sender:self];
                [self performSegueWithIdentifier:@"realSign" sender:self];
            }
            else
            {
                [self performSegueWithIdentifier:@"pinDeclined" sender:self];
            }
            NSLog(@"dictionary: %@", xmlDoc);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];
        [task resume];
        
    }
    
        //Ends here
        
    

}
#pragma mark -
#pragma mark UIPickerViewDelegate

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UIView *v=[[UIView alloc]
               initWithFrame:CGRectMake(0,0,
                                        [self pickerView:pickerView widthForComponent:component],
                                        [self pickerView:pickerView rowHeightForComponent:component])];
    [v setOpaque:TRUE];
    [v setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *lbl=nil;
    lbl= [[UILabel alloc]
          initWithFrame:CGRectMake(8,0,
                                   [self pickerView:pickerView widthForComponent:component]-16,
                                   [self pickerView:pickerView rowHeightForComponent:component])];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setTextColor:[UIColor whiteColor]];
    NSString *ret=@"";
    switch (component) {
            
            
        case 0:
            if(pickerView == _tip_picker)
            {
            ret=[NSString stringWithFormat:@"$%@     %@%%",[values objectAtIndex:row],[percent objectAtIndex:row]];
            }
            else
            {
                if(deviceNames.count == 1)
                {
                    ret = @"No device available";
                }
                else
                {
                ret = deviceNames[row+1];
                [v setBackgroundColor:[ThemeColor changeGraphColor]];
                }
            }
            [v setBackgroundColor:[ThemeColor changeGraphColor]];
            break;
        case 1:
//            number=[(NSNumber*)[values objectAtIndex:row] intValue];
//        
//                [v setBackgroundColor:[UIColor greenColor]];
//            
//            ret = [NSString stringWithFormat:@"%d",number];
            break;
    }
    [lbl setText:ret];
    [lbl setFont:[UIFont boldSystemFontOfSize:18]];
    [v addSubview:lbl];
    
    return v;
    
   
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
 if(pickerView == self.tip_picker)
 {
    self.bill_lbl.text=[NSString stringWithFormat:@"$%.2f",totallbl];
    self.tip_lbl.text=[NSString stringWithFormat:@"$%0.2f",[[values objectAtIndex:row] floatValue]];
    total=totallbl+[[values objectAtIndex:row] floatValue];
    self.total_lbl.text=[NSString stringWithFormat:@"$%0.2f",total];
    self.amount.text= [NSString stringWithFormat:@"%.2f",total ];
 }
    else
    {
        deviceID = deviceIDs[row+1];
        
    }
    
}


- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if(pickerView == _tip_picker)
    {
    float ret=50;
    switch (component) {
        case 0:
            ret=self.tip_picker.frame.size.width;
            break;
        default:
            break;
    }
    return ret;
    }
    else
        return self.terminalList.frame.size.width;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 35;
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if(pickerView == self.tip_picker)
    {
    return 2;
    }
    else
        return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView == self.tip_picker)
    {
    switch (component) {
            
        case 0:
            return percent.count;
        case 1:
            return values.count;
        default:
            return 1;
    }
    }
    else
    {
        if(deviceNames.count ==1)
        return deviceNames.count;
        else
            return deviceNames.count-1;
    }
            
}

- (IBAction)pay_with_card:(id)sender {
    
    [self getListofConnectors];
    
//    if (sharedManager.UniMagDeviceConnected==1)
//    {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"requestforswipe" object:nil userInfo:nil];
//    }
//    else
//    {
//    self.blur_view.hidden=false;
//    self.hardware_view.hidden=false;
//    }
    
    
}


#pragma keyboard
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [self.address resignFirstResponder];
        [self.zipcode becomeFirstResponder];
    }
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.name)
    {
    [self.name resignFirstResponder];
        [self.address becomeFirstResponder];
    }
    if (textField == _pin1)
    {
        [_pin2 becomeFirstResponder];
    }
    else if (textField == _pin2)
    {
        [_pin3 becomeFirstResponder];
    }
    else if (textField == _pin3)
    {
        [_pin4 becomeFirstResponder];
    }
    
//    return NO;
    return YES;
}

- (IBAction)reset:(id)sender {
   

    
    self.cvv.text=@"";
    self.expDate.text=@"";
    self.name.text=@"";
    self.address.text=@"Address";
    self.address.textColor=[UIColor colorWithRed:0.82 green:0.85 blue:0.86 alpha:1.0];
    self.cardnum.text=@"";
    self.zipcode.text=@"";
    self.amount.text=@"";
    [self.cardnum updateCardLogoImage];
        self.cardnum.cardLogoImageView.image=[UIImage imageNamed:@"BKMoneyKit.bundle/CardLogo/default@2x"];;
}
- (IBAction)pin_process:(id)sender {
    NSString *cardType;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if([self.cardnum.cardCompanyName isEqualToString:@"MasterCard"])
    {
       cardType=@"3";
    }
    if([self.cardnum.cardCompanyName isEqualToString:@"American Express"])
    {
        cardType=@"1";
    }
    if([self.cardnum.cardCompanyName isEqualToString:@"Discover"])
    {
        cardType=@"2";
    }
    if([self.cardnum.cardCompanyName isEqualToString:@"Visa"])
    {
        cardType=@"4";
    }
  
    NSArray *substrings = [self.expDate.text componentsSeparatedByString:@"/"];
    NSString *first = [substrings objectAtIndex:0];
    NSString *second = [substrings objectAtIndex:1];
    NSString *fName;
    NSString *Lname;
   
    NSRange whiteSpaceRange = [self.name.text rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        NSArray *nameString = [self.name.text componentsSeparatedByString:@" "];
       fName = [nameString objectAtIndex:0];
       Lname = [nameString objectAtIndex:1];
    }
   else
   {
       fName=self.name.text;
       Lname=@"";
   }
    
NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
    NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
    NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
    NSString *soapmessage = [NSString stringWithFormat:@ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n"
                             "<soap12:Header>\n"
                             "<AuthHeader xmlns=\"https://MyCardStorage.com/\">\n"
                             "<UserName>MFUser</UserName>\n"
                             "<Password>slYC8T#fhnK0tLp5</Password>\n"
                             "</AuthHeader>\n"
                             "</soap12:Header>\n"
                             "<soap12:Body>\n"
                             "<CreditSale_Soap xmlns=\"https://MyCardStorage.com/\">\n"
                             "<creditCardSale>\n"
                             "<ServiceSecurity>\n"
                             "<ServiceUserName>MF</ServiceUserName>\n"
                             "<ServicePassword>kZJ33HgBhH$NFFdvE</ServicePassword>\n"
                             "<MCSAccountID>%@</MCSAccountID>\n"
                             "</ServiceSecurity>\n"
                             "<TokenData>\n"
                             "<Token></Token>\n"
                             "<TokenType>5</TokenType>\n"
                             "<Last4>%@</Last4>\n"
                             "<CardNumber>%@</CardNumber>\n"
                             "<CardType>%@</CardType>\n"
                             "<ExpirationMonth>%@</ExpirationMonth>\n"
                             "<ExpirationYear>%@</ExpirationYear>\n"
                             "<NickName>EFGH456789</NickName>\n"
                             "<FirstName>%@</FirstName>\n"
                             "<LastName>%@</LastName>\n"
                             "<StreetAddress>%@</StreetAddress>\n"
                             "<ZipCode>%@</ZipCode>\n"
                             "<CVV>%@</CVV>\n"
                             "<XID></XID>\n"
                             "<CAVV></CAVV>\n"
                             "</TokenData>\n"
                             "<TransactionData>\n"
                             "<Amount>%@</Amount>\n"
                             "<ReferenceNumber></ReferenceNumber>\n"
                             "<TicketNumber></TicketNumber>\n"
                             "<MCSTransactionID>0</MCSTransactionID>\n"
                             "<GatewayID>1</GatewayID>\n"
                             "</TransactionData>\n"
                             "</creditCardSale>\n"
                             "</CreditSale_Soap>\n"
                             "</soap12:Body>\n"
                             "</soap12:Envelope>\n",mcsacctID,last4,cardNumber,cardType,first,second,fName,Lname,self.address.text,self.zipcode.text,self.cvv.text,[NSString stringWithFormat:@"%.2f",total]];
    
    
    
    
    NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
   
    
    [finalRequest addValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];

    [finalRequest setHTTPMethod:@"POST"];
    [finalRequest setHTTPBody:soapdata];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response string: %@",fetchedXML);
        
        NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
        NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
        NSDictionary *credit=[soap valueForKey:@"CreditSale_SoapResponse"];
        NSDictionary *CreditSale_SoapResult=[credit valueForKey:@"CreditSale_SoapResult"];
        NSDictionary *result=[CreditSale_SoapResult valueForKey:@"Result"];
        resultCode=[result valueForKey:@"ResultCode"];
      amountTxt=[CreditSale_SoapResult valueForKey:@"Amount"];
        MCSTransactionID=[CreditSale_SoapResult valueForKey:@"MCSTransactionID"];
        ProcessorApprovalCode=[CreditSale_SoapResult valueForKey:@"ProcessorApprovalCode"];
       ProcessorTransactionID=[CreditSale_SoapResult valueForKey:@"ProcessorTransactionID"];
        ReferenceNumber=[CreditSale_SoapResult valueForKey:@"ReferenceNumber"];
        if([resultCode isEqualToString:@"0"])
        {
         [self performSegueWithIdentifier:@"signApproved" sender:self];
            //[self performSegueWithIdentifier:@"realSign" sender:self];
        }
        else
        {
            [self performSegueWithIdentifier:@"pinDeclined" sender:self];
            
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSLog(@"dictionary: %@", xmlDoc);
    }];
    [task resume];
    
    
    

    
    
    
    
   
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    
    NSString *mcid=[prefs valueForKey:@"MCSID"];
   // SignatureApprovedViewController *vcToPushTo = segue.destinationViewController;
    RealSignatureApprovedViewController *vcToPushTo=segue.destinationViewController;
    vcToPushTo.ProcAppCode=ProcessorApprovalCode;
    vcToPushTo.MCSTransID=[NSString stringWithFormat:@"%@",mcid];
    vcToPushTo.amounttxt= [NSString stringWithFormat:@"%@",amountTxt];
    vcToPushTo.RefNum=ReferenceNumber;
    vcToPushTo.ProceTransID=ProcessorTransactionID;
    vcToPushTo.saleType = @"Credit Card: Sale";
  
    
    
    
    
    
    PinDeclinedViewController *dec=segue.destinationViewController;
    dec.ProcAppCode=ProcessorApprovalCode;
    dec.MCSTransID=[NSString stringWithFormat:@"%@",mcid];
    dec.amounttxt= [NSString stringWithFormat:@"%@",amountTxt];
    dec.RefNum=ReferenceNumber;
    dec.ProceTransID= ProcessorTransactionID;
    dec.saleType = @"Credit Card: Sale";
    

}





- (IBAction)mp200Emv:(id)sender {
    
    if([self.amount.text isEqualToString:@"0.00"]||self.amount.text.length==0)
    {
        self.hardware_view.hidden=true;
        self.blur_view.hidden=true;
        [self.view makeToast:@"Please Enter amount"
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:nil
                       image:nil
                       style:nil
                  completion:nil];
    }
    else
    {
//    if([self->mp200Device launchMP200Device]){
//        [self.view setUserInteractionEnabled:NO];
//        [self->mp200Device sendHex:[NSString stringWithFormat:@"%.2f",[self.amount.text floatValue]] :@"insertCard"];
//    }
        isEmv=true;
        isSwipe=false;
        self.hardware_view.hidden=true;
        self.tip_view.hidden=false;
         self.blur_view.hidden=false;
     
        float totalinc=[self.amount.text floatValue];
        for(int i=0;i<=100;i++)
        {
            [percent addObject:[NSString stringWithFormat:@"%d",i]];
            [values addObject:[NSString stringWithFormat:@"%.02f",(totalinc*i)/100]];
        }
        self.bill_lbl.text=[NSString stringWithFormat:@"$%@",self.amount.text];
        
        NSLog(@"%@",self.amount.text);
        self.tip_lbl.text=[NSString stringWithFormat:@"$%0.2f",[[percent objectAtIndex:0] floatValue]];
       // float tot=[self.amount.text floatValue]+[[percent objectAtIndex:0] floatValue];
       //2 self.total_lbl.text=[NSString stringWithFormat:@"$%0.2f",tot];
        total=[self.amount.text floatValue];
        totallbl=[self.amount.text floatValue];
        [self.tip_picker reloadAllComponents];
        
    }
}

- (IBAction)mp200Swipe:(id)sender {
    if([self.amount.text isEqualToString:@"0.00"]||self.amount.text.length==0)
    {
        self.hardware_view.hidden=true;
        self.blur_view.hidden=true;
        [self.view makeToast:@"Please Enter amount"
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:nil
                       image:nil
                       style:nil
                  completion:nil];
    }
    else
    {
//    if([self->mp200Device launchMP200Device]){
//        [self.view setUserInteractionEnabled:NO];
//        [self->mp200Device sendHex:[NSString stringWithFormat:@"%.2f",[self.amount.text floatValue]] :@"swipeCard"];
//    }
        isEmv=false;
        isSwipe=true;
        self.hardware_view.hidden=true;
        self.blur_view.hidden=false;
        self.tip_view.hidden=false;
     
        
        float totalinc=[self.amount.text floatValue];
        for(int i=0;i<=100;i++)
        {
            [percent addObject:[NSString stringWithFormat:@"%d",i]];
            [values addObject:[NSString stringWithFormat:@"%.02f",(totalinc*i)/100]];
        }
        self.bill_lbl.text=[NSString stringWithFormat:@"$%@",self.amount.text];
        self.tip_lbl.text=[NSString stringWithFormat:@"$%0.2f",[[percent objectAtIndex:0] floatValue]];
        float tot=[self.amount.text floatValue]+[[percent objectAtIndex:0] floatValue];
        self.total_lbl.text=[NSString stringWithFormat:@"$%0.2f",tot];
        total=[self.amount.text floatValue];
        totallbl=[self.amount.text floatValue];
        [self.tip_picker reloadAllComponents];
    }
}

- (IBAction)uniMag:(id)sender {
    
    

    
    
    self.hardware_view.hidden=true;
    self.blur_view.hidden=true;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"requestforswipe" object:nil userInfo:nil];
}

- (IBAction)iMag:(id)sender {
    self.hardware_view.hidden=true;
     self.blur_view.hidden=true;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"requestforswipe" object:nil userInfo:nil];
}

- (IBAction)paxD20:(id)sender {
}
- (IBAction)back:(id)sender {
    
    self.resetBtn.hidden=false;
    self.pin_view.hidden=true;
    self.pin_back.hidden=true;
}
//mp200 delegate methods
-(void)MP200ResponseCompleted{
    NSLog(@"8. MP200ResponseCompleted");
    [self.view setUserInteractionEnabled:YES];
    
}
-(void)processMP200Response:(NSString *)xmlResponse{
    
    NSLog(@"5. processMP200Response");
}
- (IBAction)selectTerminal:(id)sender {
    if (deviceNames.count == 1)
    {
        
        [self.view makeToast:NSLocalizedString(@"No device selected", @"No device selected")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
    }
    else
    {
     
    
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        
        
        //    model = modelListArray[row];
        
        if ([deviceID isEqualToString:@""])
        {
            deviceID = [deviceIDs objectAtIndex:1];
        }
        NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
        
        NSString *mcid=[prefs valueForKey:@"MCSID"];
NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
        NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
        NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
        NSString *soapmessage = [NSString stringWithFormat:@ "<soap:Envelope\n"
                                 "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                                 "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
                                 "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                                 "<soap:Header>\n"
                                 "<AuthHeader\n"
                                 "xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<UserName>MFUser</UserName>\n"
                                 "<Password>slYC8T#fhnK0tLp5</Password>\n"
                                 "</AuthHeader>\n"
                                 "</soap:Header>\n"
                                 "<soap:Body>\n"
                                 "<CreateTerminalSession\n"
                                 "xmlns=\"https://MyCardStorage.com/\">\n"
                                 "<xml>&lt;Request&gt;\n"
                                 "&lt;ServiceLogin&gt;\n"
                                 "&lt;ServiceUserName&gt;MF&lt;/ServiceUserName&gt;\n"
                                 "&lt;ServicePassword&gt;kZJ33HgBhH$NFFdvE&lt;/ServicePassword&gt;\n"
                                 "&lt;MCSAccountID&gt;%@&lt;/MCSAccountID&gt;\n"
                                 "&lt;/ServiceLogin&gt;\n"
                                 "&lt;SaleInformation&gt;\n"
                                 "&lt;DeviceID&gt;%@&lt;/DeviceID&gt;\n"
                                 "&lt;Type&gt;Sale&lt;/Type&gt;\n"
                                 "&lt;Amount&gt;%f&lt;/Amount&gt;\n"
                                 "&lt;TipAmount /&gt;\n"
                                 "&lt;AutoTransmit /&gt;\n"
                                 "&lt;/SaleInformation&gt;\n"
                                 "&lt;ItemList&gt;\n"
                                 " &lt;Item&gt;\n"
                                 "&lt;Description /&gt;\n"
                                 "&lt;Amount /&gt;\n"
                                 "&lt;TaxAmount /&gt;\n"
                                 "&lt;/Item&gt;\n"
                                 "&lt;/ItemList&gt;\n"
                                 "&lt;OrderInfo&gt;\n"
                                 "&lt;TicketNO /&gt;\n"
                                 "&lt;TableNO /&gt;\n"
                                 "&lt;EmployeeID /&gt;\n"
                                 "&lt;/OrderInfo&gt;\n"
                                 "&lt;/Request&gt;</xml>\n"
                                 "</CreateTerminalSession>\n"
                                 "</soap:Body>\n"
                                 "</soap:Envelope>",mcid,deviceID,[self.amount.text floatValue]];
        
        
        
        
        NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
        
        
        [finalRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [finalRequest addValue:@"https://MyCardStorage.com/CreateTerminalSession" forHTTPHeaderField:@"SOAPAction"];
    
    [finalRequest setHTTPMethod:@"POST"];
    [finalRequest setHTTPBody:soapdata];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response string: %@",fetchedXML);
        
        NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
        NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
        NSDictionary * Tresponse = [soap valueForKey:@"CreateTerminalSessionResponse"];
        if(Tresponse == (id)[NSNull null] || Tresponse.count == 0)
        {
            NSDictionary * soapFault = [soap valueForKey:@"soap:Fault"];
            NSString * message = [soapFault valueForKey:@"faultstring"];
            [self.view makeToast:NSLocalizedString(message,message)
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            sessionKey = @"";
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.blur_view.hidden = true;
            self.terminalListView.hidden = true;
            
        }
        else
        {
        NSString *credit=[Tresponse valueForKey:@"CreateTerminalSessionResult"];
        NSScanner *scanner = [NSScanner scannerWithString:credit];
        [scanner scanUpToString:@"<SessionID>" intoString:nil];
       NSString* postTel = [credit substringFromIndex:scanner.scanLocation];
        NSString *preTel;
        NSScanner *scanner1 = [NSScanner scannerWithString:postTel];
        [scanner1 scanUpToString:@"</SessionID>" intoString:&preTel];
        
        NSLog(preTel);
        
        sessionKey = [preTel substringFromIndex:11];
        }
        
        NSLog(@"dictionary: %@", xmlDoc);
        
    }];
    [task resume];
    
    }
    double delayInSeconds =5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if ([sessionKey isEqualToString:@""])
        {
            [self.view makeToast:NSLocalizedString(@"No session key found", @"No session key found")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            
        }
        else
        {
            double delayInSeconds =40.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self getTokenData];
            });
        }
    });
   
    
}
-(void)getTokenData{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    //    model = modelListArray[row];
    
    
    
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    
    NSString *mcid=[prefs valueForKey:@"MCSID"];
NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
    NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
    NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
    NSString *soapmessage = [NSString stringWithFormat:@ "<soap:Envelope\n"
                             "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                             "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
                             "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                             "<soap:Header>\n"
                             "<AuthHeader\n"
                             "xmlns=\"https://MyCardStorage.com/\">\n"
                             "<UserName>MFUser</UserName>\n"
                             "<Password>slYC8T#fhnK0tLp5</Password>\n"
                             "</AuthHeader>\n"
                             "</soap:Header>\n"
                             "<soap:Body>\n"
                             "<RetrieveTerminalTokenData\n"
                             "xmlns=\"https://MyCardStorage.com/\">\n"
                             "<xml>\n"
                             "&lt;Request&gt;\n"
                             "&lt;ServiceLogin&gt;\n"
                             "&lt;ServiceUserName&gt;MF&lt;/ServiceUserName&gt;\n"
                             "&lt;ServicePassword&gt;kZJ33HgBhH$NFFdvE&lt;/ServicePassword&gt;\n"
                             "&lt;MCSAccountID&gt;%@&lt;/MCSAccountID&gt;\n"
                             "&lt;/ServiceLogin&gt;\n"
                             "&lt;SaleInformation&gt;\n"
                             "&lt;SessionID&gt;%@&lt;/SessionID&gt;\n"
                             "&lt;/SaleInformation&gt;\n"
                             "&lt;/Request&gt;\n"
                             "</xml>\n"
                             "</RetrieveTerminalTokenData>\n"
                             "</soap:Body>\n"
                             "</soap:Envelope>",mcid,sessionKey];
    
    
    
    
    NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
    
    
    [finalRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [finalRequest addValue:@"https://MyCardStorage.com/RetrieveTerminalTokenData" forHTTPHeaderField:@"SOAPAction"];
    
    [finalRequest setHTTPMethod:@"POST"];
    [finalRequest setHTTPBody:soapdata];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response string: %@",fetchedXML);
        self.terminalListView.hidden  = true;
        self.blur_view.hidden = true;
        
        NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
        NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
        NSDictionary *Tresponse=[soap valueForKey:@"RetrieveTerminalTokenDataResponse"];
        
        
        
        NSString *credit=[Tresponse valueForKey:@"RetrieveTerminalTokenDataResult"];
        NSScanner *scanner = [NSScanner scannerWithString:credit];
        [scanner scanUpToString:@"<Token>" intoString:nil];
        NSString* postTel = [credit substringFromIndex:scanner.scanLocation];
        NSString *preTel;
        NSScanner *scanner1 = [NSScanner scannerWithString:postTel];
        [scanner1 scanUpToString:@"</Token>" intoString:&preTel];
        
        token = [self trimString:preTel];
        
    
        
        [scanner scanUpToString:@"<Last4>" intoString:nil];
        NSString* last4post = [credit substringFromIndex:scanner.scanLocation];
        NSString *last;
        NSScanner *last4Scaner = [NSScanner scannerWithString:last4post];
        [last4Scaner scanUpToString:@"</Last4>" intoString:&last];
        
        last4 = [self trimString:last];
        _cardnum.cardNumber = last4;
        
        
        
        [scanner scanUpToString:@"<CardTypeID>" intoString:nil];
        NSString* CardTypeIDpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *CardTypeIDs;
        NSScanner *CardTypeIDScaner = [NSScanner scannerWithString:CardTypeIDpost];
        [CardTypeIDScaner scanUpToString:@"</CardTypeID>" intoString:&CardTypeIDs];
        
        cardTypeID = [self trimString:CardTypeIDs];
        
        
        
        [scanner scanUpToString:@"<ExpMonth>" intoString:nil];
        NSString* ExpMonthpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ExpiryMonth;
        NSScanner *ExpMonthScaner = [NSScanner scannerWithString:ExpMonthpost];
        [ExpMonthScaner scanUpToString:@"</ExpMonth>" intoString:&ExpiryMonth];
        
        _expMonth = [self trimString:ExpiryMonth];
        
        
        [scanner scanUpToString:@"<ExpYear>" intoString:nil];
        NSString* ExpYearpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ExpYear;
        NSScanner *ExpYearScaner = [NSScanner scannerWithString:ExpYearpost];
        [ExpYearScaner scanUpToString:@"</ExpYear>" intoString:&ExpYear];
        
        _expYear = [self trimString:ExpYear];
        
        
        
        [scanner scanUpToString:@"<FirstName>" intoString:nil];
        NSString* FirstNamepost = [credit substringFromIndex:scanner.scanLocation];
        NSString *FirstName;
        NSScanner *FirstNameScaner = [NSScanner scannerWithString:FirstNamepost];
        [FirstNameScaner scanUpToString:@"</FirstName>" intoString:&FirstName];
        
        FirstName = [self trimString:FirstName];
        
        
        
        [scanner scanUpToString:@"<LastName>" intoString:nil];
        NSString* LastNamePost = [credit substringFromIndex:scanner.scanLocation];
        NSString *LastName;
        NSScanner *LastNameScanner = [NSScanner scannerWithString:LastNamePost];
        [LastNameScanner scanUpToString:@"</LastName>" intoString:&LastName];
        
        LastName = [self trimString:LastName];
        
        if(![LastName isEqualToString:@""] && ![FirstName isEqualToString:@""])
        {
         _name.text = [NSString stringWithFormat:@"%@ %@",FirstName,LastName];
        }
        else
        {
            _name.text = @"";
        }
        [scanner scanUpToString:@"<Address>" intoString:nil];
        NSString* AddressPost = [credit substringFromIndex:scanner.scanLocation];
        NSString *Address;
        NSScanner *AddressScanner = [NSScanner scannerWithString:AddressPost];
        [AddressScanner scanUpToString:@"</Address>" intoString:&Address];
        
        Address = [self trimString:Address];
        
        
        
        [scanner scanUpToString:@"<ZipCode>" intoString:nil];
        NSString* ZipCodepost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ZipCode;
        NSScanner *ZipCodeScaner = [NSScanner scannerWithString:ZipCodepost];
        [ZipCodeScaner scanUpToString:@"</ZipCode>" intoString:&ZipCode];
        
        
        _zipcode.text = [self trimString:ZipCode];
    
        
        [scanner scanUpToString:@"<Amount>" intoString:nil];
        NSString* Amountpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *Amount;
        NSScanner *AmountScaner = [NSScanner scannerWithString:Amountpost];
        [AmountScaner scanUpToString:@"</Amount>" intoString:&Amount];
        amountTxt = [self trimString:Amount];
        if(amountTxt.length == 0)
            amountTxt = _amount.text;
        
        if(_expMonth.length>0)
        {
        self.expDate.text=[NSString stringWithFormat:@"%@/%@",_expMonth,_expYear];
        }
        else
        {
            self.expDate.text = @"";
        }
        NSLog(@"dictionary: %@", xmlDoc);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (token.length == 0)
           {
               [self performSegueWithIdentifier:@"pinDeclined" sender:self];
                // [self performSegueWithIdentifier:@"signApproved" sender:self];
               
            }
            else
            {
                [self performSegueWithIdentifier:@"realSign" sender:self];
            }
    }];
    [task resume];
    
}
-(void)getResponseFromTerminal{
    
    
    
    
    
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    
    NSString *mcid=[prefs valueForKey:@"MCSID"];
    NSString *urlStringgetairport = [NSString stringWithFormat:@"https://%@.com/api/api.asmx",sharedManager.transUrl];
    NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
    NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
    NSString *soapmessage = [NSString stringWithFormat:@ "<soap:Envelope\n"
                             "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                             "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
                             "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                             "<soap:Header>\n"
                             "<AuthHeader\n"
                             "xmlns=\"https://MyCardStorage.com/\">\n"
                             "<UserName>MFUser</UserName>\n"
                             "<Password>slYC8T#fhnK0tLp5</Password>\n"
                             "</AuthHeader>\n"
                             "</soap:Header>\n"
                             "<soap:Body>\n"
                             "<provisioningTerminalRequest\n"
                             "xmlns=\"https://MyCardStorage.com/\">\n"
                             "<xml>\n"
                             "&lt;Request&gt;\n"
                             "&lt;ServiceSecurity&gt;\n"
                             "&lt;ServiceUserName&gt;MF&lt;/ServiceUserName&gt;\n"
                             "&lt;ServicePassword&gt;kZJ33HgBhH$NFFdvE&lt;/ServicePassword&gt;\n"
                             "&lt;MCSAccountID&gt;%@&lt;/MCSAccountID&gt;\n"
                             "&lt;/ServiceSecurity&gt;\n"
                             "&lt;SaleInformation&gt;\n"
                             "&lt;SessionID&gt;%@&lt;/SessionID&gt;\n"
                             "&lt;/SaleInformation&gt;\n"
                             "&lt;/Request&gt;\n"
                             "</xml>\n"
                             "</provisioningTerminalRequest>\n"
                             "</soap:Body>\n"
                             "</soap:Envelope>",mcid,sessionKey];
    
    
    
    
    NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
    
    
    [finalRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [finalRequest addValue:@"https://MyCardStorage.com/RetrieveTerminalTokenData" forHTTPHeaderField:@"SOAPAction"];
    
    [finalRequest setHTTPMethod:@"POST"];
    [finalRequest setHTTPBody:soapdata];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes =  [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:finalRequest completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        NSString *fetchedXML = [[NSString alloc] initWithData:(NSData *)responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response string: %@",fetchedXML);
        self.terminalListView.hidden  = true;
        self.blur_view.hidden = true;
        
        NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:fetchedXML];
        NSDictionary *soap=[xmlDoc valueForKey:@"soap:Body"];
        NSDictionary *Tresponse=[soap valueForKey:@"RetrieveTerminalTokenDataResponse"];
        
        
        
        NSString *credit=[Tresponse valueForKey:@"RetrieveTerminalTokenDataResult"];
        NSScanner *scanner = [NSScanner scannerWithString:credit];
        [scanner scanUpToString:@"<Token>" intoString:nil];
        NSString* postTel = [credit substringFromIndex:scanner.scanLocation];
        NSString *preTel;
        NSScanner *scanner1 = [NSScanner scannerWithString:postTel];
        [scanner1 scanUpToString:@"</Token>" intoString:&preTel];
        
        token = [self trimString:preTel];
        
        
        
        [scanner scanUpToString:@"<Last4>" intoString:nil];
        NSString* last4post = [credit substringFromIndex:scanner.scanLocation];
        NSString *last;
        NSScanner *last4Scaner = [NSScanner scannerWithString:last4post];
        [last4Scaner scanUpToString:@"</Last4>" intoString:&last];
        
        last4 = [self trimString:last];
        _cardnum.cardNumber = last4;
        
        
        
        [scanner scanUpToString:@"<CardTypeID>" intoString:nil];
        NSString* CardTypeIDpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *CardTypeIDs;
        NSScanner *CardTypeIDScaner = [NSScanner scannerWithString:CardTypeIDpost];
        [CardTypeIDScaner scanUpToString:@"</CardTypeID>" intoString:&CardTypeIDs];
        
        cardTypeID = [self trimString:CardTypeIDs];
        
        
        
        [scanner scanUpToString:@"<ExpMonth>" intoString:nil];
        NSString* ExpMonthpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ExpiryMonth;
        NSScanner *ExpMonthScaner = [NSScanner scannerWithString:ExpMonthpost];
        [ExpMonthScaner scanUpToString:@"</ExpMonth>" intoString:&ExpiryMonth];
        
        _expMonth = [self trimString:ExpiryMonth];
        
        
        [scanner scanUpToString:@"<ExpYear>" intoString:nil];
        NSString* ExpYearpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ExpYear;
        NSScanner *ExpYearScaner = [NSScanner scannerWithString:ExpYearpost];
        [ExpYearScaner scanUpToString:@"</ExpYear>" intoString:&ExpYear];
        
        _expYear = [self trimString:ExpYear];
        
        
        
        [scanner scanUpToString:@"<FirstName>" intoString:nil];
        NSString* FirstNamepost = [credit substringFromIndex:scanner.scanLocation];
        NSString *FirstName;
        NSScanner *FirstNameScaner = [NSScanner scannerWithString:FirstNamepost];
        [FirstNameScaner scanUpToString:@"</FirstName>" intoString:&FirstName];
        
        FirstName = [self trimString:FirstName];
        
        
        
        [scanner scanUpToString:@"<LastName>" intoString:nil];
        NSString* LastNamePost = [credit substringFromIndex:scanner.scanLocation];
        NSString *LastName;
        NSScanner *LastNameScanner = [NSScanner scannerWithString:LastNamePost];
        [LastNameScanner scanUpToString:@"</LastName>" intoString:&LastName];
        
        LastName = [self trimString:LastName];
        
        if(![LastName isEqualToString:@""] && ![FirstName isEqualToString:@""])
        {
            _name.text = [NSString stringWithFormat:@"%@ %@",FirstName,LastName];
        }
        else
        {
            _name.text = @"";
        }
        [scanner scanUpToString:@"<Address>" intoString:nil];
        NSString* AddressPost = [credit substringFromIndex:scanner.scanLocation];
        NSString *Address;
        NSScanner *AddressScanner = [NSScanner scannerWithString:AddressPost];
        [AddressScanner scanUpToString:@"</Address>" intoString:&Address];
        
        Address = [self trimString:Address];
        
        
        
        [scanner scanUpToString:@"<ZipCode>" intoString:nil];
        NSString* ZipCodepost = [credit substringFromIndex:scanner.scanLocation];
        NSString *ZipCode;
        NSScanner *ZipCodeScaner = [NSScanner scannerWithString:ZipCodepost];
        [ZipCodeScaner scanUpToString:@"</ZipCode>" intoString:&ZipCode];
        
        
        _zipcode.text = [self trimString:ZipCode];
        
        
        [scanner scanUpToString:@"<Amount>" intoString:nil];
        NSString* Amountpost = [credit substringFromIndex:scanner.scanLocation];
        NSString *Amount;
        NSScanner *AmountScaner = [NSScanner scannerWithString:Amountpost];
        [AmountScaner scanUpToString:@"</Amount>" intoString:&Amount];
        amountTxt = [self trimString:Amount];
        if(amountTxt.length == 0)
            amountTxt = _amount.text;
        
        if(_expMonth.length>0)
        {
            self.expDate.text=[NSString stringWithFormat:@"%@/%@",_expMonth,_expYear];
        }
        else
        {
            self.expDate.text = @"";
        }
        NSLog(@"dictionary: %@", xmlDoc);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (token.length == 0)
        {
            [self performSegueWithIdentifier:@"pinDeclined" sender:self];
            // [self performSegueWithIdentifier:@"signApproved" sender:self];
            
        }
        else
        {
            [self performSegueWithIdentifier:@"realSign" sender:self];
        }
    }];
    [task resume];
    
    
    
    
    
    
    
    
}
- (IBAction)payAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
-(NSString*)trimString:(NSString *)string{
    
    NSUInteger location = [string rangeOfString:@">"].location + 1;
    
    return [string substringFromIndex:location];
   
}
@end
