//
//  DropDownTableViewCell.m
//  MFmPay
//
//  Created by Prism Pay on 5/24/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "DropDownTableViewCell.h"

@implementation DropDownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
