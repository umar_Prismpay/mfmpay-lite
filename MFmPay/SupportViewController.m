//
//  SupportViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/4/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//
#define IS_IPHONE_6 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )667 ) < DBL_EPSILON )
#define IS_IPHONE_6_plus ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )736 ) < DBL_EPSILON )
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#import "SupportViewController.h"
#import "MBProgressHUD.h"
#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import <MessageUI/MessageUI.h>
#import "PaymentSelectionViewController.h"
#import <UIView+Toast.h>
@interface SupportViewController ()

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView.delegate=self;
    [self styleMe];
    self.view.backgroundColor=[UIColor whiteColor];
    // Do any additional setup after loading the view.
    self.webView.delegate=self;
    NSString *fullURL = NSLocalizedString(@"http://democarts.info/faq/mfm/en_faq.html", @"http://democarts.info/faq/mfm/en_faq.html") ;
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    [self webView:self.webView shouldStartLoadWithRequest:requestObj navigationType:UIWebViewNavigationTypeReload];
    [self addDoneToolBarToKeyboard:self.textView];
    
    self.versionLbl.text =[NSString stringWithFormat:@"V %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotate {
    return NO;
}
-(void)styleMe{
    self.mailBtn.backgroundColor=[ThemeColor changeGraphColor];
    if(IS_IPHONE_5)
    {
        self.mailBtn.layer.cornerRadius = self.mailBtn.bounds.size.width/2.3;
    }
    else
    self.mailBtn.layer.cornerRadius = self.mailBtn.bounds.size.width/2;
    self.mailBtn.layer.masksToBounds = YES;
    self.supportBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.supportBtn.layer.cornerRadius = 10;
    self.supportBtn.layer.masksToBounds = YES;
    self.headerView.backgroundColor=[ThemeColor changeGraphColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //Start the progressbar..
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //Stop or remove progressbar
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //Stop or remove progressbar and show error
}

- (IBAction)call:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",@"8552202840"]]];
}
- (IBAction)mail_action:(id)sender {
//
//    NSString *emailTitle =NSLocalizedString(@"Support", @"Support");
//    // Email Content
//    NSString *messageBody = self.textView.text;
//    // To address
//    NSArray *toRecipents = [NSArray arrayWithObject:@"support@mfmpay.com"];
//
//    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
//    mc.mailComposeDelegate = self;
//    [mc setSubject:emailTitle];
//    [mc setMessageBody:messageBody isHTML:NO];
//    [mc setToRecipients:toRecipents];
//
//    // Present mail view controller on screen
//   // [self presentViewController:mc animated:YES completion:NULL];
//    [self presentViewController:mc animated:YES completion:NULL];
//
//
////    [self dismissViewControllerAnimated:YES completion:^{
////        [self presentViewController:mc animated:YES completion:nil];
////    }];
    
    
    
    
    
    

    
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
                NSString *message = self.textView.text;
                
                
                NSString * emailID = @"support@mfmpay.com";
                [mail setSubject:@"Support"];
                [mail setMessageBody:message isHTML:NO];
                [mail setToRecipients:@[emailID]];
                
                [self presentViewController:mail animated:YES completion:NULL];

                
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Success!", @"Success!")
                           image:nil
                           style:nil
                      completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)showSupport:(id)sender {
    
    self.webView.hidden=true;
    self.supportimgView.image=[UIImage imageNamed:@"Support_Contact_icon"];
    self.faqImgView.image=[UIImage imageNamed:@"Support_FAQ_Icon_deact"];
    self.supportLbl.hidden=false;
    self.faqLbl.hidden=true;
    self.supportheaderlbl.text=@"Support";
    
}

- (IBAction)showFAQ:(id)sender {
self.supportheaderlbl.text=@"FAQ";
    self.webView.hidden=false;
    self.supportimgView.image=[UIImage imageNamed:@"Support_Contact_icon_deact"];
    self.faqImgView.image=[UIImage imageNamed:@"Support_FAQ_Icon"];
    self.supportLbl.hidden=true;
    self.faqLbl.hidden=false;
}

#pragma textView
-(void)addDoneToolBarToKeyboard:(UITextView *)textView
{
    UIToolbar* doneToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    doneToolbar.barStyle = UIBarStyleBlackTranslucent;
    doneToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClickedDismissKeyboard)],
                         nil];
    [doneToolbar sizeToFit];
    textView.inputAccessoryView = doneToolbar;
}

//remember to set your text view delegate
//but if you only have 1 text view in your view controller
//you can simply change currentTextField to the name of your text view
//and ignore this textViewDidBeginEditing delegate method
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _textView = textView;
}

-(void)doneButtonClickedDismissKeyboard
{
    [_textView resignFirstResponder];
}
- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
@end
