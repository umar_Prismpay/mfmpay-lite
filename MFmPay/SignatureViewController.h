//
//  SignatureViewController.h
//  MFmPay
//
//  Created by Prism Pay on 6/8/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import "SignatureView.h"
@interface SignatureViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *payBtn;
- (IBAction)activity:(id)sender;
- (IBAction)credit:(id)sender;
- (IBAction)support:(id)sender;
- (IBAction)settings:(id)sender;
- (IBAction)clear:(id)sender;
- (IBAction)payAction:(id)sender;
@property (nonatomic) BOOL isPresented;
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UIButton *processBtn;
- (IBAction)process:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *signLbl;
@property (weak, nonatomic) IBOutlet SignatureView *signView;
//Get values
@property (strong, nonatomic)NSString *MCSTransID;
@property (strong, nonatomic)NSString *ProceTransID;
@property (strong, nonatomic)NSString *amounttxt;
@property (strong, nonatomic)NSString *RefNum;
@property (strong, nonatomic)NSString *ProcAppCode;
@property (weak, nonatomic) NSString * saleType;
@end
