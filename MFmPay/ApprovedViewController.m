//
//  ApprovedViewController.m
//  MFmPay
//
//  Created by Prism Pay on 4/27/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "ApprovedViewController.h"
#import "SupportViewController.h"
#import "ThemeColor.h"
#import "UIView+Toast.h"
#import "ActivityViewController.h"
#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import <MessageUI/MessageUI.h>
#import "PaymentSelectionViewController.h"
@interface ApprovedViewController ()
{
    BOOL isSms;
    BOOL isEmail;
}
@end

@implementation ApprovedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
      [self styleme];
  //  NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:0]);
    NSLog(@"%@",self.navigationController.viewControllers);
    NSLog(@"");
    
    NSString *controller=[NSString stringWithFormat:@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]];
    NSArray *myArray = [controller componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
    controller=[NSString stringWithFormat:@"%@",[myArray objectAtIndex:0]];
    if([controller isEqualToString:@"<CreditCardSaleViewController"])
    {
        self.backBtn.hidden=true;
        [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
         [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    if([controller isEqualToString:@"<RefundViewController"])
    {
        self.backBtn.hidden=true;
        [self.creditBtn setImage:[UIImage imageNamed:@"Credit_Icon_white"] forState:UIControlStateNormal];
        self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
        self.creditBtn.layer.cornerRadius = 10;
        self.creditBtn.layer.masksToBounds = YES;
        [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    if([controller isEqualToString:@"<VoidViewController"])
    {
        self.backBtn.hidden=true;
        [self.creditBtn setImage:[UIImage imageNamed:@"Credit_Icon_white"] forState:UIControlStateNormal];
        self.creditBtn.backgroundColor=[ThemeColor changeGraphColor];
        self.creditBtn.layer.cornerRadius = 10;
        self.creditBtn.layer.masksToBounds = YES;
        [ self.activityBtn setImage:[UIImage imageNamed:@"Activities_Icon"] forState:UIControlStateNormal];
        self.activityBtn.backgroundColor=[UIColor clearColor];
        
    }
    isSms=false;
    isEmail=true;
    self.mobileNum.enabled=false;
    [self setfield];
    [self setNumKeyPad];
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.blurView addGestureRecognizer: tapRec];
    // Do any additional setup after loading the view.
}
-(void)setNumKeyPad{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNum.inputAccessoryView = numberToolbar;
    
}
-(void)cancelNumberPad{
    [self.mobileNum resignFirstResponder];
    self.mobileNum.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.mobileNum resignFirstResponder];
}

-(void)tap:(UITapGestureRecognizer *)tapRec{
    self.blurView.hidden=true;
    self.send_receipt_view.hidden=true;
    
}
-(void)styleme{
    self.mobileNum.delegate=self;
    self.email.delegate=self;
     self.view.backgroundColor=[UIColor whiteColor];
    self.send_rec_btn.backgroundColor=[ThemeColor changeGraphColor];
    
    self.send_rec_btn.layer.cornerRadius = self.send_rec_btn.bounds.size.width/2;
    self.send_rec_btn.layer.masksToBounds = YES;
    
    self.send_btn.backgroundColor=[ThemeColor changeGraphColor];
    self.send_btn.layer.cornerRadius = self.send_btn.bounds.size.width/2;
    self.send_btn.layer.masksToBounds = YES;
    
  
    
    
    self.sendReceiptLbl.textColor=[ThemeColor changeGraphColor];
    
    self.activityBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.activityBtn.layer.cornerRadius = 10;
    self.activityBtn.layer.masksToBounds = YES;
}
-(void)setfield{
   if([_date isEqualToString:@"<null>"]||[_date isEqualToString:@""])
       _date=@"N/A";
    
    if([_transType isEqualToString:@"<null>"]||[_transType isEqualToString:@""])
        _transType=@"N/A";
    
    if([_customerName isEqualToString:@"<null>"]||[_customerName isEqualToString:@""])
        _customerName=@"N/A";
    
    if([_amountTxt isEqualToString:@"<null>"]||[_amountTxt isEqualToString:@""])
        _amountTxt=@"N/A";
    
    if([_cardTypetxt isEqualToString:@"<null>"]||[_cardTypetxt isEqualToString:@""])
        _cardTypetxt=@"N/A";
    
    if([_cardNumtxt isEqualToString:@"<null>"]||[_cardNumtxt isEqualToString:@""])
        _cardNumtxt=@"N/A";
    
    if([_historyIDtxt isEqualToString:@"<null>"]||[_historyIDtxt isEqualToString:@""])
        _historyIDtxt=@"N/A";
    
    if([_acctTypeID isEqualToString:@"<null>"]||[_acctTypeID isEqualToString:@""])
        _acctTypeID=@"N/A";
    
    if([_merchantID isEqualToString:@"<null>"]||[_merchantID isEqualToString:@""])
        _merchantID=@"N/A";
    
    if([_authCodetxt isEqualToString:@"<null>"]||[_authCodetxt isEqualToString:@""])
        _authCodetxt=@"N/A";
    
    
    if([_transType isEqualToString:@"Credit Sale"])
    {
        self.saleType.text = @"Credit Card: Sale";
    }
    else if([_transType isEqualToString:@"Void"])
    {
        self.saleType.text = @"Credit Card: Void";
    }
    else if([_transType isEqualToString:@"Credit"])
    {
        self.saleType.text = @"Credit Card: Refund";
    }
    else
    {
        self.saleType.text = _transType;
    }
    
    
    
    
    self.datelbl.text=_date;
  //  self.saleType.text=_transType;
    self.custName.text=_customerName;
    self.amount.text=_amountTxt;
    self.cardType.text=_cardTypetxt;
    self.cardNum.text=_cardNumtxt;
    self.historyID.text=_historyIDtxt;
    self.orderID.text=_appID;
    self.acctID.text=_acctTypeID;
    self.subID.text=_merchantID;
    self.authCode.text=_authCodetxt;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)send:(id)sender {
    self.blurView.hidden=false;
    self.send_receipt_view.hidden=false;
}
- (IBAction)send_email:(id)sender {
   
        isSms=false;
        isEmail=true;
    self.mobileNum.enabled=false;
    self.email.enabled=true;
    [self.emailBtn setImage:[UIImage imageNamed:@"Radio_Button"] forState:UIControlStateNormal];
    [self.smsBtn setImage:[UIImage imageNamed:@"Radio_Button_deactivate"] forState:UIControlStateNormal];

}

- (IBAction)send_sms:(id)sender {

        isSms=true;
        isEmail=false;
    self.mobileNum.enabled=true;
    self.email.enabled=false;
    [self.emailBtn setImage:[UIImage imageNamed:@"Radio_Button_deactivate"] forState:UIControlStateNormal];
    [self.smsBtn setImage:[UIImage imageNamed:@"Radio_Button"] forState:UIControlStateNormal];
    
}
- (void)sendSMS:(NSString *)bodyOfMessage recipientList:(NSArray *)recipients
{
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
        
//        NSString *forms = [self getforms];
        //    transactionResponse.orderid]];
        
        //     NSString *base64String = [imgData base64EncodedStringWithOptions:0];
        //
       
        
        NSString *message = [NSString stringWithFormat:@"A %@ has been processed.\r\nDate/Time: %@.\r\nAmount: $ %@.\r\nForm of Payment: %@\r\nThank You!.\r\nPlease print out this authorization and keep it with your bank records",@"Sale", [[format stringFromDate:[NSDate date]] capitalizedString], [NSString stringWithFormat:@"%@",self.amountTxt],self.saleType.text];
        messageController.body = message;
        messageController.recipients = recipients;
        messageController.messageComposeDelegate = self;
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support sending an SMS" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (IBAction)send_sms_email:(id)sender {

    self.blurView.hidden=true;
    self.send_receipt_view.hidden=true;
    if(isSms)
    {
//    [self.view makeToast:@"Sms has been sent"
//                duration:3.0
//                position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                   title:@"Success!"
//                   image:nil
//                   style:nil
//              completion:nil];
        
        
        [self sendSMS:@"Body of SMS..." recipientList:[NSArray arrayWithObjects:self.mobileNum.text, nil]];
    
   
    [self.email resignFirstResponder];
    [self.mobileNum resignFirstResponder];
    }
    else
    {
        if(![self NSStringIsValidEmail:self.email.text])
        {
            [self.view makeToast:NSLocalizedString(@"Please Enter Valid Email Address", @"Please Enter Valid Email Address")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
        }
        else
        {
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
                NSString *message = [NSString stringWithFormat:@"A %@ has been processed.\r\nDate/Time: %@.\r\nAmount: $ %@.\r\nForm of Payment: %@\r\nThank You!.\r\nPlease print out this authorization and keep it with your bank records",@"Sale", [[format stringFromDate:[NSDate date]] capitalizedString], [NSString stringWithFormat:@"%@",self.amountTxt],self.saleType.text];
               
                
                NSString * emailID = self.email.text;
                [mail setSubject:@"Sales Receipt"];
                [mail setMessageBody:message isHTML:NO];
                [mail setToRecipients:@[emailID]];
                
                [self presentViewController:mail animated:YES completion:NULL];
                
                
//                [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
//                            duration:3.0
//                            position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                               title:NSLocalizedString(@"Success!", @"Success!")
//                               image:nil
//                               style:nil
//                          completion:nil];
                [self.email resignFirstResponder];
                [self.mobileNum resignFirstResponder];
                self.email.text=@"";
                self.mobileNum.text=@"";
                
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
        }
       
    }

}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Success!", @"Success!")
                           image:nil
                           style:nil
                      completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
#pragma keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.email resignFirstResponder];
    [self.mobileNum resignFirstResponder];
    return YES;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)activity:(id)sender {
    NSString *controller=[NSString stringWithFormat:@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]];
    NSArray *myArray = [controller componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
    controller=[NSString stringWithFormat:@"%@",[myArray objectAtIndex:0]];
     if([controller isEqualToString:@"<CreditCardSaleViewController"]||[controller isEqualToString:@"<RefundViewController"])
    {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
    }
}
@end
