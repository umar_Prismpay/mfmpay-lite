//
//  SearchViewController.m
//  MFmPay
//
//  Created by Prism Pay on 5/23/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//
#define MAX_LENGTH 19
#import "SearchViewController.h"
#import "HSDatePickerViewController.h"
#import "SettingsViewController.h"
#import "SupportViewController.h"
#import "VoidRefundMainViewController.h"
#import "UIView+Toast.h"
#import "DropDownTableViewCell.h"
#import "PaymentSelectionViewController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ActivityViewController.h"
#import "SearchResultViewController.h"
#import "CreditCardSaleViewController.h"
@interface SearchViewController ()
{

    BOOL startDate;
     NSArray *TransTypeID;
}
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleMe];
    [self setNumKeyPad];
    TransTypeID=[NSArray arrayWithObjects:NSLocalizedString(@"All",@"All") ,NSLocalizedString(@"Credit Card: Sale",@"Credit Card: Sale") ,NSLocalizedString(@"Credit Card: Void",@"Credit Card: Void") ,NSLocalizedString(@"Credit Card: Refund",@"Credit Card: Refund") ,NSLocalizedString(@"Approved",@"Approved"),NSLocalizedString(@"Declined",@"Declined"), nil];
    UITapGestureRecognizer *startTap = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(startDate)];
    [self.start_date addGestureRecognizer: startTap];
    
    self.transaction_number_field.delegate=self;
    UITapGestureRecognizer *endTap = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(endDate)];
    [self.end_date addGestureRecognizer: endTap];
    
    UITapGestureRecognizer *Ttype = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(selectType)];
    [self.transaction_type addGestureRecognizer: Ttype];
       // Do any additional setup after loading the view.
    
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MMM/dd/yyyy"];
    NSString *endDate = [DateFormatter stringFromDate:[NSDate date]];
    
    NSDate *now = [DateFormatter dateFromString:endDate];
    // NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-13*24*60*60];
    NSString *startDate2 = [DateFormatter stringFromDate:sevenDaysAgo];
    
    self.start_date_label.text=startDate2;
    self.end_date_label.text=endDate;
    
    
    
}
#pragma Tap Gestures Functions
-(void)startDate{
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    startDate=true;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate=self;
    [self presentViewController:hsdpvc animated:YES completion:nil];
}
-(void)endDate{
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    startDate=false;
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate=self;
    [self presentViewController:hsdpvc animated:YES completion:nil];
}
-(void)selectType{
    [UIView animateWithDuration:.5f animations:^{
        
        self.dropDownHeight.constant=200;
        [self.dropDown layoutIfNeeded];
    }];
}
#pragma mark - HSDatePickerViewControllerDelegate
- (void)hsDatePickerPickedDate:(NSDate *)date {
    NSLog(@"Date picked %@", date);
    NSDateFormatter *dateFormater = [NSDateFormatter new];
    dateFormater.dateFormat = @"yyyy-MM-dd";
    if(startDate)
    {
        [self doneWithNumberPad];
      

        self.start_date_label.textColor=[UIColor blackColor];
    self.start_date_label.text = [dateFormater stringFromDate:date];
  
    }
    else
    {
        [self doneWithNumberPad];
        [self.transaction_number_field endEditing:YES];

        self.end_date_label.text=[dateFormater stringFromDate:date];
        self.end_date_label.textColor=[UIColor blackColor];
        [self.transaction_number_field resignFirstResponder];
    }
}



-(void)setNumKeyPad{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.transaction_number_field.inputAccessoryView = numberToolbar;
    
}
-(void)cancelNumberPad{
    [self.transaction_number_field resignFirstResponder];
    self.transaction_number_field.text = @"";
}

-(void)doneWithNumberPad{
   // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.transaction_number_field resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)styleMe{
    self.customer_name_field.delegate=self;
    self.transaction_number_field.delegate=self;
    self.clear_btn.backgroundColor=[ThemeColor changeGraphColor];
    self.clear_btn.layer.cornerRadius =  self.clear_btn.bounds.size.width/2;
    self.clear_btn.layer.masksToBounds = YES;
    
    self.search_Btn.backgroundColor=[ThemeColor changeGraphColor];
    self.search_Btn.layer.cornerRadius =  self.search_Btn.bounds.size.width/2;
    self.search_Btn.layer.masksToBounds = YES;
    
    
    self.activity_Btn.backgroundColor=[ThemeColor changeGraphColor];
    self.activity_Btn.layer.cornerRadius = 10;
    self.activity_Btn.layer.masksToBounds = YES;
    
    
    self.header_view.backgroundColor=[ThemeColor changeGraphColor];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)validate{
    
//    if([self.customer_name_field.text isEqualToString:@""] &&[self.transaction_number_field.text isEqualToString:@""]&&[self.start_date_label.text isEqualToString:@"Not Set"]&&[self.end_date_label.text isEqualToString:@"Not Set"]&&[self.transaction_type_label.text isEqualToString:@"Not Set"])
//    {
//        [self.view makeToast:@"All fields are empty"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Alert"
//                       image:nil
//                       style:nil
//                  completion:nil];
//        return false;
//    }
    if([self.start_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")]&&![self.end_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [self.view makeToast:NSLocalizedString(@"Enter Start Date", @"Enter Start Date")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self.start_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")]&&[self.end_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        [self.view makeToast:NSLocalizedString(@"Enter End Date", @"Enter End Date")
                    duration:3.0
                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                       title:NSLocalizedString(@"Alert", @"Alert")
                       image:nil
                       style:nil
                  completion:nil];
        return false;
    }
    else if(![self.start_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")]&&![self.end_date_label.text isEqualToString:NSLocalizedString(@"Not Set",@"Not Set")])
    {
        NSLog(@"%@",self.start_date_label.text);
        NSLog(@"%@",self.end_date_label.text);

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        // this is imporant - we set our input date format to match our input string
        // if format doesn't match you'll get nil from your string, so be careful
      
        [dateFormatter setDateFormat:@"MMM/dd/yyyy"];
        NSDate *date1 = [[NSDate alloc] init];
        NSDate *date2 = [[NSDate alloc] init];
        // voila!
        date1 = [dateFormatter dateFromString:self.start_date_label.text];
        date2=[dateFormatter dateFromString:self.end_date_label.text];
        
        
        if ([date1 compare:date2] == NSOrderedDescending) {
            NSLog(@"date1 is later than date2");
            [self.view makeToast:NSLocalizedString(@"Start date should be earlier than end date", @"Start date should be earlier than end date")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
            return false;
        }
        return true;
    }
    else
//    {
//        [self.view makeToast:@"Enter Start and End Date"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Alert"
//                       image:nil
//                       style:nil
//                  completion:nil];
//        return false;
//    }
    return true;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if(textField==self.transaction_number_field && self.transaction_number_field.text.length >= MAX_LENGTH && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {return YES;}
}
- (IBAction)search:(id)sender {
    if([self validate])
        [self performSegueWithIdentifier:@"results" sender:self];
 
   }
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    SearchResultViewController *svc=segue.destinationViewController;
    svc.startDate=self.start_date_label.text;
    
    svc.endDate=self.end_date_label.text;
    svc.transID=self.transaction_number_field.text;
    
    if([self.transaction_type_label.text isEqualToString:@"Credit Card: Sale"])
        svc.transTypeID=@"1";
    else if([self.transaction_type_label.text isEqualToString:@"Credit Card: Void"])
        svc.transTypeID=@"2";
    else if([self.transaction_type_label.text isEqualToString:@"Credit Card: Refund"])
        svc.transTypeID=@"3";
    else if([self.transaction_type_label.text isEqualToString:@"Approved"])
    {
        svc.transTypeID=@"";
        svc.transStatusID=@"0";
    }
    else if([self.transaction_type_label.text isEqualToString:@"Declined"])
    {
        svc.transTypeID=@"";
        svc.transStatusID=@"1";
    }
    else
        svc.transTypeID=@"";
    
    svc.transStatusID=@"";
//    svc.transTypeID=self.transaction_type_label.text;
    svc.customerNamestr=self.customer_name_field.text;
    
    
}
- (IBAction)clear_form:(id)sender {
    [self.transaction_number_field endEditing:YES];
    [self.transaction_number_field resignFirstResponder];
    self.end_date_label.textColor=[UIColor lightGrayColor];
    self.start_date_label.textColor=[UIColor lightGrayColor];
    self.transaction_type_label.textColor=[UIColor lightGrayColor];
    self.end_date_label.text=NSLocalizedString(@"Not Set",@"Not Set");
    self.start_date_label.text=NSLocalizedString(@"Not Set",@"Not Set");
    self.customer_name_field.text=@"";
    self.transaction_type_label.text=NSLocalizedString(@"All",@"All");
    self.transaction_number_field.text=@"";
}

- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)creditAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settingsAction:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)supportAction:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}
#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [TransTypeID count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"dropDown";
    
    
    DropDownTableViewCell *cell = (DropDownTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.contentView.backgroundColor=[ThemeColor changeGraphColor];
    cell.txtLabel.text=[TransTypeID objectAtIndex:indexPath.row];
    return cell;
}
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
////    if(textField==self.transaction_number_field)
//       // [textField resignFirstResponder];
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [UIView animateWithDuration:.5f animations:^{
        
        self.dropDownHeight.constant=0;
        [self.dropDown layoutIfNeeded];
    }];
    self.transaction_type_label.text=[TransTypeID objectAtIndex:indexPath.row];
    self.transaction_type_label.textColor=[UIColor blackColor];
}
- (BOOL)shouldAutorotate {
    return NO;
}
#pragma keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.customer_name_field resignFirstResponder];
    
    return YES;
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
//    [self.navigationController pushViewController:myVC animated:NO];
}

@end
