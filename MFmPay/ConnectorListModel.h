//
//  ConnectorListModel.h
//  MFmPay
//
//  Created by Muhammad Umar on 02/11/2017.
//  Copyright © 2017 Merchant First. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectorListModel : NSObject
@property (nonatomic) int TerminalDetailID;
@property (nonatomic , weak) NSString*  TerminalName;

@end
