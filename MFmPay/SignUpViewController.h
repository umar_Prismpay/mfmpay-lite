//
//  SignUpViewController.h
//  MFmPay
//
//  Created by Muhammad Umar on 12/16/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *companyName;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *applyBtn;
- (IBAction)signUp:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *fullName;
@end
