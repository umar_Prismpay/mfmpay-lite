//
//  SearchResultViewController.h
//  MFmPay
//
//  Created by Prism Pay on 5/24/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchResultViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *activity_Btn;
- (IBAction)payAction:(id)sender;
- (IBAction)creditAction:(id)sender;
- (IBAction)settingsAction:(id)sender;
- (IBAction)supportAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UITableView *result;
//Get values
@property (strong, nonatomic)NSString *customerNamestr;
@property (strong, nonatomic)NSString *transTypeID;
@property (strong, nonatomic)NSString *startDate;
@property (strong, nonatomic)NSString *endDate;
@property (weak, nonatomic) IBOutlet UIView *blank_view;
@property (strong, nonatomic)NSString *transID;
@property (strong, nonatomic)NSString *transStatusID;
- (IBAction)back:(id)sender;
@end
