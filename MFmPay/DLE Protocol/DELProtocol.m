//
//  DELProtocol.m
//  EADemo
//
//  Created by fae on 2014/9/11.
//
//
#import <Foundation/Foundation.h>
#import "DELProtocol.h"

@implementation NSString (DELProtocol)

//int attack(int defender, int hurt){
//    return(defender - hurt);
//}

- (char *)CMD_Insert_AMT
{
    NSString* Databuf = self;
    NSString* CMDAMT_Databuf = [[NSString alloc] initWithString:@"\x2C\x01"];
    CMDAMT_Databuf = [CMDAMT_Databuf stringByAppendingString:Databuf];
    return (char *)[CMDAMT_Databuf UTF8String];
}
//mp200 faisal
- (char *)CMD_Insert_Card
{
    NSString* Databuf = self;
    NSString* CMDAMT_Databuf = [[NSString alloc] initWithString:@"\x2C\x09"];
    CMDAMT_Databuf = [CMDAMT_Databuf stringByAppendingString:Databuf];
    return (char *)[CMDAMT_Databuf UTF8String];
}
- (char *)CMD_Swipe_Card
{
    NSString* Databuf = self;
    NSString* CMDAMT_Databuf = [[NSString alloc] initWithString:@"\x2C\x10"];
    CMDAMT_Databuf = [CMDAMT_Databuf stringByAppendingString:Databuf];
    return (char *)[CMDAMT_Databuf UTF8String];
}

- (char *)Send_DLE
{
    
    NSString *Databuf = self;
    ushort usLen = [Databuf length];
    // 將字串附加在接收者尾端
    //NSString *str1 = @"str1";
    //Databuf = [Databuf stringByAppendingString:@"1"];
    
    int i;
    Byte bOffset;
    
    NSString* baDLE_Data = [[NSString alloc] initWithString:@"\x10\x02"];
    NSString* Temp = [[NSString alloc] initWithString:@""];
    NSString* bLRC = [[NSString alloc] initWithString:@""];
    //    [baDLE_Data stringByAppendingString:@"\x10\x02"];
    //baDLE_Data = [baDLE_Data stringByAppendingString:@"\x10\x02"];
    //NSLog(@"附加後的字串1：%@",[baDLE_Data stringByAppendingString:Temp]);
    bOffset = 2;
    unichar Addx10 = '\x10';
    unichar lrc = 0;  //unichar = unsign short
    for(i = 0 ; i < usLen ; i++)
    {
        unichar Character_i = [Databuf characterAtIndex:i];//get ith Character of Databuf
        Temp = [NSString stringWithFormat:@"%C", Character_i];//convert unichar to NSString
        
        //NSLog(@"附加後的字串2：%@",[baDLE_Data stringByAppendingString:Temp]);
        baDLE_Data = [baDLE_Data stringByAppendingString:Temp];//add ith Character to baDLE_Data
        
        if(Character_i == Addx10)
        {
            bOffset++;
            baDLE_Data = [baDLE_Data stringByAppendingString:Temp];//add ith Character to baDLE_Data again
        }
    }
    
    for (i = 0; i < usLen; i ++)
    {
        unichar Character_i = [Databuf characterAtIndex:i];
        lrc ^= Character_i;
        bLRC = [NSString stringWithFormat:@"%C", lrc];
    }
    
    baDLE_Data = [baDLE_Data stringByAppendingString:bLRC];//add bLRC to baDLE_Data
    
    if(lrc == Addx10)
    {
        bOffset++;
        baDLE_Data = [baDLE_Data stringByAppendingString:bLRC];//add bLRC to baDLE_Data again
    }
    
    baDLE_Data = [baDLE_Data stringByAppendingString:@"\x10\x03"];
    
    //Send_RS232(baDLE_Data, usLen + bOffset + 3);
    
    
    NSLog(@"in function Send_DLE %s", [baDLE_Data UTF8String]);
    
    return (char *)[baDLE_Data UTF8String];
}

- (NSString *)Recv_DLE_WithData
{
    //ushort usLen = strlen(self.UTF8String);
    int i;
    unichar lrc = 0;  //unichar = unsign short
    NSString* Databuf = self;  //ALL Data
    NSString* Temp = [[NSString alloc] initWithString:@""];
    NSString* Real_Data = [[NSString alloc] initWithString:@""];
    NSString* Error_Command = [[NSString alloc] initWithString:@"Transaction Error"];
    NSString* Cancel_Command = [[NSString alloc] initWithString:@"Transaction Cancel"];
    NSString* DLE_Fail = [[NSString alloc] initWithString:@"Recieved Data is not conform to DLE"];
    ushort usLen = [Databuf length];
    
    NSLog(@"Fun_Rev_DLE %@", Databuf);
    unichar Ch_S1 = [Databuf characterAtIndex:0];//get 1th Character of Databuf
    unichar Ch_S2 = [Databuf characterAtIndex:1];//get 2th Character of Databuf
    unichar Ch_C1 = [Databuf characterAtIndex:2];//get 3th Character of Databuf
    unichar Ch_C2 = [Databuf characterAtIndex:3];//get 4th Character of Databuf
    unichar Ch_lrc = [Databuf characterAtIndex:(int)usLen-3];//get last Character of Databuf
    unichar Ch_E2 = [Databuf characterAtIndex:(int)usLen-2];//get last-1 Character of Databuf
    unichar Ch_E1 = [Databuf characterAtIndex:(int)usLen-1];//get last Character of Databuf
    
    //Calculate LRC
    for (i = 2; i < usLen-3; i ++)
    {
        unichar Character_i = [Databuf characterAtIndex:i];
        lrc ^= Character_i;
        NSLog(@"Character_%d = %02X", i, Character_i);
        
        //bLRC = [NSString stringWithFormat:@"%C", lrc];
    }
    
    for (i = 4; i < usLen-3; i ++)
    {
        unichar Character_i = [Databuf characterAtIndex:i];
        
        if (Character_i == '\x10')
        {
            i = i+1;
        }
        else
        {
            Temp = [NSString stringWithFormat:@"%C", Character_i];//convert unichar to NSString
            Real_Data = [Real_Data stringByAppendingString:Temp];//Get the data with out DLE protocol
        }
    }
    
    NSLog(@"%02X %02X %02X %02X %02X %02X %02X %02X", Ch_S1, Ch_S2, Ch_C1, Ch_C2, Ch_lrc, lrc, Ch_E2, Ch_E1);
    NSLog(@"Length = %d", usLen);
    
    if ( Ch_S1=='\x10' && Ch_S2=='\x02' && Ch_E2 =='\x10' && Ch_E1=='\x03' && Ch_lrc==lrc ) {
        
        if ( Ch_C1=='\x2C' && Ch_C2=='\x06') {
            
            return Real_Data;
            
        }
        else if( Ch_C1=='\x2C' && Ch_C2=='\x00') {
            
            return Cancel_Command; //return only 2C 00
        }
        else if( Ch_C1=='\x2C' && Ch_C2=='\x02') {
            
            return Error_Command; //return only 2C 00
        }
        
        else;
    }
    
    
    
    //NSLog(@"附加後的字串2：%@",[baDLE_Data stringByAppendingString:Temp]);
    //baDLE_Data = [baDLE_Data stringByAppendingString:Temp];//add ith Character to baDLE_Data
    
    //        if(Character_i == Addx10)
    //        {
    //            baDLE_Data = [baDLE_Data stringByAppendingString:Temp];//add ith Character to baDLE_Data again
    //        }
    
    //NSLog(@"Fun_Rev_DLE %@", Databuf);
    //return (NSString *)Databuf;
    return DLE_Fail;
}
@end
