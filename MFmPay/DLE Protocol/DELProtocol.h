//
//  DELProtocol.h
//  EADemo
//
//  Created by fae on 2014/9/11.
//
//

#import <Foundation/Foundation.h>

@interface NSString (DELProtocol)

- (char *)CMD_Insert_AMT;
- (char *)Send_DLE;

//mp200 faisal
- (char *)CMD_Swipe_Card;
- (char *)CMD_Insert_Card;


- (NSString *)Recv_DLE_WithData;

@end