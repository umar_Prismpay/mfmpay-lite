//
//  VoidSearchViewController.h
//  MFmPay
//
//  Created by Prism Pay on 6/17/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import "HSDatePickerViewController.h"
@interface VoidSearchViewController : BaseViewController<UITextFieldDelegate,HSDatePickerViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *customer_name_field;
@property (weak, nonatomic) IBOutlet UIView *start_date;

@property (weak, nonatomic) IBOutlet UILabel *start_date_label;
@property (weak, nonatomic) IBOutlet UITextField *transaction_number_field;
@property (weak, nonatomic) IBOutlet UIView *end_date;
@property (weak, nonatomic) IBOutlet UILabel *end_date_label;
@property (weak, nonatomic) IBOutlet UIView *transaction_type;
@property (weak, nonatomic) IBOutlet UILabel *transaction_type_label;
@property (weak, nonatomic) IBOutlet UIButton *search_Btn;
@property (weak, nonatomic) IBOutlet UIView *header_view;
@property (weak, nonatomic) IBOutlet UIButton *clear_btn;
- (IBAction)search:(id)sender;
- (IBAction)clear_form:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *activity_Btn;
- (IBAction)payAction:(id)sender;
- (IBAction)creditAction:(id)sender;
- (IBAction)settingsAction:(id)sender;
- (IBAction)supportAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *dropDown;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropDownHeight;
- (IBAction)back:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *creditBtn;

- (IBAction)activity:(id)sender;

@end
