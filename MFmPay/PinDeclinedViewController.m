//
//  PinDeclinedViewController.m
//  MFmPay
//
//  Created by Prism Pay on 6/14/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "PinDeclinedViewController.h"
#import "ThemeColor.h"
#import "UIView+Toast.h"
#import "ActivityViewController.h"
#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "SupportViewController.h"
#import "PaymentSelectionViewController.h"
@interface PinDeclinedViewController ()

@end

@implementation PinDeclinedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setfield];
    [self styleme];

    // Do any additional setup after loading the view.
}
-(void)setfield{
    self.amount.text=[NSString stringWithFormat:@"$%.2f",[self.amounttxt floatValue]];
    self.procTransID.text=self.ProceTransID;
    self.approvalCode.text=self.ProcAppCode;
    self.mcsTransID.text=self.MCSTransID;
    self.refNumber.text=self.RefNum;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)styleme{

    self.view.backgroundColor=[UIColor whiteColor];
    
    
    
    
    
    self.label_view.layer.borderWidth=2;
    self.label_view.layer.borderColor=[UIColor redColor].CGColor;
    self.label_view.layer.cornerRadius=5;
    self.label_view.layer.masksToBounds=YES;
    
    
    [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
    
 
    
    
}


- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}




- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
