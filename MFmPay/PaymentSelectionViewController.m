//
//  PaymentSelectionViewController.m
//  MFMPay_iPhone
//
//  Created by Muhammad Umar on 3/6/18.
//  Copyright © 2018 Merchant First. All rights reserved.
//

#import "PaymentSelectionViewController.h"
#import "ACHSaleViewController.h"
#import "CreditCardSaleViewController.h"
#import "SettingsViewController.h"
#import "ActivityViewController.h"
#import "SettingsViewController.h"
#import "VoidRefundMainViewController.h"
#import "SupportViewController.h"
#import "Luhn.h"
#import "UIView+Toast.h"
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "ApprovedViewController.h"
#import "DeclindedViewController.h"
#import "XMLDictionary.h"
#import "SignatureApprovedViewController.h"
#import "RealSignatureApprovedViewController.h"
#import "PinDeclinedViewController.h"
#import "ProgressHUD.h"
#import "BKCardNumberField.h"
#import "EADSessionController.h"
#import "DELProtocol.h"
#import "BKMoneyUtils.h"
#import "ConnectorListModel.h"
#import "PaymentSelectionViewController.h"
@interface PaymentSelectionViewController ()

@end

@implementation PaymentSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self styleMe];
    // Do any additional setup after loading the view.
}
-(void)styleMe{
//    self.btnSettings.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnActivity.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnCredit.backgroundColor=[ThemeColor changeGraphColor];
    self.btnActivity.backgroundColor=[ThemeColor changeGraphColor];
    self.btn_ach.backgroundColor=[ThemeColor changeGraphColor];
    self.btn_card.backgroundColor=[ThemeColor changeGraphColor];
    self.btnACHCredit.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnSupport.backgroundColor=[ThemeColor changeGraphColor];
//    self.btnSettings.backgroundColor=[ThemeColor changeGraphColor];
    [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}


- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}


- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)credit_card:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)ach:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ACHSaleViewController *myVC = (ACHSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ACHSaleViewController"];
    myVC.soapAction = @"ACHSale";
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)credit_action:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}
- (IBAction)achCredit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ACHSaleViewController *myVC = (ACHSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ACHSaleViewController"];
    myVC.soapAction = @"achCredit";
    [self.navigationController pushViewController:myVC animated:NO];
}
@end
