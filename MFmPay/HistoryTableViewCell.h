//
//  HistoryTableViewCell.h
//  MFmPay
//
//  Created by Prism Pay on 4/26/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *price_lbl;
@property (weak, nonatomic) IBOutlet UILabel *status_lbl;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *customer_name_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *arrow_img;

@end
