//
//  SignInViewController.m
//  MFmPay
//
//  Created by Muhammad Umar on 12/16/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SignInViewController.h"
#import "ActivityViewController.h"
#import "AFNetworking.h"
#import "XMLDictionary.h"
#import "ProgressHUD.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "StationsTableViewCell.h"
@interface SignInViewController ()
{
    NSMutableArray * stations;
    NSMutableArray * mcsID;
}

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    stations = [[NSMutableArray alloc]init];
    mcsID = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    self.acctID.delegate=self;
    self.loginBtn.layer.cornerRadius=5;
    self.loginBtn.layer.shadowColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.51 alpha:1.0].CGColor;
    self.loginBtn.layer.masksToBounds = NO;
    self.loginBtn.backgroundColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.44 alpha:1.0];
    self.loginBtn.layer.shadowOpacity = 0.8;
    self.loginBtn.layer.shadowRadius = 12;
    self.loginBtn.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    self.navigationController.navigationBarHidden = true;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [numberToolbar sizeToFit];
    
    
    UIToolbar* numberToolbar1 = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar1.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar1.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad1)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [numberToolbar1 sizeToFit];
    
    self.acctID.inputAccessoryView = numberToolbar1;
    self.password.inputAccessoryView = numberToolbar;

    
    NSUserDefaults * prefs = [NSUserDefaults standardUserDefaults];
    if ([[prefs valueForKey:@"demo"]isEqualToString:@"No"])
    {
        self.btnSwitch.on = true;
       _btnSwitch.thumbTintColor =[ThemeColor changeGraphColor];
    }
    else if ([[prefs valueForKey:@"demo"]isEqualToString:@"Yes"])
    {
        self.btnSwitch.on = false;
        _btnSwitch.tintColor = UIColor.whiteColor;
        _btnSwitch.thumbTintColor =UIColor.whiteColor;
    }
    
    // Do any additional setup after loading the view.
}
-(void)cancelNumberPad{
    [self.password resignFirstResponder];
    [self.acctID resignFirstResponder];

}
-(void)cancelNumberPad1{
    [self.acctID resignFirstResponder];
    [self.password becomeFirstResponder];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return !([newString length] > 19);
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    self.view.frame = CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.frame = CGRectMake(0, -0, self.view.frame.size.width, self.view.frame.size.height);

}

- (IBAction)login:(id)sender {
  
    if([self.acctID.text isEqualToString:@""]|| [self.password.text isEqualToString:@""])
    {
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Alert", @"Alert")
                                                  message:NSLocalizedString(@"Login ID or Password cannot be left empty", @"Login ID or Password cannot be left empty")
                                                  preferredStyle:UIAlertControllerStyleAlert];
        
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             NSLog(@"Resolving UIAlert Action for tapping OK Button");
                                             [alert dismissViewControllerAnimated:YES completion:nil];
        
                                         }];
        
        
                    [alert addAction:ok];
                    
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
    }
//    else if (self.acctID.text.length < 6 || self.acctID.text.length > 6)
//    {
//        UIAlertController * alert=   [UIAlertController
//                                      alertControllerWithTitle:NSLocalizedString(@"Alert", @"Alert")
//                                      message:NSLocalizedString(@"Account ID is not valid", @"Account ID is not valid")
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 NSLog(@"Resolving UIAlert Action for tapping OK Button");
//                                 [alert dismissViewControllerAnimated:YES completion:nil];
//                                 
//                             }];
//        
//        
//        [alert addAction:ok];
//        
//        
//        
//        [self presentViewController:alert animated:YES completion:nil];
//    }
    else
    {
          [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
        
        
        
        
        
        
        
        //new work
        
//      NSString *  url  =@"https://demo.prismpay.com/API/Mobile/Login";
        NSString * url =[NSString stringWithFormat:@"%@Mobile/Login",sharedManager.url];
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
        
        NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.acctID.text,@"UserName",self.password.text,@"Password", nil];
        [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
            NSArray *arr=[responseObject valueForKey:@"Object"];
            
            if(arr == (id)[NSNull null])
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Alert", @"Alert")
                                              message:NSLocalizedString(@"Invalid Credintials", @"Invalid Credintials")
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         NSLog(@"Resolving UIAlert Action for tapping OK Button");
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alert addAction:ok];
                
                
                
                [self presentViewController:alert animated:YES completion:nil];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
            
            else
            {
            NSArray * stationsArr = [[NSArray alloc]init];
            
            self.password.text = @"";
            [self.password resignFirstResponder];
                stationsArr = [arr valueForKey:@"Stations"];
                for(NSArray * val in stationsArr)
                {
                    [mcsID addObject:[val valueForKey:@"MCSAccountID"]];
                    [stations addObject:[val valueForKey:@"Name"]];
                }
            
       
            if(mcsID.count > 1)
            {
                self.blurView.hidden = false;
                self.stationsPopUp.hidden = false;
                [self.stationsTable reloadData];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            else
            {
            NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
            [prefs setValue:[mcsID objectAtIndex:0] forKey:@"MCSAccountID"];
            [prefs setValue:@"yes" forKey:@"signIn"];
            [prefs setValue:[mcsID objectAtIndex:0] forKey:@"MCSID"];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.navigationController pushViewController:myVC animated:NO];
            }
        }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"error: %@", error);
        }];
        
        //ends
            

    
    
    
    }
    
    
    
   
}

#pragma TableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [ mcsID count];
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"StationsTableViewCell";
    
    
    StationsTableViewCell *cell = (StationsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StationsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSString * str =[NSString stringWithFormat:@"%@", [stations objectAtIndex:indexPath.row]];
    
    cell.stationsLbl.text = str;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSUserDefaults *prefs =[NSUserDefaults standardUserDefaults];
    [prefs setValue:[mcsID objectAtIndex:indexPath.row] forKey:@"MCSAccountID"];
    [prefs setValue:@"yes" forKey:@"signIn"];
    [prefs setValue:[mcsID objectAtIndex:indexPath.row] forKey:@"MCSID"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self.navigationController pushViewController:myVC animated:NO];
    
}

- (IBAction)switchAction:(id)sender {
    NSUserDefaults *prefs=[NSUserDefaults standardUserDefaults];
    if (_btnSwitch.on) {
        [prefs setValue:@"No" forKey:@"demo"];
        sharedManager.url = @"https://reporting.prismpay.com/API/";
         sharedManager.transUrl = @"prod.prismpay";
        _btnSwitch.thumbTintColor =[ThemeColor changeGraphColor];
        
    }
    else
    {
        [prefs setValue:@"Yes" forKey:@"demo"];
        sharedManager.url = @"https://demo.prismpay.com/API/";
         sharedManager.transUrl = @"test.mycardstorage";
        _btnSwitch.tintColor = UIColor.whiteColor;
        _btnSwitch.thumbTintColor =UIColor.whiteColor;
    }
}
- (IBAction)close:(id)sender {
    
    self.blurView.hidden = true;
    self.stationsPopUp.hidden = true;
    [mcsID removeAllObjects];
}
@end
