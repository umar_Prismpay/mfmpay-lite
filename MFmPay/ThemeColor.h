//
//  ThemeColor.h
//  MFmPay
//
//  Created by Prism Pay on 4/25/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
@interface ThemeColor : NSObject
{
    
}
+(UIColor *)changeGraphColor;
+(UIImage *)setPayImage;
@end
