//
//  SignatureApprovedViewController.m
//  MFmPay
//
//  Created by Prism Pay on 6/13/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SignatureApprovedViewController.h"
#import "ThemeColor.h"
#import "UIView+Toast.h"
#import "ActivityViewController.h"
#import "VoidRefundMainViewController.h"
#import "SettingsViewController.h"
#import "CreditCardSaleViewController.h"
#import "SupportViewController.h"
#import "PaymentSelectionViewController.h"
@interface SignatureApprovedViewController ()
{
    BOOL isSms;
    BOOL isEmail;
}
@end

@implementation SignatureApprovedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    isSms=false;
    isEmail=true;
    self.mobileNum.enabled=false;
    [self setfield];
    [self styleme];
    [self setNumKeyPad];
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
                                      initWithTarget:self action:@selector(tap:)];
    [self.blurView addGestureRecognizer: tapRec];
}
-(void)tap:(UITapGestureRecognizer *)tapRec{
    self.blurView.hidden=true;
    self.receiptView.hidden=true;
    
}
-(void)setfield{
    self.amount.text=[NSString stringWithFormat:@"$%.2f",[self.amounttxt floatValue]];
    self.procTransID.text=self.ProceTransID;
    self.approvalCode.text=self.ProcAppCode;
    self.mcsTransID.text=self.MCSTransID;
    self.refNumber.text=self.RefNum;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)styleme{
    self.mobileNum.delegate=self;
    self.email.delegate=self;
    self.view.backgroundColor=[UIColor whiteColor];
    
    
    self.receiptBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.receiptBtn.layer.cornerRadius = self.receiptBtn.bounds.size.width/2;
    self.receiptBtn.layer.masksToBounds = YES;
    
    self.send_receiptBtn.backgroundColor=[ThemeColor changeGraphColor];
    self.send_receiptBtn.layer.cornerRadius = self.send_receiptBtn.bounds.size.width/2;
    self.send_receiptBtn.layer.masksToBounds = YES;
    
    
    
    self.label_view.layer.borderWidth=2;
    self.label_view.layer.borderColor=[UIColor colorWithRed:0.357 green:0.824 blue:0 alpha:1].CGColor;
    self.label_view.layer.cornerRadius=5;
    self.label_view.layer.masksToBounds=YES;
    
    
    [ self.payBtn setImage:[ThemeColor setPayImage] forState:UIControlStateNormal];
    
    self.sendReceiptLbl.textColor=[ThemeColor changeGraphColor];
    

}

#pragma keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [self.email resignFirstResponder];
    [self.mobileNum resignFirstResponder];
    return YES;
}
- (IBAction)payAction:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CreditCardSaleViewController *myVC = (CreditCardSaleViewController *)[storyboard instantiateViewControllerWithIdentifier:@"sale"];
//    [self.navigationController pushViewController:myVC animated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentSelectionViewController *myVC = (PaymentSelectionViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PaymentSelectionViewController"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)settings:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingsViewController *myVC = (SettingsViewController *)[storyboard instantiateViewControllerWithIdentifier:@"settings"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)support:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SupportViewController *myVC = (SupportViewController *)[storyboard instantiateViewControllerWithIdentifier:@"support"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)credit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VoidRefundMainViewController *myVC = (VoidRefundMainViewController *)[storyboard instantiateViewControllerWithIdentifier:@"voidnRefund"];
    [self.navigationController pushViewController:myVC animated:NO];
}

- (IBAction)activity:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ActivityViewController *myVC = (ActivityViewController *)[storyboard instantiateViewControllerWithIdentifier:@"activity"];
    [self.navigationController pushViewController:myVC animated:NO];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)setNumKeyPad{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlack;
    numberToolbar.items = @[
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.mobileNum.inputAccessoryView = numberToolbar;
    
}
-(void)cancelNumberPad{
    [self.mobileNum resignFirstResponder];
    self.mobileNum.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard = self.transaction_number_field.text;
    [self.mobileNum resignFirstResponder];
}

- (void)sendSMS:(NSString *)bodyOfMessage recipientList:(NSArray *)recipients
{
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
        
        //        NSString *forms = [self getforms];
        //    transactionResponse.orderid]];
        
        //     NSString *base64String = [imgData base64EncodedStringWithOptions:0];
        //
        
        
        NSString *message = [NSString stringWithFormat:@"A %@ has been processed.\r\nDate/Time: %@.\r\nAmount: $ %@.\r\nForm of Payment: %@\r\nThank You!.\r\nPlease print out this authorization and keep it with your bank records",@"Sale", [[format stringFromDate:[NSDate date]] capitalizedString], [NSString stringWithFormat:@"%@",self.amounttxt],self.saleType];
        messageController.body = message;
        messageController.recipients = recipients;
        messageController.messageComposeDelegate = self;
        // Present message view controller on screen
        [self presentViewController:messageController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support sending an SMS" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (IBAction)send_sms_email:(id)sender {
    self.blurView.hidden=true;
    self.receiptView.hidden=true;

    if(isSms)
    {
//        [self.view makeToast:@"Sms has been sent"
//                    duration:3.0
//                    position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                       title:@"Success!"
//                       image:nil
//                       style:nil
//                  completion:nil];
        
         [self sendSMS:@"Body of SMS..." recipientList:[NSArray arrayWithObjects:self.mobileNum.text, nil]];
        [self.email resignFirstResponder];
        [self.mobileNum resignFirstResponder];
    }
    else
    {
        if(![self NSStringIsValidEmail:self.email.text])
        {
            [self.view makeToast:NSLocalizedString(@"Please Enter Valid Email Address", @"Please Enter Valid Email Address")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Alert", @"Alert")
                           image:nil
                           style:nil
                      completion:nil];
        }
        else
        {
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
                mail.mailComposeDelegate = self;
                NSDateFormatter *format = [[NSDateFormatter alloc] init];
                [format setDateFormat:@"MMM/d/yy @ h:mm a zzz"];
                NSString *message = [NSString stringWithFormat:@"A %@ has been processed.\r\nDate/Time: %@.\r\nAmount: $ %@.\r\nForm of Payment: %@\r\nThank You!.\r\nPlease print out this authorization and keep it with your bank records",@"Sale", [[format stringFromDate:[NSDate date]] capitalizedString], [NSString stringWithFormat:@"%@",self.amounttxt],self.saleType];
                
                
                NSString * emailID = self.email.text;
                [mail setSubject:@"Sales Receipt"];
                [mail setMessageBody:message isHTML:NO];
                [mail setToRecipients:@[emailID]];
                
                [self presentViewController:mail animated:YES completion:NULL];
                
                
                //                [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                //                            duration:3.0
                //                            position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                //                               title:NSLocalizedString(@"Success!", @"Success!")
                //                               image:nil
                //                               style:nil
                //                          completion:nil];
                [self.email resignFirstResponder];
                [self.mobileNum resignFirstResponder];
                self.email.text=@"";
                self.mobileNum.text=@"";
                
            }
            else
            {
                NSLog(@"This device cannot send email");
            }
        }
//            [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
//                        duration:3.0
//                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
//                           title:NSLocalizedString(@"Success!", @"Success!")
//                           image:nil
//                           style:nil
//                      completion:nil];
//        [self.email resignFirstResponder];
//        [self.mobileNum resignFirstResponder];
//        self.mobileNum.text=@"";
//        self.email.text=@"";
    }
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self.view makeToast:NSLocalizedString(@"Email has been sent", @"Email has been sent")
                        duration:3.0
                        position:[NSValue valueWithCGPoint:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)]
                           title:NSLocalizedString(@"Success!", @"Success!")
                           image:nil
                           style:nil
                      completion:nil];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)send_email:(id)sender {
    
    isSms=false;
    isEmail=true;
    self.mobileNum.enabled=false;
    self.email.enabled=true;
    [self.emailBtn setImage:[UIImage imageNamed:@"Radio_Button"] forState:UIControlStateNormal];
    [self.smsBtn setImage:[UIImage imageNamed:@"Radio_Button_deactivate"] forState:UIControlStateNormal];
    
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    // Check the result or perform other tasks.
    
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)send_sms:(id)sender {
    
    isSms=true;
    isEmail=false;
    self.mobileNum.enabled=true;
    self.email.enabled=false;
    [self.emailBtn setImage:[UIImage imageNamed:@"Radio_Button_deactivate"] forState:UIControlStateNormal];
    [self.smsBtn setImage:[UIImage imageNamed:@"Radio_Button"] forState:UIControlStateNormal];
    
}
- (IBAction)show_receipt:(id)sender {
    self.blurView.hidden=false;
    self.receiptView.hidden=false;
}
@end
