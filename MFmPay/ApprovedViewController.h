//
//  ApprovedViewController.h
//  MFmPay
//
//  Created by Prism Pay on 4/27/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>
@interface ApprovedViewController : BaseViewController<UITextFieldDelegate,MFMailComposeViewControllerDelegate>
@property(nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UILabel *datelbl;
@property (weak, nonatomic) IBOutlet UILabel *saleType;
@property (weak, nonatomic) IBOutlet UILabel *custName;
@property (weak, nonatomic) IBOutlet UILabel *cardType;
@property (weak, nonatomic) IBOutlet UILabel *cardNum;
@property (weak, nonatomic) IBOutlet UILabel *historyID;
@property (weak, nonatomic) IBOutlet UILabel *orderID;
@property (weak, nonatomic) IBOutlet UILabel *acctID;
@property (weak, nonatomic) IBOutlet UILabel *subID;
@property (weak, nonatomic) IBOutlet UILabel *authCode;
@property (weak, nonatomic) IBOutlet UIButton *send_rec_btn;
- (IBAction)send:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *sendReceiptLbl;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *mobileNum;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *smsBtn;
- (IBAction)send_email:(id)sender;
- (IBAction)send_sms:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *send_btn;
@property (weak, nonatomic) IBOutlet UIView *send_receipt_view;
@property (weak, nonatomic) IBOutlet UIView *blurView;

- (IBAction)send_sms_email:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *activityBtn;
- (IBAction)creditAction:(id)sender;

- (IBAction)settings:(id)sender;
- (IBAction)payAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *amount;
- (IBAction)back:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *backBtn;

//Get values
@property (strong, nonatomic)NSString *customerName;
@property (strong, nonatomic)NSString *status;
@property (strong, nonatomic)NSString *amountTxt;
@property (strong, nonatomic)NSString *date;
@property (strong, nonatomic)NSString *transType;
@property (strong, nonatomic)NSString *cardTypetxt;
@property (strong, nonatomic)NSString *authCodetxt;
@property (strong, nonatomic)NSString *cardNumtxt;
@property (strong, nonatomic)NSString *historyIDtxt;
@property (strong, nonatomic)NSString *appID;
@property (strong, nonatomic)NSString *merchantID;
@property (strong, nonatomic)NSString *acctTypeID;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

- (IBAction)activity:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *creditBtn;

@end
