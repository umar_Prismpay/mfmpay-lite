//
//  SignUpViewController.m
//  MFmPay
//
//  Created by Muhammad Umar on 12/16/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "SignUpViewController.h"
#import "UIView+Toast.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.companyName.delegate=self;
    self.fullName.delegate=self;
    self.phoneNumber.delegate=self;
    
    self.applyBtn.layer.cornerRadius=5;
    self.applyBtn.layer.shadowColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.51 alpha:1.0].CGColor;
    self.applyBtn.layer.masksToBounds = NO;
    self.applyBtn.backgroundColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.44 alpha:1.0];
    self.applyBtn.layer.shadowOpacity = 0.8;
    self.applyBtn.layer.shadowRadius = 12;
    self.applyBtn.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signUp:(id)sender {
    if(self.fullName.text.length>0 && self.companyName.text.length>0 && self.phoneNumber.text.length>0)
    {
        
        NSString *url =@"https://reporting.prismpay.com/API/Utility/NewMCSOpeningRequest";
        
//        NSString *url =@"https://demo.prismpay.com/API/Utility/NewMCSOpeningRequest";
        
        
        
        
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
         // Do something...
         dispatch_async(dispatch_get_main_queue(), ^{
         
         AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
         manager.requestSerializer = [AFJSONRequestSerializer serializer];
         [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
         [manager.requestSerializer setValue:@"CDInhSBWiH72jie3mTrCA==" forHTTPHeaderField:@"Authorization"];
         
             NSDictionary *params=[NSDictionary dictionaryWithObjectsAndKeys:self.fullName.text,@"ContactName",self.companyName.text,@"CompanyName",self.phoneNumber.text,@"ContactNumber",nil];
         
         [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

         
             [MBProgressHUD hideHUDForView:self.view animated:YES];

             UIAlertController * alert=   [UIAlertController
                                           alertControllerWithTitle:NSLocalizedString(@"Alert", @"Alert")
                                           message:NSLocalizedString(@"We have got your information and you will be contacted soon", @"We have got your information and you will be contacted soon")
                                           preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* ok = [UIAlertAction
                                  actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      NSLog(@"Resolving UIAlert Action for tapping OK Button");
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
             
             
             [alert addAction:ok];
             
             
             
             [self presentViewController:alert animated:YES completion:nil];
         
  
         
         
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         NSLog(@"error: %@", error);
         }];
         
         });
         });
        
        
        
        
        
    
        
        
        
        
    
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(@"Alert", @"Alert")
                                      message:NSLocalizedString(@"All fields are mandatory", @"All fields are mandatory")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 NSLog(@"Resolving UIAlert Action for tapping OK Button");
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
@end
