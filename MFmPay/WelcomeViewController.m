//
//  WelcomeViewController.m
//  MFmPay
//
//  Created by Muhammad Umar on 12/16/16.
//  Copyright © 2016 Merchant First. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    self.AcctBtn.layer.cornerRadius=5;
    self.AcctBtn.backgroundColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.44 alpha:1.0];
    self.AcctBtn.layer.shadowColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.51 alpha:1.0].CGColor;
    self.AcctBtn.layer.masksToBounds = NO;
    
    self.AcctBtn.layer.shadowOpacity = 0.8;
    self.AcctBtn.layer.shadowRadius = 12;
    self.AcctBtn.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
    
    self.existingBTn.backgroundColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.44 alpha:1.0];
    
    self.existingBTn.layer.cornerRadius=5;
    self.existingBTn.layer.shadowColor=[UIColor colorWithRed:0.20 green:0.41 blue:0.51 alpha:1.0].CGColor;
    self.existingBTn.layer.masksToBounds = NO;
    
    self.existingBTn.layer.shadowOpacity = 0.8;
    self.existingBTn.layer.shadowRadius = 12;
    self.existingBTn.layer.shadowOffset = CGSizeMake(12.0f, 12.0f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)linked:(id)sender {
}

- (IBAction)signIn:(id)sender {
}
@end
